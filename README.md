# PfComp

PfComp is a formally-verified compiler for a packet filtering language.

## Overview 

PfComp generates C code from a filtering policy by using an intermediate representation based on Binary Decision Diagrams (BDDs). This project is built on top of [CompCert](https://github.com/AbsInt/CompCert), a verified C compiler. CompCert is used to generate machine code from a C file created by PfComp.

### The filtering language

Here is an example of a packet filter:

```text
STRUCT Packet {
    saddr: u32;
    daddr: u32;
    dport: u32;
}

POLICY {
    (saddr = 10.0.0.1 or saddr = 10.0.0.2): reject;
    (daddr > 10.0.0.2 and daddr < 10.0.5.2) and dport = 443: accept;
}
```

## Dependencies

PfComp requires Ocaml, Coq and the CompCert compiler. The recommended way to install OCaml and Coq is to use the Ocaml Package Manager `opam`. You can install it on your system by following the [opam documentation](https://opam.ocaml.org/).

After getting opam configured, add the Coq package repository to opam:

```bash
opam repo add coq-released https://coq.inria.fr/opam/released
```

Finally, install the following packages with `opam`:

```text
ocaml           (version 4.14.1)
ocamlfind       (version 1.9.6)
mlcuddidl       (version 3.0.7)
coq             (version 8.16.1)
menhir          (version 20230608)
coq-compcert    (version 3.13, using the "-b" option)
```

## Project structure

The project is organised a follows:

```
/-
 |-- src/               # OCaml code
 |-- tests/             # Benchmarks folder
 |-- theories/          # Core of the verified compiler (Coq files)
 |-- README.md
 `-- _CoqProject        # Coq project description file
```

## Building the project

Once all dependencies are installed, clone the project with:

```bash
git clone https://gitlab.inria.fr/cchavano/pfcomp.git
```

Run the Makefile
```bash
make pfc
```

**Cleaning**: To clean the PfComp files, use `make clean`. 

## Running the compiler

To compile a filtering policy into a C file, just execute `pfc` and specify the input file:
```bash
./pfc <file>
```
It will generate a C file at the root of the project.

You can pass the several options to the `pfc` executable, run `./pfc --help` to get more information.

Here is a example for compiling the file `filter.pf` info `filter.c` using the optimized version of `pfcomp`:

```bash
./pfc filter.pf -o filter.c -O1 -pp
```
## Known issues

The Makefile is not reliable when calling `make` with parallelization (`-j` option). Please, avoid using this option, otherwise you would have to run the `make` command several times for the project to be built.
