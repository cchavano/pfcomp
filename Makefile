-include Makefile.config

#OBJS= lib/Camlcoq.cmx extraction/Pfcompiler.cmx extraction/Naive.cmx cfrontend/PrintCsyntax.cmx cfrontend/PrintClight.cmx

THY=Common.v Bdd.v BddExt.v Formula.v Filter.v Fdd.v Fdd2clight.v Filter2formula.v \
	Formula2fdd.v Pfcompiler.v Naive.v NForm.v MoreErrors.v extractionMachdep.v

VFILES=$(addprefix theories/,$(THY))

.PHONY: vofiles

all: pfc

vars:
	@echo $(CCOMP)
	@echo $(COMPCERTDIR)
	@echo $(COMPCERTCONFIG)

CoqMakefile : Makefile _CoqProject
	coq_makefile -f _CoqProject -o CoqMakefile

#Get extractionMachdep.v from CompCert source and patch the `Require` directive
theories/extractionMachdep.v :
	@echo RETRIEVE extractionMachdep.v
	@cp  $(COMPCERTDIR)/$(ARCH)/extractionMachdep.v theories
	@sed -i 's\Require\From compcert Require\g' theories/extractionMachdep.v

compcert.ini : 
	@echo RETRIEVE compcert.ini
	@cp $(COMPCERTDIR)/compcert.ini compcert.ini

vofiles : $(VFILES) CoqMakefile
	make -f CoqMakefile 
	mkdir -p extraction
	coqc -R theories pfcomp  extraction.v

pfc : compcert.ini vofiles 
	make -f Makefile.extr depend
	make -f Makefile.extr pfc

clean: CoqMakefile
	rm -f theories/extractionMachdep.v
	rm -f compcert.ini
	rm -rf theories/extraction/*
	rm -rf extraction/*
	make -f CoqMakefile clean
	make -f Makefile.extr clean
	rm -f CoqMakefile
	rm -f src/Pfparser.mli src/Pfparser.ml src/Pflexer.mli src/Pflexer.ml
	rm -f pfc
