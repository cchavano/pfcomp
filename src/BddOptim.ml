open Formula
open Filter
open Filter2formula
open Lib


(*let rot = Lib.rotate () *)


module CamlCoqBdd =
  struct
    module CBdd = Cudd.Bdd

    let forge_coq_bdd f =
      let map = Hashtbl.create 1000 in 

      let rec mk st f =
        try
          let e = Hashtbl.find map f in
          (e,st)
        with Not_found -> 
              match CBdd.inspect f with
              | CBdd.Bool b -> ((if b then Bdd0.T else Bdd0.F),st)
              | CBdd.Ite(x, f1, f2) ->
                 let (e1,st) = mk st f1 in
                 let (e2,st) = mk st f2 in
                 let (e,st)  = Bdd0.mk_node e2 (Camlcoq.P.of_int x) e1 st in
                 Hashtbl.add map f e;
                 (e,st) in
      mk Bdd0.empty f

    let fdd2clight v2p f cd =
      let (e,st) = forge_coq_bdd f in
      Fdd2clight.bdd2clight v2p e st cd

    
  end

open Cudd

let max_path x p1 p2 =
  match p1, p2 with
  | None  , None -> None
  | None , Some (p,i) -> Some ((x,false)::p,i+1)
  | Some (p,i) , None -> Some ((x,true)::p,i+1)
  | Some (p,i) , Some (q,j) ->
     if j < i then  Some ((x,true)::p,i+1)
     else Some ((x,false)::q,j+1)

let longest_path acc f =
  let map = Hashtbl.create 1000 in
  let rec lp f = 
    try (Hashtbl.find map f)
    with Not_found ->
          match Bdd.inspect f with
          | Bdd.Bool b ->
             if b = acc then Some ([],0) else None
          | Bdd.Ite( x,f1,f2) ->
             let la1 = lp f1 in
             let la2 = lp f2 in
             let mp = max_path x la1 la2 in
             Hashtbl.add map f mp ; mp in
  lp f

let length_of_path = function
  | None -> 0
  | Some (_,i) -> i


let rec output_formula out o = function
  | TT -> output_string o "TT"
  | FF -> output_string o "FF"
  | Atom a -> out o a
  | Not p  -> Printf.fprintf o "~ (%a)" (output_formula out) p
  | And(p,q) -> Printf.fprintf o "(%a) && (%a)" (output_formula out) p (output_formula out) q
  | Or(p,q) -> Printf.fprintf o "(%a) || (%a)" (output_formula out) p (output_formula out) q
  | Ite(a,p,q) -> Printf.fprintf o "(%a)? (%a) : (%a)" out a (output_formula out) p (output_formula out) q

let output_term o = function
  | ConstInt i -> Printf.fprintf o "%i" (Camlcoq.Z.to_int i)
  | PacketField i -> output_string o (Camlcoq.camlstring_of_coqstring (Ctypesdefs.string_of_ident i))

let string_of_comparison  = function
  | Integers.Ceq -> "="
  | Integers.Cne -> "<>"
  | Integers.Clt -> "<"
  | Integers.Cle -> "<="
  | Integers.Cgt -> ">"
  | Integers.Cge -> ">="

let output_pred o ((t1,c),t2) = Printf.fprintf o "%a %s %a" output_term t1 (string_of_comparison c) output_term t2


let output_path v2p o p =
  let output_pred o (p,b) =
    match Common.PMap.find (Camlcoq.P.of_int p) v2p with
    | None -> failwith "Invalid Bdd path"
    | Some p -> if b then output_pred o p else 
                  Printf.fprintf o "~ %a" output_pred  p in
  let rec output_list o l =
    match l with
    | [] -> ()
    | [e] -> output_pred o e
    | e::l -> Printf.fprintf o "%a /\\ %a" output_pred e output_list l in

  match p with
  | None -> output_string o "No path"
  | Some (p,_) -> output_list o p

let packet_of_path p =
  match p with
  | None -> None
  | Some (p,i) -> Some ((List.filter (fun x -> snd x = true) p),i)



let rec all_position v l =
  match l with
  | [] -> [[v]]
  | e::l' ->
     let l1 = all_position v l' in
     (v:: l):: (List.rev_map (fun x -> e::x) l1)
  
let unit_permut n =
  let a = Array.make n 0 in
  Array.iteri (fun i v -> a.(i) <- i) a;
  a

let rec all_vars n =
  if n = 0
  then []
  else (n - 1) :: (all_vars (n - 1))

let rot = Lib.rotate () 

(*let reorder_sift_restrict man rest bddi =
  Printf.printf "Sift param [sift_max_swap] %i [sift_max_var] %i\n  " (Man.get_sift_max_swap man) (Man.get_sift_max_var man);
  
  let rec best_perm perms nb ord =
    match perms with
    | [] -> (nb,ord)
    | ordl'::perms' ->
       let ord' = Array.of_list ordl' in
       ignore (rot ()); Man.shuffle_heap man ord';
       let bdd' = Bdd.restrict bddi rest in
       let nb' = Bdd.size bdd' in
       if nb' < nb
       then
         begin
           Printf.printf "reorder %i -> %i\n" nb nb';
           flush stdout;
           (*best_perm perms' nb' ordl' *)
           (nb',ordl')
         end
         else best_perm perms' nb ord in

  let remove v l =
    List.filter (fun x -> x <> v) l in 

  let rec reorder l nb s =
    match l with
    | [] -> s
    | e::l ->
       let (nb',s')  = best_perm (all_position e (remove e s)) nb s in
       reorder l nb' s' in

  let s0 = all_vars (Man.get_bddvar_nb man) in 
  let s  = reorder s0 (Bdd.size bddi) s0 in
  Man.shuffle_heap man (Array.of_list s)  

 *)


let iter_reorder strat bdd =
  let man = Bdd.manager bdd in 
  let sz  = Bdd.size bdd in
  
  let rec reorder sz = 
    ignore (Man.garbage_collect man);
    time "REORDER" (Man.reduce_heap man strat) 0;
    let sz' = Bdd.size bdd in
    if sz' < sz then reorder sz' in

  reorder sz


let rec formula2bdd (man : 'a Man.t) (bf : boolform) =
  match bf with
  | TT -> Bdd.dtrue man 
  | FF -> Bdd.dfalse man
  | Atom a -> Bdd.ithvar man (Camlcoq.P.to_int a)
  | Not p -> Bdd.dnot (formula2bdd man p)
  | And (p, q) -> Bdd.dand (formula2bdd man p) (formula2bdd man q)
  | Or (p, q) -> Bdd.dor (formula2bdd man p) (formula2bdd man q)
  | Ite (a,p,q) -> Bdd.ite (Bdd.ithvar man (Camlcoq.P.to_int a)) (formula2bdd man p) (formula2bdd man q)


let formula2bddr (is_dyn: Man.reorder option)  (bdd : 'a Bdd.t) (man : 'a Man.t) (bf : boolform) =
  Man.disable_autodyn man; 
  
  let sz_max = ref 0 in
  
  let restrict = 
    match is_dyn with
    | None ->
       Bdd.restrict
    | Some order -> fun b r ->
                    Man.disable_autodyn man;
                    let res = Bdd.restrict b r in
                    (*ignore (Man.garbage_collect man);*)
                    Man.enable_autodyn man order ;
                    res in

  let dand d1 d2 =
    let r = Bdd.dand d1 d2 in
    sz_max := max !sz_max (Bdd.size r);
    r in 

  let dor d1 d2 =
    let r = Bdd.dor d1 d2 in
    sz_max := max !sz_max (Bdd.size r);
    r in 

  
  let rec mk bf =
    match bf with
    | TT -> Bdd.dtrue man
    | FF -> Bdd.dfalse man
    | Atom a -> Bdd.ithvar man (Camlcoq.P.to_int a)
    | Not p -> (*restrict*) Bdd.dnot (mk p) 
    | And (p, q) ->  ignore (rot ()); (restrict (dand (mk p) (mk q))) bdd
    | Or (p, q) -> ignore (rot ()); (restrict (dor (mk p) (mk q))) bdd
    | Ite (a,p,q) -> failwith "formula2bddr does not support Ite" in

  let res = mk bf in
  Printf.printf "Nb maximum of nodes %i\n" !sz_max ; flush stdout;
  res


let bdd_of_clauses p2v cl (man : 'a Man.t) =
  match  tok_form p2v cl with
  | None -> None
  | Some bform ->  Some (formula2bdd man bform)

(*let restrict_with_clause bdd preds cl man =
  match (bdd_of_clauses preds cl man) with
  | None -> assert false
  | Some cl_bdd -> Bdd.restrict bdd cl_bdd

let restrict_with_clause_list bdd preds man =
  let start = Sys.time() in
  let bdd' =
    List.fold_left
      (fun acc cl -> restrict_with_clause acc preds cl man)
      bdd
      (gen_clause_list3 preds)
  in
  let stop = Sys.time() in
  Printf.printf "Time to restrict the BDD: %fs\n" (stop -. start);
  flush stdout;
  bdd'
 *)
  
let varlist_to_predlist (vl : int list) (v2p : Fdd.var2pred_env) : (pred list option) =
  let vars_pos = List.map (fun i -> Camlcoq.P.of_int i) vl in
  let rec vpl2pl_rec (vpl :  Camlcoq.P.t list) : (pred list) option =
    match vpl with
    | [] -> Some []
    | v :: r ->
        (match Common.PMap.find v v2p with
        | None -> None
        | Some p ->
            (match vpl2pl_rec r with
            | None -> None
            | Some l -> Some (p :: l)))
  in
  vpl2pl_rec vars_pos


let rec mk_and_bdd man l = match l with
  | [] -> Bdd.dtrue man
  | e::l -> Bdd.dand e (mk_and_bdd man l)

let of_option o = match o with
  | None -> failwith "of_option: None"
  | Some v -> v

let of_error o = match o with
  | Errors.Error s -> failwith "of_option: None"
  | Errors.OK v -> v



let remove e l =
  let rec rem acc l =
    match l with
    | [] -> acc
    | e'::l' -> if e = e' then rem acc l'
                else rem (e'::acc) l' in
  rem [] l
  


let rec missing l all =
  match l with
  | [] -> all
  | e::l -> (missing l (remove e all))

let tvar bdd v2p =
  of_option (Common.PMap.find (Camlcoq.P.of_int (Bdd.topvar bdd)) v2p)

let rec tree_size f =
  match Bdd.inspect f with
  | Bdd.Bool b -> 1
  | Bdd.Ite(x,f1,f2) -> 1 + tree_size f1 + tree_size f2


let dry_run man red bf restrict =
  Man.flush man;
  ignore (Man.garbage_collect man) ; 
  let restrict = time "RESTRICTION" (formula2bdd man) restrict in 
  Printf.printf "Nb of nodes in the restriction (cudd dry run) %i\n%!" (Bdd.size restrict -1); 
  let filter      = time "ReBuilding bdd using cudd" (if red then formula2bddr None restrict man else formula2bdd man) bf in
  Printf.printf "Nb of nodes in the FDD (cudd dry run) %i\n%!" (Bdd.size filter -1)

let order_using_groups preds : pred list =
  let g = Filter.Clause.group_predicate preds in

  let add_acc t c li acc = 
    List.fold_left (fun acc i -> ((t,c),Filter.ConstInt i)::acc) acc li in 

  let compare_int i1 i2 = 
    if Integers.Int.eq i1 i2
    then 0
    else if Integers.Int.ltu i1 i2
    then -1 else 1 in

  
  let rec collect_preds acc g =
    match g with
    | [] -> acc
    | ((t,c),li)::g' ->
       match c with
       | Integers.Ceq ->
          collect_preds (add_acc t c li acc) g'
       | Integers.Cge -> 
          let li' = List.sort compare_int li in 
          collect_preds (add_acc t c li' acc) g'
       | Integers.Cle -> 
          let li' = List.rev (List.sort compare_int li) in 
          collect_preds (add_acc t c li' acc) g'
       |   _   -> collect_preds (add_acc t c li acc) g' in

  let preds' = collect_preds [] g in

  preds' @ (missing preds' preds)
  
 


let order_preds_aux  (gen_clight:(ident * Ctypes.members) option) (reorder: Man.reorder) (is_dyn:bool)  (reduce:bool) (pform : predform)  =
  let preds = order_preds_default pform in

  let preds = order_using_groups preds in

  Printf.printf "Nb of predicates: %d\n%!" (List.length preds);

  let p2v = predlist_to_p2v_env preds in
  let v2p = predlist_to_v2p_env preds in

  (*  Printf.fprintf stdout "Formula %a\n" (output_formula output_pred) pform;*)
  let bform = of_option (tok_form p2v pform) in

  let man = Man.make_d () in
  ignore(Man.disable_autodyn man);
  (* 1. build the restriction using tautologies *)
  let all_diff = if reduce then Filter.Clause.tauto_of_preds preds else Formula.TT in
  let bf_all_diff = of_option (tok_form p2v all_diff) in 
  let all_diff_bdd = formula2bdd man bf_all_diff in
  Printf.printf "Nb of nodes in the restriction (mlcudd): %d\n%!" ((Bdd.size all_diff_bdd) - 1);
  iter_reorder reorder all_diff_bdd;
  Man.flush man;
  let all_diff_bdd_size = Bdd.size all_diff_bdd in 
  Printf.printf "Nb of nodes in the restriction (mlcudd): %d\n%!" (all_diff_bdd_size - 1);         
  (* 2. construct the FDD *)
  let bdd = time "Building bdd using cudd" (
                if reduce then formula2bddr (if is_dyn then Some reorder else None) all_diff_bdd man
                else (fun bf -> Man.enable_autodyn man reorder; formula2bdd man bf)) bform in
  Printf.printf "Nb of nodes in the FDD (mlcudd): %d\n%!" ((Bdd.size bdd) - 1);
  (*ignore (Man.garbage_collect man) ; *)
  Man.flush man;
  iter_reorder reorder bdd; 
  Printf.printf "Nb of nodes after reordering (mlcudd): %d\n%!" ((Bdd.size bdd) - 1);
  let paccept = longest_path true bdd in
   Printf.printf "Longest accept path %i: %a\n" (length_of_path paccept) (output_path v2p)
     (packet_of_path paccept);
     flush stdout;

  (*  reorder_sift_restrict man all_diff_bdd bdd;
  Printf.printf "Nb of nodes after (new) reordering : %d\n%!" ((Bdd.size bdd) - 1); *)

  let vars = Bdd.list_of_support (Bdd.support bdd) in
  let pl = of_option (varlist_to_predlist vars v2p) in
  let missing = missing pl preds in
  if missing <> [] then Printf.printf "Warning: missing predicates in the support\n%!";
  (*Printf.printf "SUPPORT %i %a %a\n" (List.length pl) output_pred (List.hd (List.rev pl)) output_pred (tvar bdd v2p);*)
  Man.flush man;
  let preds = (missing@ pl) in
  (*List.iter (fun x -> Printf.printf "%a " output_pred x) preds;*)
  (preds, (match gen_clight with
          | None -> Errors.Error (Errors.msg [])
          | Some (id,fields) -> CamlCoqBdd.fdd2clight v2p bdd (id, fields)), (man,bf_all_diff,bform))
         

