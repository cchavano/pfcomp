let time str f arg =
  let t0 = Unix.gettimeofday () in
  let res = f arg in
  let t1 = Unix.gettimeofday () in
  Printf.printf  "#%s: %f\n%!" str (t1 -. t0);
  res

let rec map_opt f l =
  match l with
  | [] -> []
  | e::l -> match f e with
            | None -> map_opt f l
            | Some e' -> e'::map_opt f l

let iter_dir dir f =
  let hdl = Unix.opendir dir in

  let rec do_it () =
    match  Unix.readdir hdl with
    | s -> s::do_it ()
    | exception End_of_file -> Unix.closedir hdl ; [] in

  let files = do_it () in

  let files_sz = List.map (fun x ->
                     let s = Filename.concat dir x in
                     (s, (Unix.stat s).Unix.st_size)) files in

  let sorted_files = List.sort (fun (x,s1) (y,s2) -> compare s1 s2) files_sz in

  List.iter (fun (s,_) -> f s) sorted_files

let rotate () =

  let char_of_int = function
    | 0 -> "-"
    | 1 -> "\\"
    | 2 -> "|"
    | 3 -> "/"
    | _ -> "" in

  let cpt = ref 0 in

  let  xrotate () =
      print_string "\r";
      flush stdout;
      output_string stdout (char_of_int !cpt);
    flush stdout;
    cpt := (!cpt + 1) mod 4 in

  xrotate
