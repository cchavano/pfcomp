open Lexing
open Ctypes

let rec idents_of_members m =
  match m with
  | [] -> []
  | h :: t ->
     name_member h :: (idents_of_members t) 


let idents_of_cd cd =
  match cd with Composite (id, su, m, a) ->
    id :: idents_of_members m

let validate_filter s l =
  let string_of_id id = Camlcoq.camlstring_of_coqstring (Ctypesdefs.string_of_ident id) in
  
  let check_term i = function
    | Filter.ConstInt i -> ()
    | Filter.PacketField id ->
       if Common.PMap.mem id s
       then ()
       else failwith (Printf.sprintf "In rule %i: undeclared PacketField %s" i (string_of_id id)) in

  let check_pred i ((t,_),t') =
    check_term i t ; check_term i t' in

  let rec check_formula i = function
    | Formula.TT | Formula.FF -> ()
    | Formula.Atom a  -> check_pred i a
    | Formula.And(f1,f2)| Formula.Or(f1,f2) -> check_formula i f1 ;check_formula i f2
    | Formula.Not f -> check_formula i f
    | Formula.Ite(a,f1,f2) -> check_pred i a; check_formula i f1; check_formula i f2 in

  List.iteri (fun i (f,_) -> check_formula i f) l
      
let rec record_idents ids =
  match ids with
  | [] -> ()
  | h :: t ->
      ignore (Camlcoq.intern_string h);
      record_idents t

let  clight_count_conds (p:Clight.program) =
  Clight.(
  let rec nb_conds s = 
    match s with
    | Sskip 
      | Sassign _ 
      | Sset _
      | Scall _
      | Sbuiltin _ -> 0
    | Ssequence(s1,s2) -> nb_conds s1 + nb_conds s2
    | Sifthenelse(_,s1,s2) -> nb_conds s1 + nb_conds s2 + 1
    | Sloop(s1,s2) -> nb_conds s1 + nb_conds s2
    | Sbreak 
      | Scontinue
      | Sreturn _ -> 0
    | Sswitch (_,ls) -> nb_conds_ls ls
    | Slabel(_,s) -> nb_conds s
    | Sgoto _ -> 0
  and nb_conds_ls = function 
    | LSnil -> 0
    | LScons(_,s,ls) -> nb_conds s + nb_conds_ls ls
   in
  let count_gdef gd =
    match gd with
    | AST.Gvar _ -> 0
    | AST.Gfun (Ctypes.External _) -> 0
    | AST.Gfun (Ctypes.Internal f) -> nb_conds f.Clight.fn_body in 

  List.fold_left (fun acc (_,defs) -> acc + count_gdef defs) 0 p.Ctypes.prog_defs
  )

(** Compiler flags or options *)

type compiler =
  | Default (* verified compiler *)
  | Naive   (* naive filter (unverified) *)
  | Cudd    (* optimised but using the bdd of cudd (unverified) *)

let parse_compiler str =
  match str with
  | "naive" -> Naive
  | "cudd"  -> Cudd
  | _       -> Default
  
let string_of_compiler c =
  match c with
  | Default -> "D"
  | Naive   -> "N"
  | Cudd    -> "C"

let reorder_assoc = 
  [  "same"           ,   Cudd.Man.REORDER_SAME ;
    "none"            ,   Cudd.Man.REORDER_NONE ;          
    "random"          ,   Cudd.Man.REORDER_RANDOM ;         
    "random_pivot"    ,   Cudd.Man.REORDER_RANDOM_PIVOT ;
    "sift"            ,   Cudd.Man.REORDER_SIFT ;
    "sift_converge"   ,   Cudd.Man.REORDER_SIFT_CONVERGE ;
    "symm_sift"       ,   Cudd.Man.REORDER_SYMM_SIFT ;
    "symm_sift_conv"  ,   Cudd.Man.REORDER_SYMM_SIFT_CONV ;
    "window2"         ,   Cudd.Man.REORDER_WINDOW2 ;
    "window3"         ,   Cudd.Man.REORDER_WINDOW3 ;      
    "window4"         ,   Cudd.Man.REORDER_WINDOW4 ;      
    "window2_conv"    ,   Cudd.Man.REORDER_WINDOW2_CONV ;
    "window3_conv"    ,   Cudd.Man.REORDER_WINDOW3_CONV ; 
    "window4_conv"    ,   Cudd.Man.REORDER_WINDOW4_CONV ; 
    "group_sift"      ,   Cudd.Man.REORDER_GROUP_SIFT ;   
    "group_sift_conv" ,  Cudd.Man.REORDER_GROUP_SIFT_CONV ;
    "annealing"       ,   Cudd.Man.REORDER_ANNEALING ;    
    "genetic"         ,   Cudd.Man.REORDER_GENETIC ;        
    "linear"           , Cudd.Man.REORDER_LINEAR ;                  
    "linear_converge"  , Cudd.Man.REORDER_LINEAR_CONVERGE ;
    "lazy_sift"        , Cudd.Man.REORDER_LAZY_SIFT ;       	
    "exact"            , Cudd.Man.REORDER_EXACT ;           	
  ]

  
let parse_reorder str = try List.assoc str reorder_assoc
                        with Not_found -> Cudd.Man.REORDER_NONE           

(* input file *)
let filter_file = ref ""

(* output file *)
let c_file = ref ""
let dump_tokens = ref false

(* perform reductions *)
let reduce_fdd = ref false

(* pre-process formula *)
let pp = ref false

(* use gcc *)
let no_gc = ref false

(* compiler *)
let compiler = ref Default

(* re-ordering strategy *)
let reorder  = ref Cudd.Man.REORDER_GROUP_SIFT

type reorder_when =
  | Never  (* no re-ordering *)
  | Before (* reorder only the creation of the restriction *)
  | Between  (* reorder for the creation of the restriction and after *)
  | Always (* dynamic reordering *)


let dyn_reorder = ref false (* use dynamic reordering *)

(* compile all the pf files in directory *)
let dir = ref false

(* output directory for generated C code *)
let odir = ref ""

(* Recompute bdd(s) with existing order *)
let dry_run = ref false

let set_reorder str =
  reorder := parse_reorder str
  
let set_file file = filter_file := file

let set_compiler str =
  compiler := parse_compiler str

let no_opt_reject = ref false

let usage_msg = "Usage: pfcomp [options] <file>"


let options = [
  ("-o", Arg.Set_string c_file, "<file>\tGenerate output file in <file>.");
  ("-O1", Arg.Set reduce_fdd, "\t\tOptimize the code by removing redundant predicates.");
  ("-pp", Arg.Set pp, "\tPreprocess the formula.");
  ("-no-opt-reject", Arg.Set no_opt_reject,"\tDo not remove redundant Reject rules");
  ("-no-gc", Arg.Set no_gc,"\tDisable garbage collection of dead BDD nodes");
  ("-compiler", Arg.Symbol (["naive";"cudd";"default"],set_compiler), "\tCompile using the specified compiler (default is verified).");
  ("-reorder" , Arg.Symbol (List.map fst reorder_assoc,set_reorder), "\tSet cudd reordering heuristics (default group_sift).");
  ("-dyn"     , Arg.Set dyn_reorder,"\tDynamic reordering (default false)");
  ("-dir",Arg.Set dir,"\tCompile all the pf files from provided directory (default false).");
  ("-odir", Arg.Set_string odir,"\tGenerate output files in <dir> (default current directory).");
  ("-dry-run",Arg.Set dry_run,"\tRecompute bdd using cudd without reordering (default false).");
  ("-dump-tokens", Arg.Set dump_tokens, "\tPrint all tokens (stop after lexing).")
]

(* The output file name is constructed from the input file name and the provided options.
   This is only done if there is no user-provided output file (see option -o <output-file>) *)
   
let string_of_options () =
  let opt = if !reduce_fdd then "O1" else "O0" in
  let pp  = if !pp then "pp" else "" in
  let dyn = if !dyn_reorder then "dyn" else "" in
  let comp = string_of_compiler !compiler in
  let reorder =  List.assoc !reorder (List.map (fun (x,y) -> y,x) reorder_assoc) in
  String.concat "_" (List.filter (fun s -> s <> "") [opt;pp;comp;dyn;reorder])

let output_file_name file =
  if !dir || !c_file = ""
  then
    let raw_filename = file |> Filename.basename |> Filename.remove_extension in
    let cname = Printf.sprintf "%s_%s.c" raw_filename (string_of_options ()) in
    if !odir <> "" then Filename.concat !odir cname else cname
  else !c_file

let compile_file fname = 

  let outfile = (output_file_name fname) in

  Printf.printf "Compiling %s -> %s\n%!" (Filename.basename fname) outfile ;
  
  let input = open_in fname in

  let lexbuf = from_channel input in
  
    lexbuf.lex_curr_p <- { pos_fname = !filter_file; pos_lnum = 1; pos_bol = 0; pos_cnum = 0 };

    if !dump_tokens then begin
      PrintTokens.print lexbuf;
      close_in input;
      exit 0
    end;

    ignore(Camlcoq.use_canonical_atoms := true);
    
    let (pf, (id,fields)) = Pfparser.program Pflexer.read_token lexbuf in

    Printf.printf "Nb of rules: %i\n%!" (List.length pf);

    let idents = idents_of_members fields in

    let _ = validate_filter (List.fold_left (fun acc i -> Common.PMap.add i () acc) Common.PMap.empty idents) pf in
    
    let idents =
        (List.map
           (fun id ->
             Camlcoq.camlstring_of_coqstring (Ctypesdefs.string_of_ident id)))
          (id::idents)
    in

    record_idents idents;

    ignore(Camlcoq.intern_string "packet");
    ignore(Camlcoq.intern_string "filter");

    begin match !compiler with
    | Naive -> 
       begin
         match Naive.filter2c_naive pf id fields with
         | Some prog -> PrintCsyntax.destination := Some outfile; PrintCsyntax.print_if prog
         | None -> Printf.fprintf stderr "Compilation error\n"
       end
    | Default -> 
       begin 
         let pf = if not (!no_opt_reject) then Filter2formula.reduce pf else pf in
         let pform = (if !pp then NForm.opt_formula else (fun x -> x)) (Filter2formula.filter2formula pf) in
         let (preds,_,(man,rest,bform)) = BddOptim.order_preds_aux None !reorder !dyn_reorder !reduce_fdd pform in 
         
         if !dry_run then BddOptim.dry_run man !reduce_fdd bform rest ;

         
         let compil =  Pfcompiler.pol2clight (fun _ -> preds) (not !no_opt_reject) !pp !reduce_fdd (not (!no_gc)) in
         
         let cprog = compil pf (id, fields) in
         
         match cprog with
         | Errors.OK (_, prog) ->
            Printf.printf "Nb of <if> in the Clight program %i\n" (clight_count_conds prog);
            PrintClight.destination := Some outfile; PrintClight.print_if_2 prog
         | Errors.Error s -> Printf.fprintf stderr "Compilation error %s\n" (C2C.string_of_errmsg s)
       end
    | Cudd -> begin
        let pform = (if !pp then NForm.opt_formula else (fun x -> x)) (Filter2formula.filter2formula (Filter2formula.reduce pf)) in
        let (_,pg,_) = BddOptim.order_preds_aux (Some (id,fields)) !reorder !dyn_reorder !reduce_fdd pform in 
        match pg with
        | Errors.Error s -> Printf.fprintf stderr "Compilation error %s\n" (C2C.string_of_errmsg s)
        | Errors.OK (_, prog) ->
           Printf.printf "Nb of <if> in the Clight program %i\n" (clight_count_conds prog);
           PrintClight.destination := Some outfile; PrintClight.print_if_2 prog
      end
    end ; close_in input



let show_usage options msg = Arg.usage options msg

let () =
  Arg.parse options set_file usage_msg;

    if !filter_file = "" then begin
        Printf.fprintf stderr "Error: no input file provided\n";
        show_usage options usage_msg;
    exit 1
      end;

  try
    (if not !dir then (Lib.time "COMPILER" compile_file !filter_file)
     else
       begin
         let dirname = !filter_file in
         Lib.iter_dir dirname (fun x ->
             if Filename.check_suffix x ".pf"
             then
               Lib.time "COMPILER" compile_file x
             else () )
       end);
    output_string stdout "\n" 
  with
  | Sys_error msg ->
      Printf.fprintf stderr "System error: %s\n" msg
  | Pflexer.Error msg ->
      Printf.fprintf stderr "%s\n" msg
  | Pfparser.Error -> Printf.fprintf stderr "Syntax error\n"
  | Stack_overflow  ->
     Printf.printf "STACK OVERFLOW\n";
     Printexc.print_backtrace stdout
