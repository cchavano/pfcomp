{
  open Lexing
  open Pfparser
  open Printf
  
  exception Error of string

  let error msg = raise @@ Error msg

  let intsize s =
    int_of_string (String.sub s 1 (String.length s - 1)) 


  let uint32_of_string s =
  try
    let i = Int64.of_string s in
    if 0L <= i && i <= 0xFF_FF_FF_FFL
    then Int64.to_int32 i
    else error (sprintf "Int literal %s overflows the maximum value of 32-bit integers" s)
  with Failure _ ->
    error (sprintf "Int literal %s overflows the maximum value of 32-bit integers" s)

}

let digit = ['0'-'9']
let letter = ['a'-'z''A'-'Z']
let space = [' ''\t''\r']

let int_lit = digit+
let ident = (letter | '_') (letter | digit | '_')*

let comment = '#'[^'\n']+'\n'
              
rule read_token = parse
  | '\n'      { new_line lexbuf; read_token lexbuf }
  | space+    { read_token lexbuf }
  | comment   { new_line lexbuf ; read_token lexbuf }
  | "STRUCT"  { STRUCT }
  | "FILTER"
    {
	printf "Warning: the 'FILTER' keyword is deprecated, use 'POLICY' instead\n";
	POLICY
    }
  | "POLICY"  { POLICY }
  | "true"    { TRUE }
  | "false"   { FALSE }
  | "."       { DOT }
  | "*"       { STAR }
  | "{"       { LBRACE }
  | "}"       { RBRACE }
  | "("       { LPAREN }
  | ")"       { RPAREN }
  | ";"       { SEMICOLON }
  | ":"       { COLON }
  | "<="      { CLE }
  | ">="      { CGE }
  | "="       { CEQ }
  | "<"       { CLT }
  | ">"       { CGT }
  | "!="      { CNE }
  | "or"  | "||" { OR }
  | "and" | "&&" { AND }
  | "not"     { NOT }
  | "u32" { U32 }
  | "reject"  { REJECT }
  | "accept"  { ACCEPT }
  | int_lit as i
    {
      INT_LIT (uint32_of_string i)
    }
  | ident as id { IDENT id }
  | eof { EOF }
  | _
    {
      error (sprintf "Lexing error : unknown character")
    }
