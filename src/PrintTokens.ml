open Pfparser
open Printf

let token_to_string = function
  | STRUCT -> "STRUCT"
  | POLICY -> "POLICY"
  | LPAREN -> "LPAREN"
  | RPAREN -> "RPAREN"
  | LBRACE -> "LBRACE"
  | RBRACE -> "RBRACE"
  | DOT    -> "DOT"
  | STAR   -> "STAR"
  | COLON -> "COLON"
  | SEMICOLON -> "SEMICOLON"
  | NOT -> "NOT"
  | OR -> "OR"
  | AND -> "AND"
  | CEQ -> "CEQ"
  | CLT -> "CLT"
  | CGT -> "CGT"
  | CLE -> "CLE"
  | CGE -> "CGE"
  | CNE -> "CNE"
  | U32 -> "U32"
  | INT_LIT i -> sprintf "INT_LIT '%li'" i
  | ACCEPT -> "ACCEPT"
  | REJECT -> "REJECT"
  | IDENT id -> sprintf "IDENT '%s'" id
  | TRUE  -> "TRUE"
  | FALSE -> "FALSE"
  | EOF -> "EOF"

let print lexbuf =
  let rec get_tokens acc lexbuf =
    let token = Pflexer.read_token lexbuf in
    match token with
    | EOF -> acc ^ "EOF"
    | _ ->
        let acc' = sprintf "%s%s\n" acc (token_to_string token) in
        get_tokens acc' lexbuf
  in
  printf "%s\n" (get_tokens "" lexbuf)
