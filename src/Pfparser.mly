%{
  open Filter
  open Formula
  open Ctypesdefs
  open Ctypes
  open Camlcoq

  let int_of_ip l =
    let rec of_bytes l = 
      match l with
      | [] -> 0l
      | i::r -> Int32.(add (shift_left (of_bytes r)  8)  i) in
    of_bytes (List.rev l)

  let is_byte i =
    0l <= i && i < 256l

  type trm = T of term
	    | IP of (int32 option) list


  type itv = Uniq of int32
            | Itv of int32 * int32

  let itv_of_pattern i  n =
    Int32.(
	 let lb = shift_left i (n * 8) in
	 let inc = sub (shift_left 1l (n * 8)) 1l in
	 Itv(lb, add lb inc))
	
  let decompose_ip l =
    match l with
    | [Some i3; Some i2 ; Some i1; Some i0 ] -> Uniq (int_of_ip [i3;i2;i1;i0])
    | [Some i3; Some i2 ; Some i1; None ] -> itv_of_pattern (int_of_ip [i3;i2;i1]) 1
    | [Some i3; Some i2 ; None; None ] -> itv_of_pattern (int_of_ip [i3;i2]) 2
    | [Some i3; None ; None; None ] -> itv_of_pattern (int_of_ip [i3]) 3
    | [None; None ; None; None ] -> itv_of_pattern (int_of_ip []) 4
    | _   -> failwith "Wrong ip address"
  
  let compile_ip id ip =
    match decompose_ip ip with
    | Uniq i -> Atom ((PacketField id,Integers.Ceq), (ConstInt (coqint_of_camlint i)))
    | Itv(lb,ub) -> And(Atom ((PacketField id,Integers.Cge),ConstInt (coqint_of_camlint lb)),
			  Atom ((PacketField id,Integers.Cle),ConstInt (coqint_of_camlint ub)))
    
  let compile_pred t1 c t2 =
    match t1 , c , t2 with
    | T t1 , c , T t2 -> Atom ((t1,c),t2)
    | T (PacketField id) , Integers.Ceq , IP l -> compile_ip id l
    | _ , _ , _ -> failwith "predicate is not supported"

%}

%token STRUCT
%token POLICY
%token LPAREN RPAREN LBRACE RBRACE
%token DOT COLON SEMICOLON STAR 
%token NOT OR AND
%token CEQ CLT CGT CLE CGE CNE
%token U32
%token <int32> INT_LIT
%token TRUE FALSE
%token ACCEPT
%token REJECT
%token <string> IDENT
%token EOF
%left OR
%left AND
%nonassoc NOT

%start program
%type<Filter.pfilter * (AST.ident * Ctypes.members)> program
%%

program:
  | st = struct_def
    fl = filter_def
    EOF
    {
      (fl, st)
    }

struct_def:
  | STRUCT id = IDENT LBRACE
    fields = nonempty_list(field)
    RBRACE
    {
      (ident_of_string (coqstring_of_camlstring id), fields)
    }

(* intsize : sz = U { match sz with
		   | 8  -> OK I8
		   | 16 -> OK I16
		   | 32 -> OK I32
		   | 1  -> OK IBool
		   | _  -> Error sz 
		 } *)

field:
  (* bitfield *)
  | U32 id = IDENT COLON w = INT_LIT SEMICOLON {
				 let fid = ident_of_string (coqstring_of_camlstring id) in
                                 if w <= 0l || w > 32l
                                 then failwith (Printf.sprintf "Field %s is too wide" id)
				 else                                  
                                   let w   = Z.of_uint (Int32.to_int w) in
                                   Member_bitfield (fid,I32, Unsigned, noattr,w,false) }
  (* for compatibility *)
  | id = IDENT COLON U32 SEMICOLON { let fid= ident_of_string (coqstring_of_camlstring id) in
                                     Member_plain(fid,tuint)
                           } 

filter_def:
  | POLICY LBRACE
    rules = nonempty_list(rule)
    RBRACE { rules }

rule:
  | c = cond COLON a = action SEMICOLON { (c, a) }

action:
  | ACCEPT { Accept }
  | REJECT { Reject }
  
cond:
  | c1 = cond AND c2 = cond { And (c1, c2) }
  | c1 = cond OR c2 = cond  { Or (c1, c2) }
  | LPAREN c = cond RPAREN { c }
  | NOT c = cond { Not c }
  | TRUE         { TT }
  | FALSE        { FF }
  | p = pred { p } 

pred:
  | t1 = term c = comp t2 = term { compile_pred t1 c t2 }

term:
  | i  = INT_LIT { T (ConstInt (coqint_of_camlint i)) }
  | i  = ip      { IP  i }
  | id = IDENT  {  T (PacketField (ident_of_string (coqstring_of_camlstring id))) }

ipbyte: i = INT_LIT  {
                if is_byte i
                then
                  Some i else failwith (Printf.sprintf "%ld should be a byte within the range [0;255]" i)}
  | STAR { None }

ip: i3 = ipbyte DOT i2 = ipbyte DOT i1 = ipbyte DOT i0 = ipbyte
    { 
      [i3;i2;i1;i0] 
    }

comp:
  | CEQ { Integers.Ceq }
  | CLE { Integers.Cle }
  | CGE { Integers.Cge }
  | CLT { Integers.Clt }
  | CGT { Integers.Cgt }
  | CNE { Integers.Cne }
