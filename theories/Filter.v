Require Import compcert.lib.Integers.
Require Import ZArith Lia Bool List.
Require Import Common Formula.
Require Import Orders Mergesort.

Import ListNotations.

Lemma ltu_trans : forall x y z,
    Int.ltu x y = true -> Int.ltu y z = true -> Int.ltu x z = true.
Proof.
  intros.
  unfold Int.ltu in *.
  destruct (Coqlib.zlt (Int.unsigned x) (Int.unsigned y)); try discriminate.
  destruct (Coqlib.zlt (Int.unsigned y) (Int.unsigned z)); try discriminate.
  destruct (Coqlib.zlt (Int.unsigned x) (Int.unsigned z)); auto.
  lia.
Qed.


Definition ident := positive.

Definition packet := ident -> int.

Inductive term : Set :=
  | ConstInt : int -> term
  | PacketField : ident -> term.

Definition pred : Set := term * comparison * term.

Definition cond := @formula pred.

Inductive action : Set :=
  | Accept : action
  | Reject : action.

Definition rule : Set := cond * action.

Definition pfilter := list rule.

Section PredNormalization.

Definition pred_cmp_op (p : pred) : comparison :=
  match p with (_, c, _) => c end.

Definition swap_pred_args (p : pred):=
  match p with (t1, c, t2) => (t2, swap_comparison c, t1) end.

Definition normalize_pred (p : pred) : pred :=
  match p with
  | (ConstInt i1, _, ConstInt i2) => if Int.cmpu Cgt i1 i2 then swap_pred_args p else p
  | (ConstInt _, _, PacketField _) => swap_pred_args p
  | (PacketField f1, _, PacketField f2) => if (f2 <? f1)%positive then swap_pred_args p else p
  | _ => p
  end.

Inductive wf_pred : pred -> Prop :=
  | wf_pred_consts :
      forall (i1 i2 : int) (c : comparison),
        Int.cmpu Cle i1 i2 = true -> wf_pred (ConstInt i1, c, ConstInt i2)
  | wf_pred_fields :
      forall (f1 f2 : ident) (c : comparison),
        (f1 <= f2)%positive -> wf_pred (PacketField f1, c, PacketField f2)
  | wf_pred_mix :
      forall (f : ident) (i : int) (c : comparison), wf_pred (PacketField f, c, ConstInt i).

Theorem normalize_pred_wf :
  forall (p : pred), wf_pred (normalize_pred p).
Proof.
  intros. destruct p as [[t1 c] t2]. destruct t1; destruct t2; simpl.
  - destruct (Int.ltu i0 i) eqn:Elt.
    -- apply wf_pred_consts. simpl. rewrite Int.not_ltu. rewrite Elt. reflexivity.
    -- apply wf_pred_consts. simpl. rewrite Int.not_ltu. rewrite Int.ltu_not in Elt.
       rewrite <- negb_orb in Elt. apply negb_false_iff in Elt. tauto.
  - apply wf_pred_mix.
  - apply wf_pred_mix.
  - destruct (i0 <? i)%positive eqn:Elt.
    -- apply wf_pred_fields. simpl in Elt. apply Pos.ltb_lt in Elt.
        unfold "<"%positive in Elt. unfold "<="%positive. rewrite Elt. unfold not. discriminate.
    -- apply wf_pred_fields. unfold "<?"%positive in Elt. destruct (i0 ?= i)%positive eqn:EEq in Elt; try discriminate.
      + unfold "<="%positive. apply Pos.compare_eq_iff in EEq. subst.
        rewrite Pcompare_refl. unfold not. discriminate.
      + rewrite Pcompare_eq_Gt in EEq. apply Pos.gt_lt in EEq. unfold "<="%positive.
        unfold "<"%positive in EEq. rewrite EEq. unfold not. discriminate.
Qed.

Fixpoint normalize_cond (c : cond) :=
  match c with
  | TT => TT
  | FF => FF
  | Atom a => Atom (normalize_pred a)
  | Not c => Not (normalize_cond c)
  | And c1 c2 => And (normalize_cond c1) (normalize_cond c2)
  | Or c1 c2 => Or (normalize_cond c1) (normalize_cond c2)
  | Ite a c1 c2 => Ite (normalize_pred a) (normalize_cond c1) (normalize_cond c2)
  end.

Definition elim_eq_pred (p:pred) :=
  match p with
  | (t1 , Ceq , t2) => Some (And (Atom (t1 , Cge , t2)) (Atom (t2, Cge , t1)))
  | _ => None
  end.

Definition ite_f (c:cond) (f1 f2:cond) :=
  And (Or (Not c) f1)  (Or c f2).


Fixpoint elim_eq (c: cond) := 
  match c with
  | TT => TT
  | FF => FF
  | Atom a => match elim_eq_pred a with
              | None => Atom a
              | Some f => f
              end
  | Not c => Not (elim_eq c)
  | And c1 c2 => And (elim_eq c1) (elim_eq c2)
  | Or c1 c2 => Or (elim_eq c1) (elim_eq c2)
  | Ite a c1 c2 => match elim_eq_pred a with
                   | None => Ite a (elim_eq c1) (elim_eq c2)
                   | Some f => ite_f f (elim_eq c1) (elim_eq c2)
                   end
  end.


End PredNormalization.

Section PredEqDec.

Definition eqb_term (t1 t2 : term): bool :=
  match t1, t2 with
  | ConstInt i1, ConstInt i2 => Int.eq i1 i2
  | PacketField f1, PacketField f2 => Pos.eqb f1 f2
  | _, _ => false
  end.

Definition comp_to_int (c : comparison) : Z :=
  match c with
  | Ceq => 1
  | Cne => 2
  | Clt => 3
  | Cle => 4
  | Cgt => 5
  | Cge => 6
  end.

Definition eqb_comp (c1 c2 : comparison): bool :=
  Z.eqb (comp_to_int c1) (comp_to_int c2).

Definition eqb_pred (p1 p2 : pred) : bool :=
  let '(t1, c1, t1') := p1 in
  let '(t2, c2, t2') := p2 in
  (eqb_term t1 t2) && (eqb_comp c1 c2) && (eqb_term t1' t2').

Definition eq_term (t1 t2 : term) : Prop :=
  match t1, t2 with
  | ConstInt i1, ConstInt i2 => i1 = i2
  | PacketField f1, PacketField f2 => f1 = f2
  | _, _ => False
  end.

Lemma eqb_term_iff (t1 t2 : term) : eqb_term t1 t2 = true <-> t1 = t2.
Proof.
  destruct t1; destruct t2; simpl; split; try discriminate; intros.
  - apply (Int.same_if_eq i i0) in H. congruence.
  - inv H. apply Int.eq_true.
  - apply Pos.eqb_eq in H. congruence.
  - inv H. apply Pos.eqb_refl.
Qed.

Lemma eqb_term_refl : forall t, eqb_term t t = true.
Proof.
  intros.
  rewrite eqb_term_iff.
  reflexivity.
Qed.

Lemma eqb_comp_iff (c1 c2 : comparison) : eqb_comp c1 c2 = true <-> c1 = c2.
Proof.
  destruct c1; destruct c2; simpl; split; (trivial || discriminate).
Qed.

Lemma eqb_pred_iff (p1 p2 : pred) : eqb_pred p1 p2 = true <-> p1 = p2.
Proof.
  destruct p1 as [[t1 c1] t1']; destruct p2 as[[t2 c2] t2']; simpl. split; intros.
  - repeat (apply andb_prop in H; (destruct H as [H H3] || (destruct H as [H H2]))).
    apply eqb_term_iff in H. apply eqb_term_iff in H3. apply eqb_comp_iff in H2. congruence.
  - inv H. apply andb_true_intro. split. apply andb_true_intro. split.
    apply eqb_term_iff. reflexivity. apply eqb_comp_iff; reflexivity. apply eqb_term_iff. reflexivity.
Qed.

Lemma eq_pred_dec : forall (p1 p2 : pred), {p1 = p2} + {p1 <> p2}.
Proof.
  intros. destruct (eqb_pred p1 p2) eqn:EqPred.
  - rewrite eqb_pred_iff in EqPred. auto.
  - rewrite <- not_true_iff_false in EqPred. rewrite eqb_pred_iff in EqPred. auto.
Defined.

End PredEqDec.


Section Semantics.

Definition eval_term (pk : packet) (t : term) : int :=
  match t with
  | ConstInt i => i
  | PacketField f => pk f
  end.

Definition eval_pred (pk : packet) (p : pred) : bool :=
  let '(t1, c, t2) := p in Int.cmpu c (eval_term pk t1) (eval_term pk t2).

Theorem eval_normalized_pred_ok :
  forall (pk : packet) (p : pred),
    eval_pred pk p = eval_pred pk (normalize_pred p).
Proof.
  intros. destruct p as [[t1 c] t2]. destruct t1; destruct t2; simpl.
  - destruct (Int.ltu i0 i); simpl.
    -- symmetry. apply Int.swap_cmpu.
    -- reflexivity.
  - symmetry. apply Int.swap_cmpu.
  - reflexivity.
  - destruct (i0 <? i)%positive; simpl.
    -- symmetry. apply Int.swap_cmpu.
    -- reflexivity.
Qed.

Definition eval_cond {A : Set} := @eval_formula A.

Theorem eval_normalized_cond_ok :
  forall (pk : packet) (c : cond),
    eval_cond (eval_pred pk) c = eval_cond (eval_pred pk) (normalize_cond c).
Proof.
  intros. induction c; try reflexivity.
  - apply eval_normalized_pred_ok.
  - simpl. f_equal. apply IHc.
  - simpl. rewrite IHc1. rewrite IHc2. reflexivity.
  - simpl. rewrite IHc1. rewrite IHc2. reflexivity.
  - simpl. rewrite  eval_normalized_pred_ok.
    rewrite IHc1. rewrite IHc2. reflexivity.
Qed.

Lemma elim_eq_pred_ok : forall pk a f,
    elim_eq_pred a = Some f ->
    eval_pred pk a = eval_formula (eval_pred pk) f.
Proof.
  unfold elim_eq_pred.
  destruct a as ((t1,c),t2).
  destruct c; try discriminate.
  intros. inv H.
  simpl.
  rewrite! Int.not_ltu.
  rewrite Int.eq_sym.
  destruct (Int.eq (eval_term pk t2) (eval_term pk t1)) eqn:EQ;
    rewrite orb_comm; simpl; rewrite orb_comm; simpl.
  - reflexivity.
  - symmetry.
    destruct (Int.ltu (eval_term pk t2) (eval_term pk t1)) eqn:LT ; auto.
    simpl.
    apply not_true_is_false.
    intro.
    eapply ltu_trans in H; eauto.
    rewrite Int.ltu_not in H.
    rewrite Int.eq_true in H.
    simpl in H. rewrite andb_comm in H. discriminate.
Qed.

Lemma eval_ite_f : forall pk f c1 c2,
    eval_formula (eval_pred pk) (ite_f f  c1 c2) =
      if eval_formula (eval_pred pk) f
      then eval_formula (eval_pred pk) c1
      else eval_formula (eval_pred pk) c2.
Proof.
  unfold ite_f.
  simpl; intros.
  destruct (eval_formula (eval_pred pk) f); simpl;auto.
  rewrite andb_comm. reflexivity.
Qed.


Theorem eval_elim_eq_ok :
  forall (pk : packet) (c : cond),
    eval_cond (eval_pred pk) c = eval_cond (eval_pred pk) (elim_eq c).
Proof.
  intros. unfold eval_cond.
  induction c; try reflexivity.
  - simpl. destruct (elim_eq_pred a) eqn:EQ; auto.
    apply elim_eq_pred_ok;auto.
  - simpl. f_equal. apply IHc.
  - simpl. rewrite IHc1. rewrite IHc2. reflexivity.
  - simpl. rewrite IHc1. rewrite IHc2. reflexivity.
  - simpl.
    destruct (elim_eq_pred a) eqn:EQ.
    + apply elim_eq_pred_ok with (pk := pk) in EQ.
      rewrite EQ.
      rewrite eval_ite_f.
      rewrite IHc1. rewrite IHc2.
      auto.
    + simpl.
      rewrite IHc1. rewrite IHc2. reflexivity.
Qed.

Fixpoint exec (pk : packet) (pf : pfilter) : action :=
  match pf with
  | [] => Reject
  | (c, a) :: t => if eval_cond (eval_pred pk) c then a else exec pk t
  end.

End Semantics.


Module Clause.

  Definition lit := (bool * pred)%type.

  Definition t := list lit.

  Definition eval_lit (pk:packet) (l:lit) :=
    if (fst l) then eval_pred pk (snd l)
    else negb (eval_pred pk (snd l)).

  Definition eval_clause (pk:packet) (cl:t) := List.existsb (eval_lit pk) cl.

  Definition formula_of_lit (l:lit) :=
    if (fst l) then Atom (snd l) else Not (Atom (snd l)).

  Definition formula_of_clause (cl:t) :=
    List.fold_left (fun cl l => Or cl (formula_of_lit l)) cl FF.

  Lemma eval_formula_of_lit : forall pk a,
      eval_cond (eval_pred pk) (formula_of_lit a) = eval_lit pk a.
  Proof.
    unfold formula_of_lit,eval_lit.
    destruct a; simpl.
    destruct b;simpl;auto.
  Qed.

  Lemma eval_formula_of_clause : forall pk cl,
      eval_clause pk cl = eval_cond (eval_pred pk) (formula_of_clause cl).
  Proof.
    unfold eval_clause,formula_of_clause.
    intros.
    assert (ACC : eval_cond (eval_pred pk) FF = existsb (eval_lit pk) nil).
    {
      reflexivity.
    }
    rewrite (app_nil_end cl) at 1.
    revert ACC.
    generalize (@FF pred)  as f.
    generalize (@nil lit)  as acc.
    induction cl.
    - simpl. congruence.
    - simpl.
      intros.
      rewrite <- IHcl with (acc:= (a::acc)).
      rewrite! existsb_app. simpl.
      symmetry.
      rewrite orb_comm.
      rewrite (orb_comm (existsb (eval_lit pk) cl)).
      rewrite orb_assoc.
      reflexivity.
      simpl. rewrite ACC.
      rewrite orb_comm.
      f_equal.
      apply eval_formula_of_lit.
  Qed.


  Definition diff_int (t1 t2:term) : bool :=
    match t1 , t2 with
    | ConstInt i1 , ConstInt i2 => negb (Int.eq i1 i2)
    |  _          , _           => false
    end.

  Lemma diff_int_true : forall t1 t2,
      diff_int t1 t2 = true ->
      exists i1 i2, t1 = ConstInt i1 /\ t2 = ConstInt i2 /\
                      i1 <> i2.
  Proof.
    intros.
    destruct t1 ,t2 ; simpl in H; try discriminate.
    exists i,i0.
    split;auto. split ;auto.
    intro. subst. rewrite Int.eq_true in H. discriminate.
  Qed.

  Definition is_conflict (p1 p2:pred) : bool :=
    let '(t1,c1,t1') := p1 in
    let '(t2,c2,t2') := p2 in
    match c1 , c2 with
    | Ceq , Ceq => eqb_term t1 t2 && diff_int t1' t2'
    | Ceq , Cne | Cne,Ceq => eqb_term t1 t2 && eqb_term t1' t2'
    | Cgt , Ceq  | Ceq , Cgt => eqb_term t1 t2 && eqb_term t1' t2'
    | Cgt , Cle  | Cle , Cgt => eqb_term t1 t2 && eqb_term t1' t2'
    |  _  , _ => false
    end.

  Definition neg_pred (p:pred) : pred :=
    let '(t1,c1,t1') := p in
    (t1,negate_comparison c1, t1').

  Definition normalize_lit (p: bool * pred) : pred :=
    if fst p then snd p else neg_pred (snd p).

  Definition is_conflict_lit (p1 p2: bool * pred) :=
    is_conflict (normalize_lit p1) (normalize_lit p2).

  Lemma neg_pred_ok : forall pk p,
      eval_pred pk (neg_pred p) = negb (eval_pred pk p).
  Proof.
    unfold neg_pred.
    destruct p as ((t1,c),t1'); simpl.
    rewrite Int.negate_cmpu. reflexivity.
  Qed.

  Definition term_dec (x y: term) : {x = y} + {x <> y}.
  Proof.
    repeat decide equality.
    apply Int.eq_dec.
  Qed.


  Definition term_comparison_dec (x y: term * comparison) : {x = y} + {x <> y}.
  Proof.
    repeat decide equality.
    apply Int.eq_dec.
  Qed.

  Definition term_int (t: term) : option int :=
    match t with
    | ConstInt i => Some i
    | _          => None
    end.

  Fixpoint mk_and {A: Type} (f: A -> @formula pred) (l : list A) : @formula pred :=
    match l with
    | nil => TT
    | e :: l => And (f e) (mk_and f l)
    end.

  Lemma eval_formula_mk_and : forall {A : Type} (f: A -> @formula pred) pk l,
    (Forall (fun x => eval_formula (eval_pred pk) (f x) = true) l)
    <-> eval_formula (eval_pred pk) (mk_and f l) = true.
  Proof.
    induction l; simpl.
    - split; auto.
    - rewrite andb_true_iff.
      rewrite <- IHl.
      clear IHl.
      split; intros.
      inv H. split;auto.
      destruct H;constructor;auto.
  Qed.


  Definition group_predicate (l:list pred) : list ( (term * comparison) * list int) :=
    let l := group_by term_comparison_dec fst l in
    rev_map (fun '(g,l) => (g, rev_map_filter (fun x => term_int (snd x)) l)) l.



  Fixpoint AllDiff (t: term) (l:list int) : @formula pred :=
    match l with
    | nil => TT
    | p::l' => Ite (t,Ceq,ConstInt p) (mk_and (fun j => Not (Atom (t,Ceq,ConstInt j))) l') (AllDiff t l')
    end.

  Fixpoint Trans (c:comparison) (t: term) (l:list int) : @formula pred :=
    match l with
    | nil => TT
    | p::nil => TT
    | p::(q::l) as ll => And (Or (Not (Atom(t,c, ConstInt p))) (Atom (t,c,ConstInt q))) (Trans c t ll)
    end.


  Module IntGeOrder <: TotalLeBool.
    Definition t := int.
    Definition leb := Int.cmpu Cge.

    Lemma  leb_total : forall x y : t, leb x y = true \/ leb y x = true.
    Proof.
      unfold leb.
      intros. simpl.
      destruct (Int.ltu y x) eqn:LTY; try tauto.
      destruct (Int.ltu x y) eqn:LTX; try tauto.
      rewrite Int.ltu_not in LTY.
      rewrite andb_true_iff in LTY.
      rewrite LTX in LTY. simpl in LTY.
      intuition congruence.
    Qed.

  End IntGeOrder.

  Module IntLeOrder <: TotalLeBool.
    Definition t := int.
    Definition leb := Int.cmpu Cle.

    Lemma  leb_total : forall x y : t, leb x y = true \/ leb y x = true.
    Proof.
      unfold leb.
      intros. simpl.
      destruct (Int.ltu y x) eqn:LTY; try tauto.
      destruct (Int.ltu x y) eqn:LTX; try tauto.
      rewrite Int.ltu_not in LTY.
      rewrite andb_true_iff in LTY.
      rewrite LTX in LTY. simpl in LTY.
      intuition congruence.
    Qed.

  End IntLeOrder.

  Module IntGeSort := Sort IntGeOrder.
  Module IntLeSort := Sort IntLeOrder.

  (** GeLe generates minimal clauses of the form (not t >= i \/ not t <= j for i > j).
      Both list are sorted in decreasing order.
   *)
  Definition GeLe_clause (t:term) (i j:int) :=
    (Or (Not (Atom (t ,Cge , ConstInt i)))
       (Not (Atom (t ,Cle , ConstInt j)))).

  Fixpoint find_lt_max (i:int) (l2 : list int) :=
    match l2 with
    | nil => None
    | j1:: l2 => if Int.cmpu Clt j1 i then Some (j1,l2) else find_lt_max i l2
    end.

  Fixpoint  GeLe (t:term) (l1 l2:list int) {struct l1}: @formula pred :=
      match l1 with
      | nil => TT
      | i1::nil => match find_lt_max i1 l2 with
                   | None => TT
                   | Some(j,l2') => GeLe_clause t i1 j
                   end
      | i1::(i2::l1'') as l1' => match find_lt_max i1 l2 with
                                 | None => TT
                                 | Some(j,l2') =>
                                     if Int.cmpu Clt j i2
                                     then GeLe t l1' (j::l2')
                                     else
                                       And
                                         (GeLe_clause t i1 j)
                                         (GeLe t l1' (j::l2'))
                                 end
      end.


  Definition is_Le (c:comparison) :=
    match c with
    | Cle => true
    | _   => false
    end.

  Fixpoint collect_ge_le_aux (l l2: list ((term * comparison) * list int)) {struct l} :=
    match l with
    | nil => TT
    | ((tr,Cge),l1)::l =>
        match List.find (fun '((tr',cmp),_) => Coqlib.proj_sumbool (term_dec tr tr') && is_Le cmp) l2 with
        | None => collect_ge_le_aux l l2
        | Some r => And (GeLe tr (IntGeSort.sort l1) (IntGeSort.sort  (snd r))) (collect_ge_le_aux l l2)
        end
    |  _ ::l  => collect_ge_le_aux l l2
    end.

  Definition collect_ge_le (l : list ((term * comparison) * list int)) :=
    collect_ge_le_aux l l.


  Lemma find_lt_max_lt : forall i l j l',
      find_lt_max i l = Some (j, l') ->
      Int.cmpu Clt j i = true.
  Proof.
    induction l; simpl.
    - discriminate.
    - intros.
      destruct (Int.ltu a i) eqn:LTU. inv H. auto.
      eapply IHl; eauto.
  Qed.



  Lemma GeLe_clause_ok : forall pk tr i a,
      Int.cmpu Clt i a = true ->
      eval_formula (eval_pred pk) (GeLe_clause tr a i) = true.
  Proof.
    unfold GeLe_clause,eval_formula,eval_pred.
    simpl.
    intros.
    rewrite Int.not_ltu.
    destruct (Int.ltu a (eval_term pk tr)) eqn:LT_a_tr; simpl; auto.
    rewrite negb_involutive.
    destruct (Int.ltu i (eval_term pk tr)) eqn:LT_i_tr;simpl;auto.
    exfalso.
    revert LT_i_tr. apply eq_true_false_abs.
    eapply ltu_trans;eauto.
    rewrite negb_involutive.
    destruct (Int.eq a (eval_term pk tr)) eqn:EQ_a_tr; simpl;auto.
    apply Int.same_if_eq in EQ_a_tr.
    congruence.
  Qed.

  Lemma eval_formula_GeLe : forall pk tr l1 l2,
      eval_formula (eval_pred pk) (GeLe tr l1 l2) = true.
  Proof.
    induction l1; intros.
    - reflexivity.
    -
      simpl.
      destruct l1.
      + destruct (find_lt_max a l2) eqn:FLT.
        destruct p.
        apply find_lt_max_lt in FLT.
        apply GeLe_clause_ok; auto.
        reflexivity.
      + destruct (find_lt_max a l2) eqn:FLT.
        destruct p.
        destruct (Int.ltu i0 i) eqn:LTU.
        apply IHl1.
        cbn - [GeLe GeLe_clause].
        apply find_lt_max_lt in FLT.
        rewrite GeLe_clause_ok by auto.
        rewrite IHl1. reflexivity.
        reflexivity.
  Qed.

  Lemma collect_ge_le_ok : forall pk l,
      eval_formula (eval_pred pk) (collect_ge_le l) = true.
  Proof. unfold collect_ge_le.
         intros.
         generalize l at 2 as l'.
         induction l; simpl.
         - reflexivity.
         - destruct a. destruct p.
           destruct c; auto.
           intros.
           destruct (find (fun '(tr', cmp, _) => Coqlib.proj_sumbool (term_dec t0 tr') && is_Le cmp) l').
           simpl. rewrite eval_formula_GeLe.
           rewrite IHl. reflexivity.
           auto.
  Qed.





  Definition clause_of_group (t : term)  (c: comparison) (l: list int) :=
    match c with
    | Ceq => AllDiff t l
    | Cge => Trans Cge t (IntGeSort.sort l)
    | Cle => Trans Cle t (IntLeSort.sort l)
    | _   => TT
    end.

  Definition clause_of_groups (l :list ( (term * comparison) * list int)) :=
    mk_and (fun '((t,c),li) => (clause_of_group t c li)) l.

  Definition tauto_of_preds  (l:list pred) :=
    let gr := (group_predicate l) in
    And (clause_of_groups gr) (collect_ge_le gr).

  Lemma Forall_rev_map : forall {A B: Type} (P : B -> Prop) (F: A -> B) (l:list A),
      Forall (fun x => P (F x)) l ->
      Forall P (rev_map F l).
  Proof.
    intros.
    rewrite Forall_forall.
    intros.
    rewrite in_rev_map in H0.
    destruct H0 as (y & IN & Fy).
    subst.
    rewrite Forall_forall in H.
    apply H;auto.
  Qed.


  Lemma eval_formula_AllDiff : forall pk t l,
      NoDup l ->
      eval_formula (eval_pred pk) (AllDiff t l) = true.
  Proof.
    intros.
    induction H.
    - simpl. auto.
    - simpl AllDiff.
      simpl.
      destruct (Int.eq (eval_term pk t0) x) eqn:EQ.
      + rewrite <- eval_formula_mk_and.
        subst.
        clear IHNoDup.
        rewrite Forall_forall.
        simpl. intros.
        apply eq_true_not_negb.
        intro.  apply Int.same_if_eq  in EQ,H2.
        intuition congruence.
      + auto.
  Qed.


  Lemma eval_Trans_correct :
    forall (c:comparison) pk ti l
           (TRANS : forall x y z, Int.cmpu c x y = true ->
                                  Int.cmpu c y z = true ->
                                  Int.cmpu c x z = true)
           (SRT : Sorted.Sorted (fun x y => Int.cmpu c x y= true) l),
      eval_formula (eval_pred pk) (Trans c ti l) = true.
  Proof.
    intros.
    induction SRT.
    +  reflexivity.
    + simpl.
      destruct l.
      * simpl;auto.
      * simpl eval_formula.
        rewrite andb_true_iff.
        split.
        inv H.
        rewrite orb_true_iff.
        destruct ((Int.cmpu c (eval_term pk ti) a)) eqn:CMP;simpl; try tauto.
        right.
        eapply TRANS;eauto.
        apply IHSRT.
  Qed.


  Lemma eval_clause_of_group : forall pk t c l,
      NoDup l ->
      eval_formula (eval_pred pk)
        (clause_of_group t c l) = true.
  Proof.
    unfold clause_of_group.
    destruct c; simpl; auto.
    - (* AllDiff *)
      intros. apply eval_formula_AllDiff;auto.
    - (* Trans_le *)
      intros.
      eapply eval_Trans_correct.
      { simpl.
        intros.
        rewrite Int.not_ltu in *.
        rewrite orb_true_iff in *.
        destruct H0.
        destruct H1.
        left.
        eapply ltu_trans;eauto.
        apply Int.same_if_eq in H1.
        subst. tauto.
        apply Int.same_if_eq in H0.
        subst.
        auto.
      }
      apply (IntLeSort.Sorted_sort l).
    - (* Trans_ge *)
      intros.
      eapply eval_Trans_correct.
      { simpl.
        intros.
        rewrite Int.not_ltu in *.
        rewrite orb_true_iff in *.
        destruct H0.
        destruct H1.
        left.
        eapply ltu_trans;eauto.
        apply Int.same_if_eq in H1.
        subst. tauto.
        apply Int.same_if_eq in H0.
        subst.
        auto.
      }
      apply (IntGeSort.Sorted_sort l).
  Qed.


  Lemma term_int_inv : forall ti i, term_int ti = Some i -> ti = ConstInt i.
  Proof.
    intros.
    destruct ti; simpl in *.
    congruence.
    discriminate.
  Qed.

  Lemma tauto_of_preds_correct : forall pk l
      (ND : NoDup l),
      eval_formula (eval_pred pk) (tauto_of_preds l) = true.
  Proof.
    unfold tauto_of_preds.
    intros.
    simpl.
    apply andb_true_intro.
    split.
    { unfold clause_of_groups.
    rewrite <- eval_formula_mk_and.
    unfold group_predicate.
    apply Forall_rev_map.
    assert (GBY := group_by_correct term_comparison_dec fst l).
    assert (GBYND := group_by_NoDup term_comparison_dec fst l ND).
    revert GBY GBYND.
    generalize (group_by term_comparison_dec fst l) as L.
    unfold is_group_by.
    intros.
    induction GBY.
    - constructor.
    - destruct x as (k,lk).
      inv GBYND.
      constructor;eauto.
      clear IHGBY.
      simpl in H2.
      destruct k as (t,c).
      apply eval_clause_of_group.
      unfold rev_map_filter.
      assert (ACC: NoDup (map ConstInt (@nil Int.int) ++ map snd lk)).
      {
        simpl.
        clear - H H2. induction H2.
        - simpl. constructor.
        - simpl. constructor;auto.
          inv H.
          intro. rewrite in_map_iff in H.
          destruct H as ((x1,i) & EQ & IN).
          destruct x. simpl in *. subst.
          rewrite Forall_forall in H5.
          specialize (H5 _ IN).
          simpl in H5. subst. tauto.
          apply IHNoDup.
          inv H;auto.
      }
      revert ACC.
      generalize (@nil Int.int) as acc.
      clear.
      set (F := (fun (acc0 : list int) (e : term * comparison * term) =>
        match term_int (snd e) with
        | Some v => v :: acc0
        | None => acc0
        end)).
      induction lk.
      +  simpl. intros. rewrite <- app_nil_end in ACC.
         apply NoDup_map_inv in ACC; auto.
      + simpl.
        intros.
        apply IHlk.
        unfold F.
        destruct (term_int (snd a)) eqn:INT.
        * simpl.
          apply term_int_inv in INT.
          rewrite <- INT.
          change (snd a :: map ConstInt acc ++ map snd lk)
            with ((snd a:: nil) ++ map ConstInt acc ++ map snd lk).
          apply NoDup_swap1.
          auto.
        *  change (map ConstInt acc ++ snd a :: map snd lk)
            with (map ConstInt acc ++ (snd a:: nil) ++ map snd lk) in ACC.
           apply NoDup_swap1 in ACC.
           inv ACC;auto.
    }
    apply collect_ge_le_ok.
  Qed.



  Fixpoint add_term_to_list (t:term) (c: int) (acc : list (term * list pred)) (l: list (term * list pred)) :=
    match l with
    | nil => (t,(t,Ceq, ConstInt c)::nil)::acc
    | (t1,l1)::l' => if eqb_term t t1
                     then rev_app ((t1, (t,Ceq, ConstInt c)::l1)::l') acc
                     else add_term_to_list t c ((t1,l1)::acc) l'
    end.

  Definition term_Ceq_int (p: pred) : option (term * int) :=
    match p with
    | (t,Ceq,ConstInt i) => Some(t,i)
    |   _                => None
    end.

  Fixpoint xall_diff (acc : list (term * list pred)) (l : list pred)  :=
    match l with
    | nil => acc
    | e :: l => xall_diff
                  (match term_Ceq_int e with
                   | Some(t,i) => (add_term_to_list t i nil acc)
                   |   None                => acc
                   end)
                  l
    end.

  Definition all_diff (l: list pred) := xall_diff nil l.



  Fixpoint all_diff_form (l:list pred) : @formula pred :=
    match l with
    | nil => TT
    | p::l' => Ite p (mk_and (fun j => Not (Atom j)) l') (all_diff_form l')
    end.

  Lemma all_diff_form_correct :
    forall (l:list pred)
           (ND : NoDup l)
           (ALL: forall p1 p2, List.In p1 l -> List.In p2 l -> p1 <> p2 ->
                               forall pk, eval_pred pk p1 = true -> eval_pred pk p2 = true -> False),
      forall pk, eval_formula (eval_pred pk) (all_diff_form l) = true.
  Proof.
    intros.
    induction ND.
    - simpl. reflexivity.
    - simpl.
      destruct (eval_pred pk x) eqn:B.
      +
        rewrite <- eval_formula_mk_and.
        rewrite Forall_forall.
        intros.
        simpl.
        rewrite negb_true_iff.
        apply not_true_is_false.
        unfold not.
        eapply ALL;eauto.
        simpl. tauto.
        simpl. tauto.
        intuition congruence.
      + apply IHND.
        intros.
        eapply ALL;eauto.
        simpl. tauto.
        simpl. tauto.
  Qed.

  Definition all_diff_tauto (l:list pred) :=
    mk_and (fun x => all_diff_form (snd x)) (xall_diff nil l).

  Lemma add_term_to_list_In : forall t i l acc x,
      List.In x (add_term_to_list t i acc l) ->
      List.In x acc \/ List.In x l \/
        exists y, (y = nil \/ List.In (fst x,y) l) /\
                      x = (t,(t,Ceq,ConstInt i)::y).
  Proof.
    induction l;simpl.
    -  intros.
       destruct H.
       + subst.
         right. right.
         exists nil.
         split. tauto.
         reflexivity.
       + tauto.
    - destruct a as (t1,l1).
      intros.
      destruct (eqb_term t0 t1) eqn:EQ.
      apply eqb_term_iff in EQ.
      subst.
      apply in_rev_app in H.
      simpl in H. destruct H.
      tauto.
      destruct H;subst.
      right.
      right.
      exists l1.
      simpl. split.
      right. tauto.
      reflexivity.
      tauto.
      apply IHl in H.
      simpl in H.
      destruct H.
      destruct H ; subst.
      tauto.
      tauto.
      destruct H. tauto.
      destruct H as (y & EQ1 & EQ2).
      subst. destruct EQ1. subst.
      right. right. exists nil.
      split. tauto. reflexivity.
      simpl in H.
      right.
      right.
      exists y. split.
      right. simpl. tauto.
      reflexivity.
  Qed.


  Lemma add_term_to_list_NoDup : forall t i l acc
      (ND : Forall (fun x  => NoDup (snd x)) (l++acc))
      (NIN1  : Forall (fun x => List.In (t,Ceq,ConstInt i) (snd x) -> False) l),
    Forall (fun x  => NoDup (snd x)) (add_term_to_list t i acc l).
  Proof.
    intros.
    rewrite Forall_forall in *.
    intros.
    apply add_term_to_list_In in H.
    destruct H; auto.
    apply ND. rewrite in_app_iff. tauto.
    destruct H;auto.
    apply ND. rewrite in_app_iff. tauto.
    destruct H as (y & EQ1 & EQ2).
    destruct EQ1; subst. simpl. constructor. simpl. tauto. constructor.
    simpl in H.
    constructor ;auto.
    intro.
    eapply NIN1;eauto.
    change y with (snd (t0,y)). eapply ND.
    rewrite in_app_iff. tauto.
  Qed.


  Lemma term_Ceq_int_inv : forall x t i,
      term_Ceq_int x = Some (t, i) -> x = (t,Ceq,ConstInt i).
  Proof.
    intros.
    unfold term_Ceq_int in H.
    destruct x. destruct p.
    destruct c; try discriminate. destruct t1 ; try discriminate.
    congruence.
  Qed.

  Lemma xall_diff_NoDup :
    forall l
           (ND: NoDup l)
           acc
           (ACC: Forall (fun x => NoDup (snd x)) acc)
           (ACCL: forall p, List.In p l -> Forall (fun x => List.In p (snd x) -> False) acc)
    ,
           Forall (fun x => NoDup (snd x)) (xall_diff acc l).
  Proof.
    intros l ND.
    induction ND.
    - simpl.
      intros.  auto.
    - intros.
      simpl.
      apply IHND;auto.
      + destruct (term_Ceq_int x) eqn:TEQI.
        *  destruct p as (t,i).
           apply term_Ceq_int_inv in TEQI.
           subst.
           apply add_term_to_list_NoDup; auto.
           rewrite Forall_app. split;auto.
           apply ACCL.
           simpl.
           tauto.
        *  auto.
      + intros.
        destruct (term_Ceq_int x) eqn:TEQI.
        destruct p0.
        apply term_Ceq_int_inv in TEQI. subst.
        rewrite Forall_forall.
        intros.
        apply add_term_to_list_In in H1.
        destruct H1 as [H1 | [H1 | (y & EQ1 & EQ2)]].
        simpl in H1. tauto.
        specialize (ACCL _ (or_intror _ H0)).
        rewrite Forall_forall in ACCL.
        eapply ACCL ;eauto.
        subst.
        simpl in *.
        destruct H2. subst.
        tauto.
        destruct EQ1. subst. simpl in H1. tauto.
        specialize (ACCL _ (or_intror _ H0)).
        rewrite Forall_forall in ACCL.
        eapply ACCL ;eauto.
        apply ACCL.
        simpl. tauto.
  Qed.

  Lemma forall_xall_diff : forall x l acc
      (ACC:  forall y, List.In y acc -> Forall (fun p => exists i, term_Ceq_int p = Some (fst y,i)) (snd y)),
      List.In x (xall_diff acc l) ->
      Forall (fun p => exists i, term_Ceq_int p = Some (fst x,i)) (snd x).
  Proof.
    induction l.
    - simpl. intros.
      apply ACC;auto.
    - intros.
      simpl in H.
      apply IHl in H.
      auto.
      clear H.
      intros.
      destruct (term_Ceq_int a) eqn:CEQ; auto.
      destruct p.
      apply term_Ceq_int_inv in CEQ.
      subst.
      apply add_term_to_list_In in H.
      simpl in H. destruct H; try tauto.
      destruct H.
      auto.
      destruct H. destruct H; subst.
      simpl.
      constructor.
      exists i. simpl. reflexivity.
      destruct H; subst.
      constructor.
      simpl in H.
      apply ACC in H;auto.
  Qed.

  Lemma all_diff_tauto_correct : forall pk l
      (ND : NoDup l),
      eval_formula (eval_pred pk) (all_diff_tauto l) = true.
  Proof.
    unfold all_diff_tauto.
    intros.
    rewrite  <- eval_formula_mk_and.
    rewrite Forall_forall.
    intros.
    apply all_diff_form_correct.
    - revert x H.
      rewrite <- Forall_forall.
      apply xall_diff_NoDup; auto.
    - intros.
      apply forall_xall_diff in H; auto.
      rewrite Forall_forall in H.
      apply H in H0.
      apply H in H1.
      destruct H0. destruct H1. apply term_Ceq_int_inv in H0.
      apply term_Ceq_int_inv in H1.
      subst. simpl in *.
      apply Int.same_if_eq in H3.
      apply Int.same_if_eq in H4. congruence.
      simpl. tauto.
  Qed.


  Lemma is_conflict_sound : forall p1 p2,
      is_conflict p1 p2 = true ->
      forall pk, eval_pred pk p1 = true -> eval_pred pk p2 = true -> False.
  Proof.
    destruct p1 as ((t1,c1),t1').
    destruct p2 as ((t2,c2),t2').
    simpl.
    destruct c1,c2; try discriminate; intros;
      rewrite andb_true_iff in *; rewrite! eqb_term_iff in *;
      repeat match goal with
      | H : ?X /\ ?Y |- _ => destruct H
      end ; subst; simpl in *.
    -
      apply Int.same_if_eq in H0.
      apply Int.same_if_eq in H1.
      apply diff_int_true in H2.
      destruct H2 as (i1 & i2 & EQ1 & EQ2 & D).
      subst. simpl in *.
      congruence.
    -  apply Int.same_if_eq in H0.
       rewrite H0 in H1.
       rewrite Int.eq_true in H1.
       discriminate.
    -  apply Int.same_if_eq in H0.
       rewrite H0 in H1.
       rewrite Int.ltu_not  in H1.
       rewrite andb_true_iff in H1.
       rewrite Int.eq_true in H1. simpl in H1. intuition congruence.
    -  apply Int.same_if_eq in H1.
       rewrite H1 in H0.
       rewrite Int.eq_true in H0.
       discriminate.
    - rewrite H1 in H0. discriminate.
    -  apply Int.same_if_eq in H1.
       rewrite H1 in H0.
       rewrite Int.ltu_not  in H0.
       rewrite andb_true_iff in H0.
       rewrite Int.eq_true in H0. simpl in H0. intuition congruence.
    - rewrite H0 in H1. discriminate.
  Qed.

  Lemma eval_normalize_lit : forall pk p,
      eval_pred pk (normalize_lit p) = eval_lit pk p.
  Proof.
    unfold normalize_lit.
    unfold eval_lit. intros.
    destruct (fst p); auto.
    rewrite neg_pred_ok. reflexivity.
  Qed.

  Lemma is_conflict_lit_sound : forall p1 p2,
      is_conflict_lit  p1 p2 = true ->
      forall pk, eval_lit pk p1 = true ->
                 eval_lit  pk p2 = true -> False.
  Proof.
    unfold is_conflict_lit.
    intros.
    apply is_conflict_sound with (pk:=pk) in H; auto.
    rewrite eval_normalize_lit; auto.
    rewrite eval_normalize_lit; auto.
  Qed.

  Definition gen_clause (p1 p2:pred) :=
    if is_conflict p1 p2
    then Some ((false,p1) :: (false,p2) :: nil)
    else None.

  Lemma gen_clause_correct : forall p1 p2 cl,
      gen_clause p1 p2 = Some cl ->
      forall pk, eval_clause pk cl = true.
  Proof.
    unfold gen_clause.
    intros.
    destruct (is_conflict p1 p2) eqn:C; try discriminate.
    inv H.
    unfold eval_clause.
    generalize (is_conflict_sound p1 p2 C pk).
    intro.
    rewrite existsb_exists.
    simpl. destruct (eval_pred pk p1) eqn:EP1.
    destruct (eval_pred pk p2) eqn:EP2.
    tauto.
    - exists (false,p2).
      unfold eval_lit. simpl. rewrite EP2. tauto.
    - exists (false,p1).
      unfold eval_lit. simpl. rewrite EP1. tauto.
  Qed.


End Clause.
