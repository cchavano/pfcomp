(** Boolean formulae with n-ary boolean operators *)
Require Import Common.
Require Import Formula.
Require Import ZArith Lia.
Require Import Coq.Program.Wf.

Inductive op := AND | OR.

Definition op_eq_dec (o1 o2:op) : {o1 = o2} + {o1 <> o2}.
Proof.
  decide equality.
Qed.

Definition eval_op (o : op) : bool -> bool -> bool :=
  match o with
  | AND => andb
  | OR  => orb
  end.

Definition idem_op (o :op) :=
  match  o with
  | AND => true
  | OR  => false
  end.

Lemma eval_op_assoc : forall o b1 b2 b3,
    eval_op o (eval_op o b1 b2) b3 = eval_op o b1 (eval_op o b2 b3).
Proof.
  destruct o;simpl; intros.
  rewrite andb_assoc. reflexivity.
  rewrite orb_assoc. reflexivity.
Qed.

Lemma eval_op_comm : forall o (x y : bool), eval_op o x y = eval_op o y x.
Proof.
  destruct o; simpl; intros.
  apply andb_comm.
  apply orb_comm.
Qed.

Lemma eval_op_id : forall o x,
    eval_op o x x = x.
Proof.
  destruct o; simpl; intros.
  apply andb_diag.
  apply orb_diag.
Qed.


Section S.
  Context {A: Type}.

  Variable eqA : forall x1 y : A, {x1 = y} + {x1 <> y}.

  Inductive form :=
  | ATOM (b:bool) (a:A)
  | OP (o:op) (l:list form).


  Fixpoint eval_form (env : A -> bool) (f: form) : bool :=
    match f with
    | ATOM b a => if b then env a else negb (env a)
    | OP o l => List.fold_right (fun e acc => eval_op o (eval_form env e) acc) (idem_op o) l
    end.

  Definition t_f := OP AND nil.
  Definition f_f := OP OR nil.

  Definition ite_f (a:A) (f1 f2:form) :=
    OP AND (OP OR ((ATOM false a):: f1::nil) ::
              (OP OR ((ATOM true a):: f2::nil)) :: nil).

  Definition nite_f (a:A) (f1 f2:form) :=
    OP OR (OP AND ((ATOM true a):: f1::nil) ::
              (OP AND ((ATOM false a):: f2::nil)) :: nil).

  Lemma ifnegb : forall {A: Type} (a:bool) (x y:A),
      (if negb a then x else y) = if a then y else x.
  Proof.
    destruct a; reflexivity.
  Qed.

  Fixpoint of_form (pol:bool) (f: @formula A) : form :=
    match f with
    | TT => if pol then t_f else f_f
    | FF => if pol then f_f else t_f
    | Atom x => ATOM pol x
    | And f1 f2 => OP (if pol then AND else OR)  (of_form pol f1::of_form pol f2::nil)
    | Or f1 f2  => OP (if pol then OR else AND) (of_form pol f1::of_form pol f2 :: nil)
    | Not f     =>   of_form (negb pol) f
    | Ite a f1 f2 =>
        if pol
        then ite_f a (of_form pol f1) (of_form pol f2)
        else nite_f a (of_form pol f1) (of_form pol f2)
    end.


  Lemma of_form_correct : forall env f (pol:bool),
      (if pol then eval_formula env f
       else negb (eval_formula env f))
      = eval_form env (of_form pol f).
  Proof.
    induction f; simpl;auto.
    - destruct pol; reflexivity.
    - destruct pol; reflexivity.
    - intros.
      rewrite <- IHf.
      rewrite ifnegb.
      rewrite negb_involutive. reflexivity.
    - intros.
      rewrite <- IHf1.
      rewrite <- IHf2.
      destruct pol; simpl.
      rewrite andb_true_r. reflexivity.
      rewrite orb_false_r.
      rewrite negb_andb. reflexivity.
    - intros.
      rewrite <- IHf1.
      rewrite <- IHf2.
      destruct pol; simpl.
      rewrite orb_false_r. reflexivity.
      rewrite andb_true_r.
      rewrite negb_orb.  reflexivity.
    - intros.
      unfold ite_f,nite_f.
      destruct pol; simpl;
        rewrite <- IHf1;
        rewrite <- IHf2;
        rewrite! orb_false_r;
        rewrite! andb_true_r.
      destruct (env a);simpl;auto.
      rewrite andb_true_r. reflexivity.
      destruct (env a);simpl;auto.
      rewrite orb_false_r. reflexivity.
  Qed.

  Section FlattenRec.

    Variable Frec : form -> op * list form.

    Definition flatten_list (o:op) (l:list form) : list form :=
      List.fold_right (fun f acc  =>
                           let (o',l') := Frec f in
                           if op_eq_dec o  o' then app l' acc else
                             (OP o' l')::acc) nil l.
    End FlattenRec.

    Fixpoint flatten (f: form) :=
      match f with
      | ATOM b a => (AND, (ATOM b a:: nil))
      | OP o l => (o,flatten_list flatten o l)
      end.

    Definition form_of_flatten (f:op * list form) := OP (fst f) (snd f).


    Fixpoint flatten_correct (env: A -> bool) (f:form):
        eval_form env f = eval_form env (form_of_flatten (flatten  f)).
    Proof.
      destruct f.
      - simpl. rewrite andb_true_r. reflexivity.
      - simpl.
        set (F := (fun (e : form) (acc : bool) => eval_op o (eval_form env e) acc)).
        induction l; simpl.
        + reflexivity.
        +
          destruct (flatten a) as (o',l') eqn:FL.
          destruct (op_eq_dec o o').
          { subst.
            unfold F at 1.
            rewrite flatten_correct.
            rewrite FL.
            rewrite fold_right_app.
            rewrite <- IHl.
            unfold form_of_flatten.
            simpl eval_form.
            change ((fun (e : form) (acc : bool) => eval_op o' (eval_form env e) acc)) with F.
            generalize (fold_right F (idem_op o') l) as acc.
            intros.
            clear.
            induction l'.
            - simpl. destruct o' ;simpl; auto.
            - simpl.
              unfold F at 1.
              rewrite <- IHl'.
              generalize (fold_right F (idem_op o') l') as v.
              intros. unfold F.
              apply eval_op_assoc.
          }
          { simpl.
            unfold F at 1.
            rewrite flatten_correct.
            rewrite FL.
            rewrite IHl.
            auto.
          }
    Qed.

    Fixpoint xsplit {A: Type} (b:bool) (l1 l2: list A) (l:list A) :=
      match l with
      | nil => (l1,l2)
      | e1::r => if b then xsplit false (e1::l1) l2 r
                 else xsplit true l1 (e1::l2) r
      end.

    Definition split {A : Type} (l:list A) := xsplit true nil nil l.

    Definition is_nil {A: Type} (l:list A) :=
      match l with
      | nil => true
      | _   => false
      end.

    Lemma xsplit_length_diff :
      forall {B: Type} (l:list B) (b:bool) (l1 l2 :list B) l1' l2'
             (SPLIT : xsplit b l1 l2 l = (l1',l2')),
        ((Nat.Even (length l) -> Zlength l1' - Zlength l2' = Zlength l1 - Zlength l2)
        /\
          (Nat.Odd (length l) -> if b then
                                   Zlength l1' - Zlength l2' = (Zlength l1 +1) - Zlength l2
                                 else Zlength l1' - Zlength l2' = Zlength l1 - (Zlength l2 +1)))%Z.
    Proof.
      induction l.
      - simpl.
        intros.
        inv SPLIT.
        split_and;auto.
        intros. inv H. lia.
      - intros.
        simpl in SPLIT.
        destruct b.
        { assert (SPLIT' := SPLIT).
          apply IHl in SPLIT; clear IHl.
          destruct_and.
          split_and; intros.
          - simpl in H.
            rewrite Nat.Even_succ in H.
            apply SPLIT1 in H.
            rewrite Zlength_cons in H.
            lia.
          - simpl in H.
            rewrite Nat.Odd_succ in H.
            apply SPLIT0 in H.
            rewrite Zlength_cons in H.
            lia.
        }
        {
          apply IHl in SPLIT.
          destruct_and.
          split_and; simpl; intros.
          - rewrite Nat.Even_succ in H.
          apply SPLIT1 in H.
          rewrite Zlength_cons in H.
          lia.
          - rewrite Nat.Odd_succ in H.
            apply SPLIT0 in H.
            rewrite Zlength_cons in H.
            lia.
        }
    Qed.

    Lemma xsplit_length :
      forall {B: Type} (l:list B) (b:bool) (l1 l2 :list B) l1' l2'
             (SPLIT : xsplit b l1 l2 l = (l1',l2')),
        length l + length l1 + length l2 = length l1' + length l2'.
    Proof.
      induction l.
      - simpl.
        intros.
        inv SPLIT.
        auto.
      - intros.
        simpl in *.
        destruct b; apply IHl in SPLIT;
          simpl in *.
        lia.
        lia.
    Qed.

    Definition split_lt {B: Type} (l:list B) (res : list B * list B) :=
      forall (LEN : length l >= 2),
        length (fst res) < length l /\
          length (snd res) < length l.

    Lemma split_lt_split {B: Type} (l: list B): split_lt l (split l).
    Proof.
      unfold split_lt;intros.
      unfold split.
      destruct (xsplit true nil nil l) as (l1',l2') eqn:SPLT.
      assert (SPLT':= SPLT).
      apply xsplit_length in SPLT.
      apply xsplit_length_diff in SPLT'.
      rewrite Zlength_nil in *.
      rewrite !Zlength_correct in *.
      simpl.
      destruct (Nat.Even_or_Odd  (length l)).
      - apply SPLT' in H.
        simpl in *.
        lia.
      - apply SPLT' in H.
        simpl in *.
        lia.
    Qed.

    Definition split_prf {B: Type} (l:list B) : { res : list B * list B | split_lt l res}.
    Proof.
      exists (split l).
      apply split_lt_split.
    Defined.

    Lemma fold_right_app_assoc :
      forall {B C: Type} (id: B) (mk : B -> B -> B) (E: B -> C) (MK : C -> C -> C)
             (MORPH : forall x y, E (mk x y) = MK (E x) (E y))
             (ASSOC : forall x y z, E (mk x (mk y z)) = E (mk (mk x y) z))
             (IDL   : forall x, E (mk id x) = E x)
             (l1 l2: list B)
      ,
        E (fold_right mk id (l1++l2)) = E (mk (fold_right mk id l1) (fold_right mk id l2)).
    Proof.
      induction l1.
      - simpl. intros. rewrite IDL. reflexivity.
      - simpl.
        intros.
        rewrite MORPH.
        rewrite IHl1;auto.
        rewrite <- ASSOC.
        rewrite! MORPH.
        reflexivity.
    Qed.

    Lemma In_hd : forall {B: Type} (eqB : forall (x y: B), {x =  y} + {x <> y}) (x: B) (l: list B),
        List.In x l -> exists l',
         ~ List.In x l' /\
           forall y,
             (List.In y (x::l') <-> List.In y l).
    Proof.
      induction l.
      -  simpl. tauto.
      - simpl. intros.
        destruct H.
        + subst.
          exists (List.remove eqB x l).
          intros.
          split.
          apply remove_In.
          split ; intros.
          destruct H ; try tauto.
          apply in_remove in H.
          tauto.
          destruct H ; try tauto.
          destruct (eqB x y).
          tauto.
          right.
          apply in_in_remove; auto.
        + apply IHl in H.
          destruct H as (l' & IN).
          destruct IN.
          destruct (eqB a x).
          {
            subst.
            exists l'.
            simpl in *.
            split ; intros.
            * intro. tauto.
            * rewrite <- H0. tauto.
          }
          {
            exists (a::l').
            simpl in *.
            split ; intros.
            * intro. tauto.
            * rewrite <- H0. tauto.
          }
    Qed.

    Fixpoint remove_first {B: Type} (eqB : forall (x y: B), {x =  y} + {x <> y}) (x:B) (l:list B) : list B :=
      match l with
      | nil => nil
      | e::l => if eqB x e then l
                else e :: remove_first eqB x l
      end.

    Lemma fold_right_in :
      forall {B C: Type} (eqB : forall (x y: B), {x =  y} + {x <> y})(id: B) (mk : B -> B -> B)
             (E: B -> C)  (MK : C -> C -> C)
             (ASSOC : forall x y z, MK (MK x y) z = MK x (MK y z))
             (COMM : forall x y , MK x y = MK y x)
             (MKID  : forall x, MK x x = x)
             (MORPH : forall x y, E (mk x y) = MK (E x) (E y))
             (l l': list B)
             (SAME : forall x, List.In x l <-> List.In x l'),
        E (fold_right mk id l) = E (fold_right mk id l') .
    Proof.
      induction l.
      - simpl. intros.
        destruct l'. simpl. reflexivity.
        simpl.
        specialize (SAME b).
        simpl in SAME. tauto.
      - simpl.
        intros.
        rewrite MORPH.
        assert (IN : List.In a l').
        {
          rewrite <- SAME. tauto.
        }
        destruct (In_dec eqB a l).
        { rewrite (IHl l').
        clear - IN ASSOC COMM MKID MORPH.
          induction l'.
          -   simpl in IN. tauto.
          - simpl. simpl in IN.
          destruct IN; subst.
          +  rewrite MORPH.
             rewrite <- ASSOC.
             rewrite MKID. reflexivity.
          +  rewrite MORPH.
             rewrite <- ASSOC.
            rewrite (COMM (E a)).
            rewrite ASSOC.
            rewrite IHl';auto.
          -   split ; intros.
              rewrite <- SAME. tauto.
              rewrite <- SAME in H.
              intuition congruence.
        }
        rewrite (IHl (remove eqB a l')).
        clear - IN ASSOC COMM MKID MORPH.
        {
          induction l'.
          -   simpl in IN. tauto.
          - simpl. simpl in IN.
          destruct IN; subst.
          +  destruct (eqB a a).
             destruct (In_dec eqB a l').
             rewrite MORPH.
             rewrite <- IHl';auto.
             rewrite <- ASSOC.
             rewrite MKID. reflexivity.
             rewrite notin_remove;auto.
             congruence.
          + destruct (eqB a a0); subst.
            rewrite MORPH.
            rewrite <- IHl';auto.
            rewrite <- ASSOC.
            rewrite MKID. reflexivity.
            simpl.
            rewrite MORPH.
            rewrite <- ASSOC.
            rewrite (COMM (E a)).
            rewrite ASSOC.
            rewrite IHl';auto.
        }
        split ; intros.
        apply in_in_remove.
        congruence.
        rewrite <- SAME. tauto.
        apply in_remove in H.
        rewrite <- SAME in H. intuition congruence.
    Qed.

    Lemma in_xsplit : forall {B: Type} l b (l1 l2:list B) ,
      forall x1 : B, List.In x1 (l1++l2++l) <-> List.In x1 (fst (xsplit b l1 l2 l) ++ snd (xsplit b l1 l2 l)).
    Proof.
      induction l; simpl.
      - intros. rewrite! in_app_iff. simpl. tauto.
      - destruct b.
        + intros.
        rewrite <- IHl.
        simpl.
        rewrite! in_app_iff. simpl. tauto.
        + intros.
        rewrite <- IHl.
        simpl.
        rewrite! in_app_iff. simpl.
        rewrite! in_app_iff.
        tauto.
    Qed.

    Lemma in_split : forall {B: Type} l (l1 l2:list B) ,
      forall x1 : B, List.In x1 l <-> List.In x1 (fst (split l) ++ snd (split l)).
    Proof.
      unfold xsplit.
      intros.
      unfold split.
      rewrite <- in_xsplit.
      rewrite ! in_app_iff.
      simpl. tauto.
    Qed.



    Program Fixpoint to_formulas {B:Type} (id : B) (mk : B -> B -> B) (l: list B) {measure (length l)}
      : { r : B | forall (C: Type) (E: B -> C) (MK : C -> C -> C)
                         (eqB : forall (x y:B),{x = y} + {x <> y})
                         (ASSOC : forall x y z, MK (MK x y) z = MK x (MK y z))
                         (IDL   : forall x, E (mk id x) = E x)
                         (COMM : forall x y , MK x y = MK y x)
                         (MKID  : forall x, MK x x = x)
                         (MORPH : forall x y, E (mk x y) = MK (E x) (E y)),
          E (List.fold_right mk id l) = E r}:=
      match l with
      | nil =>  id
      | e::nil => e
      |  x  => let res := split x in
               let f1 := to_formulas id mk (fst res) in
               let f2 := to_formulas id mk (snd res) in
               mk f1 f2
      end.
    Next Obligation.
      rewrite MORPH.
      rewrite COMM. rewrite <- MORPH. auto.
    Qed.
    Next Obligation.
        apply split_lt_split.
        destruct l; try congruence.
        destruct l.
        specialize (H b). congruence.
        simpl. lia.
      Qed.
      Next Obligation.
        apply split_lt_split.
        destruct l; try congruence.
        destruct l.
        specialize (H b). congruence.
        simpl. lia.
      Qed.
      Next Obligation.
        assert (SPLT:= split_lt_split l).
        unfold split_lt in SPLT.
        assert (HLEN : length l >= 2).
        {
          destruct l; simpl; try congruence.
          specialize (n b).
          destruct l; simpl; try congruence.
          lia.
        }
        apply SPLT in HLEN.
        destruct HLEN.
        destruct (to_formulas B id mk (fst (split l)) (to_formulas_func_obligation_3 B l to_formulas l (conj n n0) eq_refl)).
        destruct (to_formulas B id mk (snd (split l)) (to_formulas_func_obligation_4 B l to_formulas l (conj n n0) eq_refl)).
        simpl.
        rewrite MORPH.
        erewrite <- e; eauto.
        erewrite <- e0; eauto.
        rewrite <- MORPH.
        rewrite <- fold_right_app_assoc with (MK := MK);auto.
        apply fold_right_in with (MK := MK);auto.
        apply in_split; auto.
        intros.
        rewrite ! MORPH.
        rewrite ASSOC. auto.
      Qed.
      Next Obligation.
        intuition congruence.
      Defined.

      Definition idem_form (o:op) : @formula A :=
        match o with
        | AND => TT
        | OR  => FF
        end.

      Definition of_op (o:op) : @formula A -> @formula A -> @formula A:=
        match o with
        | AND => And
        | OR  => Or
        end.

   Fixpoint to_formula (f:form) :=
     match f with
     | ATOM b a => if b then Atom a else Not (Atom a)
     | OP o l  =>
       proj1_sig (to_formulas (idem_form o) (of_op o)
         (List.map to_formula l))
     end.



   Lemma eval_formula_of_op : forall env o f1 f2,
       eval_formula env (of_op o f1 f2) = eval_op o (eval_formula env f1)
                                                  (eval_formula env f2).
   Proof.
     destruct o; simpl; auto.
   Qed.

   Lemma formula_dec : forall (EQ : forall x y:A, {x = y} + {x<> y}),
     forall x1 y : @formula A, {x1 = y} + {x1 <> y}.
   Proof.
     decide equality.
   Qed.

   Fixpoint to_formula_correct (env: A -> bool) (f:form):
       eval_form env f = eval_formula env (to_formula f).
   Proof.
     destruct f; simpl.
     - destruct b; simpl; auto.
     - set (F := (fun (e : form) (acc : bool) => eval_op o (eval_form env e) acc)).
       induction l.
       + simpl. destruct o; reflexivity.
       + simpl.
         rewrite IHl.
         assert (EQ : forall b, F a (eval_formula env b) = eval_formula env (of_op o (to_formula a) b)).
         {
           unfold F.
           intros.
           destruct o; simpl;auto.
           rewrite to_formula_correct.
           reflexivity.
           rewrite to_formula_correct.
           reflexivity.
         }
         rewrite EQ.
         generalize (map to_formula l) as L.
         intros.
         generalize (to_formula a) as a'.
         intros.
         destruct ((to_formulas (idem_form o) (of_op o) (a' :: L))).
         simpl.
         destruct ((to_formulas (idem_form o) (of_op o) L)).
         simpl.
         rewrite eval_formula_of_op.
         assert (FD :=  formula_dec).
         assert (ASS := eval_op_assoc).
         assert (ID : forall x1 : @formula A, eval_formula env (of_op o (idem_form o) x1) = eval_formula env x1).
         {
           destruct o; simpl; auto.
         }
         assert (OC := eval_op_comm).
         assert (ID' := eval_op_id).
         assert (EO  := eval_formula_of_op).
         erewrite <- e0 with (MK:= eval_op o); auto.
         erewrite <- e with (MK:= eval_op o); auto.
         simpl.
         rewrite eval_formula_of_op.
         reflexivity.
   Qed.

   Definition opt_formula (f:@formula A):=
     let (o,l) := flatten (of_form true f) in
     to_formula (OP o l).

   Lemma opt_formula_correct : forall env f,
       eval_formula env f = eval_formula env (opt_formula f).
   Proof.
     unfold opt_formula.
     intros.
     destruct (flatten (of_form true f)) eqn:FL.
     rewrite <- to_formula_correct.
     change o with (fst (o,l)).
     change l with (snd (o,l)) at 2.
     rewrite <- FL.
     change (OP (fst (flatten (of_form true f)))
               (snd (flatten (of_form true f)))) with
       (form_of_flatten (flatten (of_form true f))).
     rewrite <- flatten_correct.
     rewrite <- of_form_correct.
     reflexivity.
   Qed.

End S.
