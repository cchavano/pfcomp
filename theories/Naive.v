From compcert Require Import AST Cop Csyntax Ctypes Values Clightdefs.
Require Import Common Formula Filter Fdd2clight.
From compcert Require Errors.

Import ListNotations.

Definition bool2c (b : bool) : Csyntax.expr :=
  Csyntax.Eval (bool2val b) tbool.

Definition term2c (t : term) (sid : ident) (fields : field_env) : option Csyntax.expr := 
  match t with
  | ConstInt i => Some (Eval (Vint i) tint)
  | PacketField f =>
      do typ <- PMap.find f fields;
      Some (Efield
            (Ederef
              (Evar _packet (tptr (Tstruct sid noattr)))
              (Tstruct sid  noattr)) f typ)
  end.

Definition pred2c (p : pred) (sid : ident) (fields : field_env) : option Csyntax.expr :=
  let '(t1, c, t2) := p in
  do tc1 <- term2c t1 sid fields;
  do tc2 <- term2c t2 sid fields;
  Some (Ebinop (comp2binop c) tc1 tc2 tbool).

Fixpoint cond2c(c : cond) (sid : ident) (fields : field_env) : option Csyntax.expr :=
  match c with
  | TT => Some (bool2c true)
  | FF => Some (bool2c false)
  | Atom p => pred2c p sid fields
  | Not c' =>
      do e <- cond2c c' sid fields;
      Some (Eunop Onotbool e tbool)
  | And c d =>
      do ec <- cond2c c sid fields;
      do ed <- cond2c d sid fields;
      Some (Eseqand ec ed tbool)
  | Or c d =>
      do ec <- cond2c c sid fields;
      do ed <- cond2c d sid fields;
      Some (Eseqor ec ed tbool)
  | Ite _ _ _ => None
  end.

Definition action2c (a : action) : Csyntax.statement :=
  Sreturn (Some (if a then bool2c true else bool2c false)).

Fixpoint filter2c_naive_rec (pf : pfilter) (sid : ident) (fields : field_env) : option Csyntax.statement :=
  match pf with
  | [] => Some (action2c Reject)
  | (c, a) :: t =>
      do st <- filter2c_naive_rec t sid fields;
      do ec <- cond2c c sid fields;
      let ite := Sifthenelse ec (action2c a) Sskip in
      Some (Ssequence ite st)
  end.

Definition filter2c_naive (pf : pfilter) (id:ident) (mfields : members) : option Csyntax.program :=
  if wf_members_dec mfields then
    let fields := mk_field_env mfields in
    do body <- filter2c_naive_rec pf id fields;
    let f_filter := {|
                     fn_return := tbool;
                     fn_callconv := cc_default;
                     fn_params := [(_packet, (tptr (Tstruct id noattr)))];
                     fn_vars := [];
                     fn_body := body;
                   |} in
    let global_definitions := [(_filter, Gfun(Internal f_filter))] in
    let mk_res := Ctypes.make_program [Composite id Struct mfields noattr] global_definitions public_idents _main in
    match mk_res with
    | Errors.Error e => None
    | Errors.OK p  => Some p
    end
  else None.
