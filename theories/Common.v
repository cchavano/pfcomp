(** This file is coming from https://github.com/braibant/hash-consing-coq and has been modified. *)

(** Pot-pourri of definitions that are useful in other parts of the development. *)

Require Eqdep.

Ltac split_and :=
  repeat
    match goal with
    | |- ?X /\ ?Y => split
    end.

Ltac destruct_and :=
  repeat
    match goal with
    | H : ?X /\ ?Y |- _ =>
        let h1 := fresh H in
        let h2 := fresh H in
        destruct H as (h1 & h2)
    end.


Definition bind {A B: Type} (f: option A) (g: A -> option B) : option B :=
  match f with
    | Some x => g x
    | None => None
  end.

Definition bind2 {A B C: Type} (f: option (A * B)) (g: A -> B -> option C) : option C :=
  match f with
  | Some (x, y) => g x y
  | None => None
  end.

Definition bind3 {A B C D: Type} (f: option (A * B * C)) (g: A -> B -> C -> option D) : option D :=
  match f with
  | Some (x, y, z) => g x y z
  | None => None
  end.


Definition bind4 {A B C D E: Type} (f: option (A * B * C *D)) (g: A -> B -> C -> D -> option E) : option E :=
  match f with
  | Some (x, y , z , t) => g x y z t
  | None => None
  end.


Remark bind_inversion:
  forall (A B: Type) (f: option A) (g: A -> option B) (y: B),
  bind f g = Some y ->
  {x | f = Some x /\ g x = Some y}.
Proof. 
  intros; destruct f.  simpl in H.
  exists a; auto. 
  discriminate. 
Qed. 

Remark bind_inversion_None {A B} x (f: A -> option B) : bind x f = None -> 
  (x = None) + (exists y, x = Some y /\ f y = None).
Proof. 
  destruct x; simpl; intuition.
  right. exists a; intuition.  
Qed. 

Remark bind2_inversion:
  forall {A B C: Type} (f: option (A*B)) (g: A -> B -> option C) (y: C),
    bind2 f g = Some y ->
      {x1 : A & {x2 : B | f = Some (x1,x2) /\ g x1 x2 = Some y}}.
Proof. 
  intros ? ? ? [ [x y] | ] ? ? H; simpl in H; eauto.
  discriminate. 
Qed.

Notation "'do' X <- A ; B" := (bind A (fun X => B) )
  (at level 200, X ident, A at level 100, B at level 200). 

Notation "'do' X , Y  <- A ; B" := (bind2 A (fun X Y => B))
 (at level 200, X ident, Y ident, A at level 100, B at level 200).

Notation "'do' X , Y , Z  <- A ; B" := (bind3 A (fun X Y Z => B))
                                         (at level 200, X ident, Y ident, Z ident, A at level 100, B at level 200).

Notation "'do' X , Y , Z , T <- A ; B" := (bind4 A (fun X Y Z T => B))
                                            (at level 200, X ident, Y ident, Z ident, T ident, A at level 100, B at level 200).



Notation "'check' A ; B" := (if A then B else None)
  (at level 200, A at level 100, B at level 200). 

Ltac invert_do H :=
  match type of H with
    | (Some _ = Some _) =>
        inversion H; clear H; try subst
    | (None = Some _) =>
        discriminate
    | (bind ?F ?G = Some ?X) => 
        let x := fresh "x" in
          let EQ1 := fresh "EQ" in
            let EQ2 := fresh "EQ" in
              destruct (bind_inversion _ _ F G _ H) as [x [EQ1 EQ2]];
        clear H;
        try (invert_do EQ2)
    | (bind ?x ?f = None) => 
        let EQ := fresh "EQ" in 
        let EQ1 := fresh "EQ" in
        let EQ2 := fresh "EQ" in
        let x' := fresh "x" in 
        destruct (bind_inversion_None x f H) as [EQ | [x' [EQ1 EQ2]]];
        clear H;
        try (invert_do EQ1);
        try (invert_do EQ2);
        try (invert_do EQ)
  | (bind2 ?X (fun X1 X2 => _) = Some _) =>
      let f := fresh "EQ" in
      let v1 := fresh X1 in
      let v2 := fresh X2 in
      destruct X as [(v1,v2)|] eqn:f; simpl in H ; try discriminate
  end.

Ltac invert_do2 H :=
  match type of H with
    | (Some _ = Some _) =>
        inversion H; clear H
    | (None = Some _) =>
        discriminate
    | (bind ?F ?G = Some ?X) => 
        let x := fresh "x" in
          let EQ1 := fresh "EQ" in
            let EQ2 := fresh "EQ" in
              destruct (bind_inversion _ _ F G _ H) as [x [EQ1 EQ2]];
        clear H;
        try (invert_do2 EQ2)
    | (bind ?x ?f = None) => 
        let EQ := fresh "EQ" in 
        let EQ1 := fresh "EQ" in
        let EQ2 := fresh "EQ" in
        let x' := fresh "x" in 
        destruct (bind_inversion_None x f H) as [EQ | [x' [EQ1 EQ2]]];
        clear H;
        try (invert_do2 EQ1);
        try (invert_do2 EQ2);
        try (invert_do2 EQ)
  end.
  
Ltac simpl_do := 
    repeat match goal with 
      | H : Some _ = Some _ |- _ => injection H; clear H; intros; subst 
      | H : (None = Some _) |- _ => discriminate
      | H : (Some _ = None) |- _ => discriminate
      | H : None = None |- _ => clear H
      | H : (bind ?F ?G = Some ?X) |- _ => 
        destruct (bind_inversion _ _ F G _ H) as [? [? ?]]; clear H
      | H : (bind2 ?F ?G = Some ?X) |- _ => 
        destruct (bind2_inversion F G _ H) as [? [? [? ?]]]; clear H
      | |- context [(bind (Some _) ?G)] => simpl
      | H : (bind ?x ?f = None) |- _ => 
        let EQ := fresh in 
        destruct (bind_inversion_None x f H) as [EQ | [? [EQ ?]]]; 
          rewrite EQ in H; simpl in H
                                                  
      | H : ?x = Some ?y |- context [?x] => rewrite H
    end. 

(*Ltac intro_do n H :=
  match goal with 
    | |- context [do _ <- ?x; _] =>
      destruct x as [n|] eqn:H; simpl 
  end.
*)

(** The dependent type swiss-knife. *)
Ltac injectT :=  subst; repeat match goal with 
                                   H : existT _ _ _ = existT _ _ _ |- _ => 
                                   apply Eqdep.EqdepTheory.inj_pair2 in H
                                 |   H : context [eq_rect ?t _ ?x ?t ?eq_refl] |- _ => 
                                     rewrite <- Eqdep.EqdepTheory.eq_rect_eq in H
                                 |   H : context [eq_rect ?t _ ?x ?t ?H'] |- _ => 
                                     rewrite (Eqdep.EqdepTheory.UIP_refl _ _ H') in H;
                                       rewrite <- Eqdep.EqdepTheory.eq_rect_eq in H
                                 |   H : existT _ ?t1 ?x1 = existT _ ?t2 ?x2 |- _ => 
                                     let H' := fresh "H'" in 
                                     assert (H' := EqdepFacts.eq_sigT_fst H); subst
                               end; subst.

Ltac inject H :=
      injection H; clear H; intros; subst. 


(* Added *)
Ltac inv H := inversion H; clear H; subst.

Require Import List.

Fixpoint xnodup {A: Type} (eq_dec: forall x y : A, {x = y} + {x <> y}) (acc: list A) (l: list A) : list A :=
  match l with
  | nil => acc
  | e::l => if in_dec eq_dec e acc then xnodup eq_dec acc l else xnodup eq_dec (e::acc) l
  end.

Definition nodup_opt {A: Type} (eq_dec: forall x y : A, {x = y} + {x <> y})  (l: list A) : list A :=
  xnodup eq_dec nil l.

Lemma in_nodup_opt : forall {A: Type} eq_dec (l:list A),
    forall x, In x l -> In x (nodup_opt eq_dec l).
Proof.
  unfold nodup_opt.
  intros.
  assert (ACC : forall acc x,
             In x l \/ In x acc ->
             In x (xnodup eq_dec acc l)).
  {
    clear x H.
    induction l ; simpl.
    - tauto.
    - intros.
      rewrite or_assoc in H.
      destruct H.
      + subst.
        destruct (in_dec eq_dec x acc).
        apply IHl. tauto.
        apply IHl.
        simpl. tauto.
      + destruct (in_dec eq_dec a acc).
        apply IHl. tauto.
        apply IHl. simpl. tauto.
  }
  apply ACC. tauto.
Qed.

Fixpoint rev_app {A : Type} (l1 l2 : list A) : list A :=
  match l1 with
  | nil => l2
  | e1::l1 => rev_app l1 (e1::l2)
  end.

Lemma in_rev_app : forall {A: Type} (l1 l2: list A),
    forall x, In x (rev_app l1 l2) <-> (In x l1 \/ In x l2).
Proof.
  induction l1.
  - simpl. tauto.
  - simpl.
    intros.
    rewrite IHl1.
    simpl. tauto.
Qed.

Definition rev_map {A B: Type} (F: A -> B) (l : list A) :=
  List.fold_left (fun acc e => (F e)::acc) l nil.


Lemma rev_map_cons : forall {A B: Type} (F : A -> B) l e,
    rev_map F (e::l) = (rev_map F l) ++ F e :: nil.
Proof.
  unfold rev_map.
  intros A B F l.
  simpl. intro.
  set (G := (fun (acc : list B) (e0 : A) => F e0 :: acc)).
  generalize (F e :: nil)  as acc .
  induction l; simpl.
  - reflexivity.
  - intros.
    rewrite IHl.
    rewrite (IHl (G nil a)).
    rewrite <- app_assoc.
    reflexivity.
Qed.

Lemma in_rev_map : forall {A B: Type} (F : A -> B) l,
    forall x, In x (rev_map F l) <-> exists y, In y l /\ F y = x.
Proof.
  induction l.
  - simpl. firstorder.
  - intros. rewrite rev_map_cons.
    rewrite in_app_iff.
    simpl. firstorder congruence.
Qed.


Definition rev_map_filter {A B: Type} (F: A -> option B) (l : list A) :=
  List.fold_left (fun acc e =>
                    match F e with
                    | None => acc
                    | Some v => v::acc
                    end) l nil.

Lemma in_rev_map_filter : forall {A B: Type} (F : A -> option B) l,
    forall x, In x (rev_map_filter F l) <-> exists y, In y l /\ F y = Some x.
Proof.
  unfold rev_map.
  intros A B F.
  unfold rev_map_filter.
  set (G := (fun (acc : list B) (e : A) => match F e with
                                                 | Some v => v :: acc
                                                 | None => acc
                                                 end)).
  assert (forall  (l : list A) acc (x : B), In x (fold_left G l acc) <-> ((exists y : A, In y l /\ F y = Some x) \/ In x acc)).
  {
    induction l.
    - simpl. firstorder congruence.
    - intros.
      simpl.
      rewrite IHl.
      unfold G.
      destruct (F a) eqn:Fa.
      intuition (subst; simpl in *; try firstorder congruence).
      intuition (subst; simpl in *; try firstorder congruence).
      intuition (subst; simpl in *; try firstorder congruence).
  }
  intros.
  rewrite H. simpl. tauto.
Qed.


Fixpoint xadd_to_group {A B:Type} (eqB: forall x y,{x = y} + {x <> y}) (k:B) (e:A) (acc: list (B * list A)) (l: list (B * list A)):=
  match l with
  | nil => (k,e::nil)::acc
  | (b,lb)::l => if eqB k b then rev_app l ((b,e::lb)::acc) else xadd_to_group eqB k e ((b,lb)::acc) l
  end.

Definition add_to_group {A B:Type} (eqB: forall x y,{x = y} + {x <> y}) (k:B) (e:A) (l: list (B * list A)):=
  xadd_to_group eqB k e nil l.

Fixpoint xgroup_by {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B) (acc: list (B * list A)) (l: list A) : list (B * list A) :=
  match l with
  | nil => acc
  | e::l => xgroup_by eqB F (add_to_group eqB (F e) e acc) l
  end.

Definition group_by {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B)  (l: list A) : list (B * list A) :=
  xgroup_by eqB F nil l.

Definition distr {A B: Type} (k:B) (l: list A) := List.map (fun x => (k,x)) l.

Definition flatten_groups {A B: Type} (l : list (B * list A)) :=
  List.concat (List.map (fun '(x,lx) => distr x lx) l).

Lemma flatten_groups_cons : forall {A B: Type} (k:B) (lk:list A) l,
    flatten_groups ((k, lk) :: l) =
      distr k lk ++ flatten_groups l.
Proof. reflexivity. Qed.

Lemma flatten_groups_app : forall {A B: Type} (l1 l2: list (B * list A)),
    flatten_groups (l1 ++ l2) =
      flatten_groups l1 ++ flatten_groups l2.
Proof.
  unfold flatten_groups.
  intros.
  rewrite! map_app.
  rewrite concat_app.
  reflexivity.
Qed.


Lemma NoDup_app1 : forall {A: Type} (l1 l2:list A),
    NoDup l1 -> NoDup l2 ->
    (forall x, In x l1 -> In x l2 -> False) ->
    NoDup (l1 ++l2).
Proof.
  intros *.
  intro ND. revert l2.
  induction ND.
  - simpl;auto.
  - simpl ;intros.
    constructor; auto.
    rewrite in_app_iff.
    intro. firstorder.
    firstorder.
Qed.

Lemma NoDup_app2 : forall {A: Type} (l1 l2:list A),
    NoDup (l1 ++l2) ->
    NoDup l1 /\ NoDup l2 /\
    (forall x, In x l1 -> In x l2 -> False).
Proof.
  induction l1.
  - simpl. intros.
    split_and;auto.
    constructor.
  - intros.
    inv H.
    apply IHl1 in H3.
    destruct_and.
    rewrite in_app_iff in H2.
    split_and; auto.
    constructor;auto.
    simpl. firstorder congruence.
Qed.

Lemma NoDup_app_iff : forall {A: Type} (l1 l2:list A),
    NoDup (l1 ++l2) <-> NoDup (l2 ++ l1).
Proof.
  split; intros.
  - apply NoDup_app2 in H.
    destruct_and.
    apply NoDup_app1;split_and;auto.
    firstorder.
  - apply NoDup_app2 in H.
    destruct_and.
    apply NoDup_app1;split_and;auto.
    firstorder.
Qed.

Lemma NoDup_swap1 : forall {A: Type} (l1 l2 l3:list A),
    NoDup (l1++l2 ++l3) -> NoDup (l2 ++l1 ++ l3).
Proof.
  intros.
  apply NoDup_app2 in H.
  destruct_and.
  apply NoDup_app2 in H2.
  destruct_and.
  apply NoDup_app1;split_and;auto.
  apply NoDup_app1;split_and;auto.
  intros.
  eapply H3;eauto. rewrite in_app_iff. tauto.
  intros. rewrite in_app_iff in H4.
  destruct H4. eapply H3;eauto.
  rewrite in_app_iff. tauto.
  eapply H5;eauto.
Qed.

Lemma NoDup_swap : forall {A: Type} (l1 l2 l3:list A),
    NoDup (l1++l2 ++l3) <-> NoDup (l2 ++l1 ++ l3).
Proof.
  split;intros.
  apply NoDup_swap1;auto.
  apply NoDup_swap1;auto.
Qed.

Lemma NoDup_rev : forall {A: Type} (l1 l2:list A),
    NoDup (l1 ++l2) <-> NoDup (rev l1++  l2).
Proof.
  induction l1.
  - intros.
    simpl. tauto.
  -  simpl.
     intros.
     rewrite <- app_assoc.
     rewrite <- IHl1.
     rewrite NoDup_app_iff.
     simpl.
     change (a ::l1++l2) with ((a::nil) ++ l1 ++l2).
     change (a ::l2++l1) with ((a::nil) ++ l2 ++l1).
     rewrite NoDup_swap.
     rewrite NoDup_app_iff.
     rewrite app_assoc.
     tauto.
Qed.


Lemma rev_app_rev : forall {A: Type} (l1 l2: list A),
    rev_app l1 l2 = rev l1 ++l2.
Proof.
  induction l1; simpl.
  - intros. reflexivity.
  - intros. rewrite IHl1.
    rewrite <- app_assoc.
    simpl. reflexivity.
Qed.

Lemma NoDup_flatten_groups_app : forall {A B: Type}  (l2 l1 : list (B * list A)),
    NoDup (flatten_groups (l2 ++ l1)) ->
    NoDup (flatten_groups (l1 ++ l2)).
Proof.
  intros.
  rewrite flatten_groups_app in*.
  rewrite NoDup_app_iff.
  tauto.
Qed.

Lemma NoDup_flatten_groups_app_iff : forall {A B: Type}  (l2 l1 : list (B * list A)),
    NoDup (flatten_groups (l2 ++ l1)) <->
    NoDup (flatten_groups (l1 ++ l2)).
Proof.
  split;intros;
  apply NoDup_flatten_groups_app;auto.
Qed.


Lemma in_flatten_goups_rev1 : forall {A B: Type} x (l:list (A * list B)),
    In x (flatten_groups (rev l)) ->
    In x (flatten_groups l).
Proof.
  unfold flatten_groups.
  intros.
  rewrite! in_concat in *.
  destruct H as (v & (INM & IN)).
  rewrite in_map_iff in INM.
  destruct INM as ((k,lk) & D & IN').
  subst. eexists; split_and ;eauto.
  rewrite in_map_iff. exists (k,lk); split; auto.
  rewrite in_rev  ;auto.
Qed.

Lemma in_flatten_goups_rev2 : forall {A B: Type} x (l:list (A * list B)),
    In x (flatten_groups l) ->
    In x (flatten_groups (rev l)).
Proof.
  unfold flatten_groups.
  intros.
  rewrite! in_concat in *.
  destruct H as (v & (INM & IN)).
  rewrite in_map_iff in INM.
  destruct INM as ((k,lk) & D & IN').
  subst. eexists; split_and ;eauto.
  rewrite in_map_iff. exists (k,lk); split; auto.
  rewrite <- in_rev  ;auto.
Qed.

Lemma in_flatten_goups_rev_iff : forall {A B: Type} x (l:list (A * list B)),
    In x (flatten_groups (rev l)) <->
    In x (flatten_groups l).
Proof.
  split ; intros.
  apply in_flatten_goups_rev1;auto.
  apply in_flatten_goups_rev2;auto.
Qed.


Lemma flatten_groups_nil : forall {A B: Type}, flatten_groups (@nil (A * list B)) = nil.
Proof.
  reflexivity.
Qed.

Lemma NoDup_flatten_groups_rev_iff : forall {A B: Type} (l: list (A * list B)),
    NoDup (flatten_groups l) <->
    NoDup (flatten_groups (rev l)).
Proof.
  induction l; simpl.
  -  rewrite flatten_groups_nil. tauto.
  - intros.
    destruct a as (k,lk).
    rewrite flatten_groups_cons in *.
    rewrite flatten_groups_app.
    rewrite flatten_groups_cons in *.
    rewrite flatten_groups_nil.
    rewrite <- app_nil_end.
    rewrite NoDup_app_iff.
    split;intros;
    apply NoDup_app2 in H;
      destruct_and;
      apply NoDup_app1; try tauto.
    intros.
    rewrite in_flatten_goups_rev_iff in H.
    eauto.
    intros.
    rewrite <- in_flatten_goups_rev_iff in H.
    eauto.
Qed.



Lemma xadd_to_group_NoDup :
  forall {A B:Type} (eqB: forall x y,{x = y} + {x <> y})  (k:B) (e:A) (l acc:list (B * list A))
         (ND : NoDup (flatten_groups (l++acc)))
         (NOTIN : Forall (fun x => x <> (k,e)) (flatten_groups (l++acc))),
    NoDup (flatten_groups (xadd_to_group eqB k e acc l)).
Proof.
  induction l.
  -
    intros.
    simpl.
    rewrite flatten_groups_cons.
    simpl.
    rewrite flatten_groups_app in ND.
    rewrite flatten_groups_nil in ND.
    simpl in ND.
    rewrite Forall_forall in NOTIN.
    constructor;auto.
    intro.
    eapply NOTIN;eauto.
  - simpl; intros.
    destruct a as (b,lb).
    destruct (eqB k b); subst.
    + rewrite flatten_groups_cons in ND.
      rewrite flatten_groups_app in ND.
      rewrite rev_app_rev.
      rewrite NoDup_flatten_groups_app_iff.
      simpl.
      rewrite flatten_groups_cons.
      rewrite flatten_groups_app.
      simpl.
      constructor.
      { rewrite Forall_forall in NOTIN.
      intro. eapply NOTIN; eauto.
      rewrite in_app_iff in *.
      rewrite flatten_groups_cons.
      rewrite in_app_iff. rewrite flatten_groups_app in *.
      rewrite in_app_iff in *.
      intuition auto.
      rewrite in_flatten_goups_rev_iff in H.
      tauto.
      }
      rewrite NoDup_app_iff.
      rewrite NoDup_app_iff in ND.
      apply NoDup_app2 in ND.
      destruct_and.
      apply NoDup_app1;auto.
      {
        apply NoDup_app2 in ND0.
        destruct_and.
        apply NoDup_app1;auto.
        rewrite <- NoDup_flatten_groups_rev_iff;auto.
        intros. rewrite in_flatten_goups_rev_iff in H0.
        eauto.
      }
      intros. eapply ND3;eauto.
      rewrite in_app_iff in *. rewrite in_flatten_goups_rev_iff in H.
      tauto.
    + eapply IHl.
      rewrite flatten_groups_app.
      rewrite flatten_groups_cons.
      rewrite NoDup_swap.
      rewrite flatten_groups_cons in ND. rewrite flatten_groups_app in ND.
      tauto.
      rewrite Forall_forall in *.
      intros.
      eapply NOTIN.
      rewrite flatten_groups_cons.
      rewrite flatten_groups_app in *.
      rewrite flatten_groups_cons in H.
      rewrite! in_app_iff in *.
      tauto.
  Qed.

Lemma xadd_to_group_In :
  forall {A B:Type} (eqB: forall x y,{x = y} + {x <> y})  (k:B) (e:A) (l acc:list (B * list A)),
    forall x,
    In x (flatten_groups (xadd_to_group eqB k e acc l)) <-> (In x (flatten_groups (acc ++ l)) \/ x = (k,e)).
Proof.
  induction l.
  - simpl. intros.
    rewrite flatten_groups_app. rewrite flatten_groups_nil.
    rewrite <- app_nil_end. unfold flatten_groups.
    intuition congruence.
  - simpl.
    destruct a as (k',lk).
    intros.
    destruct (eqB k k'); subst.
    rewrite rev_app_rev.
    rewrite! flatten_groups_app.
    rewrite! flatten_groups_cons.
    rewrite! in_app_iff.
    simpl. rewrite in_flatten_goups_rev_iff.
    intuition congruence.
    rewrite IHl.
    rewrite! flatten_groups_app.
    rewrite! flatten_groups_cons.
    rewrite! in_app_iff.
    intuition congruence.
Qed.



Lemma xgroup_by_NoDup :
  forall {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B) l acc
         (ND : NoDup ((List.map (fun x => (F x,x)) l) ++ flatten_groups acc))
  ,
    NoDup (flatten_groups (xgroup_by eqB F acc l)).
Proof.
  induction l.
  - simpl. auto.
  - intros.
    simpl.
    apply IHl;auto.
    simpl in ND.
    set (L := map (fun x : A => (F x, x)) l) in *.
    change ((F a,a)::(L++flatten_groups acc)) with (((F a , a)::nil) ++ L ++ flatten_groups acc) in ND.
    apply NoDup_swap in ND.
    apply NoDup_app2 in ND.
    destruct_and.
    apply NoDup_app1; auto.
    eapply xadd_to_group_NoDup.
    rewrite <- app_nil_end.
    apply NoDup_app2 in ND2; auto.
    tauto.
    rewrite flatten_groups_app.
    rewrite flatten_groups_nil. rewrite <- app_nil_end.
    apply NoDup_app2 in ND2.
    destruct_and.
    rewrite Forall_forall.
    repeat intro. subst. eapply ND5;eauto.
    simpl. tauto.
    intros.
    unfold add_to_group in H0.
    erewrite xadd_to_group_In    in H0.
    simpl in H0.
    eapply ND3;eauto.
    rewrite in_app_iff. simpl. intuition congruence.
Qed.

Lemma group_by_NoDup :
  forall {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B) l
         (ND : NoDup l)
  ,
    Forall (fun x => NoDup (snd x)) (group_by eqB F l).
Proof.
  intros.
  assert (ND' : NoDup ((List.map (fun x => (F x,x)) l) ++ flatten_groups nil)).
  {
    rewrite flatten_groups_nil.
    rewrite <- app_nil_end.
    induction ND.
    -  constructor.
    - simpl. constructor; auto.
      intro.
      rewrite in_map_iff in H0. destruct H0 as (v & EQ & IN).
      intuition congruence.
  }
  eapply xgroup_by_NoDup with (eqB:=eqB) in ND'.
  unfold group_by.
  revert ND'.
  generalize (xgroup_by eqB F nil l) as L.
  induction L.
  - constructor.
  -   intros.
      destruct a as (k,lk).
      rewrite flatten_groups_cons in ND'.
      apply NoDup_app2 in ND'.
      destruct_and.
      constructor;auto.
      simpl.
      clear - ND'0.
      unfold distr in ND'0.
      apply NoDup_map_inv in ND'0;auto.
Qed.

Fixpoint rev_map_app {A B: Type} (F : A -> option B) (l1 : list A) (l2: list B) :=
  match l1 with
  | nil => l2
  | e::l1 => match F e with
             | None => rev_map_app F l1 l2
             | Some e' => rev_map_app F l1 (e' :: l2)
             end
  end.


Fixpoint xall_pairs
  {A B C: Type}  (P : A ->  B -> option C) (acc: list C) (l1 : list A) (l2 : list B) : list C :=
  match l1 with
  | nil => acc
  | e ::l =>  (xall_pairs P (rev_map_app (P e) l2 acc) l l2)
  end.


Definition all_pairs
  {A B C: Type}  (P : A ->  B -> option C) (l1 : list A) (l2 : list B) : list C :=
  xall_pairs P nil l1 l2.


Fixpoint xall_pairs_sym {A B: Type} (P: A -> A -> option B) (acc:list B) (l : list A) : list B :=
  match l with
  | nil => acc
  | e ::l' =>  xall_pairs_sym P (rev_map_app (P e) l' acc) l'
  end.


Definition all_pairs_sym {A B: Type} (P: A -> A -> option B) (l : list A) : list B :=
  xall_pairs_sym P nil l.


Lemma in_rev_map_app : forall {A B: Type} (F : A -> option B) l1 l2,
    forall x,
      In x (rev_map_app F l1  l2) ->
      In x l2 \/ exists x1, In x1 l1 /\ F x1 = Some x.
Proof.
  induction l1;simpl.
  - tauto.
  - intros.
    destruct (F a) eqn:FA.
    apply IHl1 in H.
    simpl in H.
    intuition subst.
    + right.
      exists a. intuition.
    + destruct H0 as (x1 & IN & FX).
      right. exists x1. tauto.
    + apply IHl1 in H.
      destruct H;try tauto.
      destruct H as (x1 & IN & FX).
      right.
      exists x1. tauto.
Qed.


Lemma In_xall_pairs : forall {A B C:Type} (P: A -> B -> option C) l1 acc l2,
  forall x, List.In x (xall_pairs P acc l1 l2) ->
            In x acc \/ exists x1 x2, In x1 l1 /\ In x2 l2 /\ P x1 x2 = Some x.
Proof.
  induction l1; simpl.
  -  tauto.
  - intros.
    apply IHl1 in H.
    destruct H as [H|(x1&x2&IN1 & IN2 & PX)].
    apply in_rev_map_app in H.
    destruct H as [H|H].
    tauto.
    destruct H as (x1 & IN1 & P1).
    right.
    exists a,x1.
    tauto.
    right.
    exists x1,x2. tauto.
Qed.


Lemma In_all_pairs : forall {A B C:Type} (P: A -> B -> option C) l1 l2,
  forall x, List.In x (all_pairs P l1 l2) ->
            exists x1 x2, In x1 l1 /\ In x2 l2 /\ P x1 x2 = Some x.
Proof.
  intros.
  apply In_xall_pairs in H.
  simpl in H. tauto.
Qed.


Lemma In_xall_pairs_sym : forall {A B:Type} (P: A -> A -> option B) l acc,
  forall x, List.In x (xall_pairs_sym P acc l) ->
            In x acc \/
              (exists x1 x2, In x1 l /\ In x2 l /\ P x1 x2 = Some x).
Proof.
  induction l; simpl.
  -  tauto.
  - intros.
    apply IHl in H.
    destruct H as [H|H].
    apply in_rev_map_app in H.
    destruct H as [H|H].
    tauto.
    destruct H as (x1 & IN1 & P1).
    right.
    exists a,x1.
    tauto.
    destruct H as (x1&x2&IN1 & IN2 & PX).
    right.
    exists x1,x2. tauto.
Qed.


Lemma In_all_pairs_sym : forall {A B:Type} (P: A -> A -> option B) l,
  forall x, List.In x (all_pairs_sym P l) ->
            exists x1 x2, In x1 l /\ In x2 l /\ P x1 x2 = Some x.
Proof.
  intros.
  apply In_xall_pairs_sym in H.
  simpl in H. tauto.
Qed.



Lemma Forall_rev_app : forall {A: Type} (P : A -> Prop) l acc,
    Forall P l ->
    Forall P acc ->
    Forall P (rev_app l acc).
Proof.
  intros.
  rewrite Forall_forall in *.
  intros.
  rewrite in_rev_app in H1.
  intuition.
Qed.


Definition is_group_by {A B: Type} (F: A -> B) (l: list (B * list A)) :=
  Forall (fun '(b,lb) => Forall (fun x => F x = b) lb) l.


Lemma xgroup_by_correct : forall {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B) l acc,
    is_group_by F acc ->
    is_group_by F (xgroup_by eqB F acc l).
Proof.
  induction l; simpl.
  - auto.
  - intros.
    apply IHl.
    { clear IHl l.
      unfold add_to_group.
      set (G := (fun (acc0 : list (B * list A)) '(b, lb) => if eqB (F a) b then (b, a :: lb) :: acc0 else (b, lb) :: acc0)).
      assert (ACC : is_group_by F nil).
      { constructor. }
      rename acc into l.
      revert ACC. generalize (@nil (B * list A)) as acc.
      induction l.
      - simpl. constructor.
        constructor. reflexivity. constructor.
        auto.
      - simpl. intros.
        destruct a0 as (k,lk).
        destruct (eqB (F a) k).
        + subst.
          unfold is_group_by.
          apply Forall_rev_app.
          inv H;auto.
          constructor;auto.
          inv H;auto.
        +
          apply IHl;auto.
          inv H;auto.
          constructor;auto.
          inv H; auto.
    }
Qed.

Lemma group_by_correct : forall {A B: Type} (eqB: forall x y,{x = y} + {x <> y}) (F: A -> B) l,
    is_group_by F (group_by eqB F l).
Proof.
  intros.
  apply xgroup_by_correct.
  constructor.
Qed.

Require Import BinPos BinNat Lia.

Lemma positive_strong_ind:
  forall (P:positive->Prop),
    (forall n, (forall m, (m < n)%positive -> P m) -> P n) ->
    forall n, P n.
Proof.
  intros P HP.
  assert (forall n m, (m < n)%positive -> P m).
  induction n using Pos.peano_ind; intros.
  lia.
  apply HP. intros.
  apply IHn. lia.
  intros. apply (H (Pos.succ n)).
  lia.
Qed.

(** missing lemmas from the stdlib  *)

Lemma N_compare_sym: forall x y, N.compare y x = CompOpp (N.compare x y). 
Proof. intros. rewrite Ncompare_antisym. auto. Qed.

Lemma Pos_compare_sym: forall x y, Pos.compare y x = CompOpp (Pos.compare x y). 
Proof. exact Pos.compare_antisym. Qed. 

Lemma N_compare_trans x y z c :
  N.compare x y = c -> N.compare y z = c -> N.compare x z = c. 
Proof.
  destruct c.
  intros; rewrite N.compare_eq_iff in * |- *. congruence. 
  intros; rewrite N.compare_lt_iff in * |- *. lia.
  intros; rewrite N.compare_gt_iff in * |- *. lia.
Qed. 

Lemma Pos_compare_trans: forall c x y z, Pos.compare x y = c -> Pos.compare y z = c -> Pos.compare x z = c.
Proof. 
  destruct c.
  intros; rewrite Pos.compare_eq_iff in * |- *. congruence. 
  intros; rewrite Pos.compare_lt_iff in * |- *. lia.
  intros; rewrite Pos.compare_gt_iff in * |- *. lia.
Qed. 

#[export] Hint Resolve N_compare_sym N_compare_trans Pos_compare_sym Pos_compare_trans : compare.

(** Boilerplate definitions for FMaps. *)
Require Export FMapPositive FMapAVL OrderedTypeAlt.
Require Export FMapFacts. 



Module PMap := PositiveMap.
Module PMapFacts := FMapFacts.Facts(PMap).

Module Type MyOrderedTypeAlt.
 Parameter t : Type.

 Parameter compare : t -> t -> comparison.
 Infix "?=" := compare (at level 70, no associativity).

 Parameter compare_sym : forall x y, (y?=x) = CompOpp (x?=y).
 Parameter compare_trans : forall c x y z, (x?=y) = c -> (y?=z) = c -> (x?=z) = c.
 Parameter reflect : forall x y, x ?= y = Eq -> x = y.
End MyOrderedTypeAlt. 

 
Module PairOrderedTypeAlt(O U: MyOrderedTypeAlt) <: MyOrderedTypeAlt.
  Definition t := (O.t * U.t)%type.
  Definition compare (m n: t) :=
    let '(mo,mu) := m in
      let '(no,nu) := n in
        match O.compare mo no with
          | Eq => U.compare mu nu
          | r => r
        end.
  Lemma compare_sym: forall x y, compare y x = CompOpp (compare x y). 
  Proof.
    intros [x x'] [y y']; simpl.
    rewrite O.compare_sym, U.compare_sym. destruct (O.compare x y); reflexivity.
  Qed.
  Lemma compare_trans: forall c x y z, compare x y = c -> compare y z = c -> compare x z = c.
    intros c [x x'] [y y'] [z z']; simpl.
    case_eq (O.compare x y); case_eq (O.compare y z); intros xy yz; simpl;
      try rewrite (O.compare_trans _ _ _ _ yz xy); intros; subst; try discriminate; auto.
      eauto using U.compare_trans.
      apply O.reflect in yz; subst. rewrite xy. trivial.
      apply O.reflect in yz; subst. rewrite xy. trivial.
      apply O.reflect in xy; subst. rewrite yz. trivial.
      apply O.reflect in xy; subst. rewrite yz. trivial.
  Qed.
  Lemma reflect: forall x y, compare x y = Eq -> x = y.
  Proof.
    intros [x x'] [y y']. simpl.
    case_eq (O.compare x y); intro xy; try apply O.reflect in xy;
    case_eq (U.compare x' y'); intro xy'; try apply U.reflect in xy';
      intro; try discriminate; subst; auto.
  Qed.
End PairOrderedTypeAlt.

Module Pos_as_OTA <: OrderedTypeAlt.
  Definition t := positive.
  Fixpoint compare x y := 
    match x,y with 
      | xH, xH => Eq
      | xH, _ => Lt
      | _, xH => Gt
      | xO a, xO b => compare a b
      | xI a, xI b => compare a b
      | xI a, xO b => Gt
      | xO a, xI b => Lt
    end.
  Lemma compare_sym: forall x y, compare y x = CompOpp (compare x y). 
  Proof. induction x; intros y; destruct y; simpl; auto. Qed.
  Lemma compare_trans: forall c x y z, compare x y = c -> compare y z = c -> compare x z = c. 
  Proof. 
    intros c x. revert c.
    induction x; intros c y z; destruct y; simpl; intro H; auto; subst; try discriminate H;
      destruct z; simpl; intro H'; eauto; try discriminate H'.
  Qed.
  Lemma reflect: forall x y, compare x y = Eq -> x = y.
  Proof. 
    induction x; intros y; destruct y; simpl; intro H; auto; try discriminate.
    apply IHx in H. subst. reflexivity.
    apply IHx in H. subst. reflexivity.
  Qed.
End Pos_as_OTA.

Module PP := PairOrderedTypeAlt Pos_as_OTA Pos_as_OTA. 
Module PPO := OrderedType_from_Alt(PP).
Module PPMap := FMapAVL.Make PPO. 
Module PPMapFacts := FMapFacts.Facts (PPMap). 


(** Results about lexicographic order *)
Require Setoid.

Module Compare.

Import Setoid.

Notation lex x y := 
  (match x with 
    | Eq => y
    | c => c
  end). 
Section t. 
  Variable A B: Type. 
  Variable f : A -> A -> comparison.
  Variable g : B -> B -> comparison.
  Hypothesis Hf_sym: forall x y,  f x y = CompOpp (f y x). 
  Hypothesis Hf_trans:forall c x y z,  f x y = c -> f y z = c -> f x z = c. 
  Hypothesis Hg_sym: forall x y,  g x y = CompOpp (g y x). 


  Hint Resolve Hf_sym Hf_trans Hg_sym  : lex.
  Lemma lex_sym x1 x2 y1 y2: lex (f x1 x2) (g y1 y2) = CompOpp (lex (f x2 x1) (g y2 y1)).
  Proof. 
    repeat match goal with 
               |- context [f ?x ?y] => case_eq (f x y); intros ?
             | |- context [f ?x ?y] => case_eq (g x y); intros ?
             | H : f ?x ?y = _, H' : f ?y ?x = _ |- _ => 
               rewrite Hf_sym, H' in H; simpl in H; clear H'; try discriminate
           end; simpl; try eauto with lex. 
  Qed.

  Lemma CompOpp_move x y : CompOpp x = y <-> x = CompOpp y. 
    destruct x; destruct y; simpl; intuition discriminate.  
  Qed. 
  
  Lemma Hf_sym' c x y : f x y = CompOpp c -> f y x = c. 
  Proof.   
    intros. rewrite Hf_sym. rewrite CompOpp_move. assumption.
  Qed. 

  Hint Resolve Hf_sym' : lex. 

  Lemma lex_trans c x1 x2 x3 y1 y2 y3: 
    forall (Hg_trans:forall c,  g y1 y2 = c -> g y2 y3 = c -> g y1 y3 = c),
    lex (f x1 x2) (g y1 y2) = c -> 
    lex (f x2 x3) (g y2 y3) = c -> 
    lex (f x1 x3) (g y1 y3) = c.
  Proof. 
    Ltac finish :=
      repeat match goal with 
               | H : ?x = ?y, H' : ?x = ?z |- _ => constr_eq y z; clear H'
               | H : ?x = ?y, H' : ?x = ?z |- _ => 
                 rewrite H in H'; discriminate
             end; simpl; try eauto with lex. 

    repeat match goal with 
               |- context [f ?x ?y] => case_eq (f x y); intros ?
             | |- context [f ?x ?y] => case_eq (g x y); intros ?
             | H : f ?x ?y = _, H' : f ?y ?x = _ |- _ => 
               rewrite Hf_sym, H' in H; simpl in H; clear H'; try discriminate
             | H : f ?x ?y = _, H' : f ?y ?z = _ |- _ => 
               pose proof (Hf_trans _ _ _ _ H H'); clear H H'
           end; finish. 

    assert (f x2 x3 = Eq) by eauto with lex; finish. 
    assert (f x1 x2 = Gt) by eauto with lex; finish. 
    assert (f x2 x3 = Eq) by eauto with lex; finish. 
    assert (f x1 x2 = Lt) by eauto with lex; finish. 
    assert (f x1 x2 = Eq) by eauto with lex; finish. 
    assert (f x2 x3 = Gt) by eauto with lex; finish. 
    congruence. 
    assert (f x1 x2 = Eq) by eauto with lex; finish. 
    assert (f x2 x3 = Lt) by eauto with lex; finish. 
    congruence. 
  Qed. 
End t. 

End Compare.

(* -------------------------------------------------------------------------------- *)

(* When using well-founded recursion, it is useful to prefix the proof
of well-foundedness with 2^n Acc constructors *)

 Fixpoint guard A (R: A -> A -> Prop) n (wfR: well_founded R): well_founded R :=
   match n with 
     | 0 => wfR 
     | S n => fun x => Acc_intro x (fun y _ => guard _ _ n (guard _ _ n wfR) y) 
   end.

Definition wf A f :=
  guard _ _ 512 (Wf_nat.well_founded_ltof A f).

(* slight adaptation of Leroy's Wfsimpl.v in Compcert  *)
Require Import FunctionalExtensionality. 

Section FIX.

  Variables A B: Type.
  Variable R: A -> A -> Prop.
  Hypothesis Rwf: well_founded R.
  Set Implicit Arguments.
  Variable F: forall (x: A), (forall (y: A), R y x -> B) -> B.

  Definition Fix (x: A) : B := Wf.Fix (guard _ _ 512 Rwf) (fun (x: A) => B) F x.

  Theorem Fix_eq:
    forall x, Fix x = F (fun (y: A) (P: R y x) => Fix y).
  Proof.
    unfold Fix; intros. apply Wf.Fix_eq with (P := fun (x: A) => B). 
    intros. assert (f = g). apply functional_extensionality_dep; intros.
    apply functional_extensionality; intros. auto. 
    subst g; auto.
  Qed.

End FIX.

(** Same, with a nonnegative measure instead of a well-founded ordering *)

Section FIXM.

  Variables A B: Type.
  Variable measure: A -> nat.
  Variable F: forall (x: A), (forall (y: A), measure y < measure x -> B) -> B.
  
  Definition Fixm (x: A) : B := Wf.Fix (guard _ _ 512 (Wf_nat.well_founded_ltof A measure)) (fun (x: A) => B) F x.
  
  Theorem Fixm_eq:
    forall x, Fixm x = F (fun (y: A) (P: measure y < measure x) => Fixm y).
  Proof.
    unfold Fixm; intros. apply Wf.Fix_eq with (P := fun (x: A) => B). 
    intros. assert (f = g). apply functional_extensionality_dep; intros.
    apply functional_extensionality; intros. auto. 
    subst g; auto.
  Qed.

  Lemma Fixm_ind P:
    (forall x, (forall y, measure y < measure x -> P (Fixm y)) -> P (Fixm x)) ->
    forall x, P (Fixm x).
  Proof. 
    intros. 
    induction x using (well_founded_induction_type (Wf_nat.well_founded_ltof _ measure)).
    apply X. intros. apply X0. unfold Wf_nat.ltof. eauto.
  Qed.  

End FIXM.
