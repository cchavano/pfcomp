Require Import List Btauto.
Require Import Common.
Import ListNotations.

Set Implicit Arguments.

Inductive formula {A : Type} :=
  | TT
  | FF
  | Atom : A -> formula
  | Not  : formula -> formula
  | And  : formula -> formula -> formula
  | Or   : formula -> formula -> formula
  | Ite  : A -> formula -> formula -> formula.


Arguments TT {A}.
Arguments FF {A}.
Arguments Atom {A}.
Arguments Not {A}.
Arguments And {A}.
Arguments Or {A}.
Arguments Ite {A}.


Fixpoint eval_formula {A : Type} (eval_atom : A -> bool) (form : @formula A) : bool :=
  match form with
  | TT => true
  | FF => false
  | Atom a => eval_atom a
  | Not p => negb (eval_formula eval_atom p)
  | And p q => andb (eval_formula eval_atom p) (eval_formula eval_atom q)
  | Or p q => orb (eval_formula eval_atom p) (eval_formula eval_atom q)
  | Ite a p q => if eval_atom a then eval_formula eval_atom p else eval_formula eval_atom q
  end.

(* [dnf f] is a naive disjunctive normal form - for small formulae *)

Definition andl (A: Type) := list (bool * A).
Definition tt {A: Type} : list (andl A) := nil :: nil.
Definition ff {A: Type} : list (andl A) := nil.

Definition eval_lit {A:Type} (eval : A -> bool) (p: bool * A) : bool :=
  if (fst p) then eval (snd p) else negb (eval (snd p)).

Fixpoint eval_andl {A:Type} (eval : A -> bool) (c: andl A) : bool :=
  match c with
  | nil => true
  | l::c' => eval_lit eval l  && eval_andl eval c'
  end.

Fixpoint eval_dnf {A: Type} (eval: A -> bool) (l : list (andl A)) : bool :=
  match l with
  | nil => false
  | c::l => eval_andl eval c || eval_dnf eval l
  end.

Definition distr {A: Type} (c:andl A) (l : list (andl A)) :=
  rev_map (fun x => c++x) l.

Fixpoint dnf_and {A: Type} (l1 l2 : list (andl A)) : list (andl A) :=
  match l1 with
  | nil => ff
  | c::l1 => (distr c l2) ++ dnf_and l1 l2
  end.

Lemma eval_dnf_app : forall {A: Type} (eval : A -> bool) (l1 l2 : list (andl A)),
    eval_dnf eval (l1 ++ l2) = (eval_dnf eval l1) || (eval_dnf eval l2) .
Proof.
  induction l1; simpl; auto.
  intros. rewrite IHl1.
  rewrite orb_assoc. reflexivity.
Qed.

Lemma eval_andl_app : forall {A: Type} (eval : A -> bool) (l1 l2 : andl A),
    eval_andl eval (l1 ++ l2) = (eval_andl eval l1) && (eval_andl eval l2) .
Proof.
  induction l1; simpl; auto.
  intros. rewrite IHl1.
  rewrite andb_assoc. reflexivity.
Qed.


Lemma eval_distr : forall {A: Type} eval (e: andl A) l,
    eval_dnf eval (distr e l) = (eval_andl eval e) && eval_dnf eval l.
Proof.
  unfold distr.
  induction l.
  - simpl. rewrite andb_comm. reflexivity.
  - simpl. rewrite rev_map_cons.
    rewrite (eval_dnf_app eval _ [e++a]).
    simpl. rewrite IHl.
    rewrite orb_false_r.
    rewrite andb_orb_distrib_r.
    rewrite orb_comm. rewrite eval_andl_app.
    reflexivity.
Qed.

Lemma eval_dnf_and : forall {A: Type} eval (l1 l2 : list (andl A)),
    eval_dnf eval (dnf_and l1 l2) = (eval_dnf eval l1) && eval_dnf eval l2.
Proof.
  induction l1; simpl.
  - auto.
  - intros. rewrite (eval_dnf_app _ (distr a l2)).
    rewrite eval_distr.
    rewrite IHl1.
    btauto.
Qed.


Fixpoint dnf {A: Type} (pol: bool) (f:@formula A) : list (andl A) :=
  match f with
  | TT => if pol then tt else ff
  | FF => if pol then ff else tt
  | Atom a => ((pol,a)::nil)::nil
  | Not p  => dnf (negb pol) p
  | Or p q => if pol then (dnf pol p) ++ (dnf pol q)
              else dnf_and (dnf pol p) (dnf pol q)
  | And p q => if pol then dnf_and (dnf pol p) (dnf pol q)
              else (dnf pol p) ++ (dnf pol q)
  | Ite a f1 f2 => if pol
                   then dnf_and ( ((false,a):: nil) :: dnf pol f1)
                                     (((true,a):: nil) :: dnf pol f2)
                   else (distr ((true,a)::nil) (dnf pol f1))
                          ++
                          (distr ((false,a)::nil) (dnf pol f2))
  end.

Lemma eval_dnf_ok : forall {A: Type} eval (f:@formula A) pol,
    eval_dnf eval (dnf pol f) = if pol then eval_formula eval f
                                else negb (eval_formula eval f).
Proof.
  induction f; simpl.
  - destruct pol ; simpl; reflexivity.
  - destruct pol ; simpl; reflexivity.
  - intros. rewrite orb_false_r. rewrite andb_true_r.
    reflexivity.
  - intros. rewrite IHf.
    rewrite negb_involutive.
    rewrite if_negb. reflexivity.
  - intros.
    destruct pol.
    + rewrite eval_dnf_and.
    rewrite IHf1. rewrite IHf2. reflexivity.
    + rewrite eval_dnf_app.
      rewrite IHf1. rewrite IHf2.
      rewrite negb_andb.
      reflexivity.
  - intros.
    destruct pol.
    + rewrite eval_dnf_app.
      rewrite IHf1. rewrite IHf2.
      reflexivity.
    + rewrite eval_dnf_and.
      rewrite IHf1. rewrite IHf2.
      rewrite negb_orb.
      reflexivity.
  - intros.
    destruct pol.
    + rewrite (eval_dnf_app _ (distr [(false, a)] ([(true, a)] :: dnf true f2))).
      rewrite eval_distr.
      simpl.
      rewrite! andb_true_r.
      rewrite eval_dnf_and.
      rewrite !IHf1. rewrite !IHf2.
      simpl.
      unfold eval_lit;simpl.
      destruct (eval a); simpl. btauto.
      rewrite IHf2. btauto.
    + rewrite (eval_dnf_app _ (distr [(true, a)] (dnf false f1))).
      rewrite !eval_distr.
      rewrite IHf1. rewrite IHf2.
      simpl. unfold eval_lit; simpl.
      destruct (eval a); btauto.
Qed.

Fixpoint is_unsat_andl {A: Type} (unsat : (bool * A) -> (bool * A) -> bool) (l: andl A) :=
  match l with
  | nil => false
  | e1::l1 => if List.existsb (unsat e1) l1
              then true
              else is_unsat_andl unsat l1
  end.

Lemma eval_andl_false : forall {A: Type} eval x (l:andl A),
    In x l -> eval_lit eval x = false ->
    eval_andl eval l = false.
Proof.
  induction l; simpl.
  - tauto.
  - intros. destruct H ; subst.
    + rewrite H0. reflexivity.
    + destruct (eval_lit eval a); simpl;auto.
Qed.

Lemma eval_dnf_false : forall {A: Type} eval (l:list (andl A)),
  (forall x, In x l -> eval_andl eval x = false) ->
    eval_dnf eval l = false.
Proof.
  induction l; simpl.
  - tauto.
  - intros.
    rewrite H by auto.
    simpl. apply IHl.
    auto.
Qed.




Lemma is_unsat_andl_ok :
  forall {A: Type} eval unsat
         (SATOK : forall l1 l2, unsat l1 l2 = true -> eval_lit eval l1 = true -> eval_lit eval l2 = true -> False) (l: andl A),
    is_unsat_andl unsat l = true ->
    eval_andl eval l = false.
Proof.
  induction l.
  - simpl.
    discriminate.
  - simpl. intros.
    destruct (existsb (unsat a) l) eqn:EXB.
    + rewrite existsb_exists in EXB.
      destruct EXB as (x & IN & US).
      specialize (SATOK a x US).
      destruct (eval_lit eval a); simpl; auto.
      eapply eval_andl_false;eauto.
      destruct (eval_lit eval x);tauto.
    + rewrite IHl; auto.
      rewrite andb_comm. reflexivity.
Qed.

Definition is_unsat_dnf {A: Type} unsat (l: list (andl A)) :=
  forallb (is_unsat_andl unsat) l.

Lemma is_unsat_dnf_ok :
  forall {A: Type} eval unsat
         (SATOK : forall l1 l2, unsat l1 l2 = true -> eval_lit eval l1 = true -> eval_lit eval l2 = true -> False) (l: list (andl A)),
    is_unsat_dnf unsat l = true ->
    eval_dnf eval l = false.
Proof.
  unfold is_unsat_dnf.
  intros.
  rewrite forallb_forall in H.
  apply eval_dnf_false.
  intros.
  specialize (H _ H0).
  apply is_unsat_andl_ok with (eval:=eval) in H; auto.
Qed.




Fixpoint map_opt_formula {A B: Type} (F : A -> option B) (form : @formula A) : option (@formula B) :=
  match form with
  | TT => Some TT
  | FF => Some FF
  | Atom a => do x <-  (F a) ; Some (Atom x)
  | Not p  => do x <- (map_opt_formula F p) ; Some (Not x)
  | And p q => do p1 <- (map_opt_formula F p) ;
               do p2 <- (map_opt_formula F q) ; Some (And p1 p2)
  | Or p q => do p1 <- (map_opt_formula F p) ;
              do p2 <- (map_opt_formula F q) ; Some (Or p1 p2)
  | Ite a p q =>
      do a' <- F a ;
      do p1 <- (map_opt_formula F p) ;
      do p2 <- (map_opt_formula F q) ;
      Some (Ite a' p1 p2)
  end.

Lemma eval_map_opt_formula :
  forall (A B: Type) (F: A -> option B) (eA : A -> bool) (eB: B -> bool)
         (EQEV : forall a x, F a = Some x -> eA a = eB x)
    f f',
    map_opt_formula F f = Some f' ->
    eval_formula eA f = eval_formula eB f'.
Proof.
  induction f; simpl; intros; invert_do H; simpl; auto.
  - f_equal. eapply IHf;eauto.
  - f_equal; eauto.
  - f_equal; eauto.
  - apply EQEV in EQ.
    rewrite EQ. destruct (eB x); eauto.
Qed.

Fixpoint In {A : Type} (a : A) (form : @formula A) : Prop :=
  match form with
  | TT
  | FF => False
  | Atom a' => a' = a
  | Not p => In a p
  | And p q
  | Or p q => (In a p) \/ (In a q)
  | Ite a' p q => a' = a \/ In a p \/ In a q
  end.

Section AtomExtract.

Variable A : Type.

Hypothesis decA : forall (x y : A), {x = y} + {x <> y}.

Fixpoint get_atoms (form : @formula A) : list A :=
  match form with
  | TT
  | FF => []
  | Atom a => [a]
  | Not p => get_atoms p
  | And p q
  | Or p q => (get_atoms p) ++ (get_atoms q)
  | Ite a' p q => a':: (get_atoms p) ++ (get_atoms q)
  end.

Definition get_atoms_nodup (form : @formula A) : list A :=
  nodup decA (get_atoms form).

Theorem in_get_atoms_iff : forall (a : A) (form : @formula A),
  In a form <-> List.In a (get_atoms form).
Proof.
  split; intros H; induction form as [| | a' | p IHp | p IHp q IHq | p IHp q IHq| a' p IHp q IHq]; simpl;
    trivial; try (simpl in H; (apply in_app_or in H || apply in_or_app); destruct H); simpl in H; try rewrite List.in_app_iff in *;
    tauto.
Qed.

Theorem in_get_atoms_nodup_iff : forall (a : A) (form : @formula A),
  In a form <-> List.In a (get_atoms_nodup form).
Proof.
  intros. unfold get_atoms_nodup. rewrite nodup_In. apply in_get_atoms_iff.
Qed.

Theorem NoDup_get_atoms_nodup : forall (form : @formula A), NoDup (get_atoms_nodup form).
Proof.
  intros. apply NoDup_nodup.
Qed.

End AtomExtract.
