Require Import compcert.lib.Integers.
Require Import Utf8 ZArith Lia.
Require Import Common Bdd BddExt Formula Filter Fdd.
Set Keyed Unification.

Import ListNotations.

Definition predform := @formula pred.

(** [pol2form pf] compiles a filter policy into a logic formula. *)
Fixpoint pol2form (pf : pfilter) : predform :=
  match pf with
  | [] => FF
  | (c, a) :: t =>
      if a then Or c (pol2form t)
      else And (Not c) (pol2form t)
  end.

Definition filter2formula (pf : pfilter) : predform :=
  normalize_cond (pol2form pf).

Definition eval_predform (pk : packet) := eval_cond (eval_pred pk).

Definition action2bool (a : action) := if a then true else false.

Coercion action2bool : action >-> bool.


Theorem pol2form_ok : ∀ (pf : pfilter) (pk : packet),
  eval_predform pk (pol2form pf) =  (exec pk pf).
Proof.
  induction pf as [| (c, ac) pf' IHpf'].
  - simpl. reflexivity.
  - simpl. intros pk. unfold eval_predform in *.
    destruct ac; simpl;rewrite IHpf';destruct (eval_cond (eval_pred pk) c); simpl; auto.
Qed.


Lemma filter_swap : forall pk pf1 pf2 c1 c2,
    (eval_predform pk c1 = true -> eval_predform pk c2 = true -> False) ->
    eval_predform pk (pol2form (pf1 ++ (c1,Reject) :: (c2,Accept) :: pf2)) =
      eval_predform pk (pol2form (pf1 ++ (c2,Accept) :: (c1,Reject) :: pf2)).
Proof.
  intros.
  unfold eval_predform, eval_cond in *.
  induction pf1.
  - simpl.
    destruct ((eval_formula (eval_pred pk) c1)).
    + simpl.
      destruct (eval_formula (eval_pred pk) c2); simpl; auto.
    + simpl. reflexivity.
  - simpl.
    destruct a. destruct a; simpl.
    rewrite IHpf1.
    reflexivity.
    rewrite IHpf1.
    reflexivity.
Qed.

Definition is_unsat (f : predform) :=
is_unsat_dnf Clause.is_conflict_lit (dnf true f).

Fixpoint is_redundant (c: cond) (pf: pfilter) :=
  match pf with
  | [] => true
  | (c',a)::l => match a with
                 | Reject => is_redundant c l
                 | Accept => is_unsat (And c c') && is_redundant c l
                 end
  end.


Fixpoint reduce (pf: pfilter) :=
  match pf with
  | [] => []
  | (c,a) ::pf => match a with
                  | Accept => (c,a) :: reduce pf
                  | Reject => if is_redundant c pf
                              then reduce pf
                              else (c,a):: reduce pf
                  end
  end.

Lemma is_redundant_ok :
  forall c pk pf
         (RED : is_redundant c pf = true)
         (C : eval_cond (eval_pred pk) c = true),
    exec pk pf = Reject.
Proof.
  induction pf; simpl.
  - auto.
  - destruct a as (c',a).
    destruct a.
    destruct (is_unsat (And c c')) eqn:UNSAT.
    + intros.
      specialize (IHpf RED C).
      rewrite IHpf.
      apply is_unsat_dnf_ok with (eval := eval_pred pk) in UNSAT.
      simpl in UNSAT.
      rewrite eval_dnf_and in UNSAT.
      rewrite !eval_dnf_ok in *.
      unfold eval_cond in *.
      rewrite C in UNSAT.
      simpl in UNSAT.  rewrite UNSAT. reflexivity.
      intros. eapply Clause.is_conflict_lit_sound; eauto.
    + discriminate.
    + intros.
      rewrite IHpf; auto.
      destruct (eval_cond (eval_pred pk) c' );auto.
Qed.


Lemma reduce_ok : forall pk pf,
    exec pk (reduce pf) = exec pk pf.
Proof.
  induction pf; simpl.
  - reflexivity.
  - destruct a as (c,a).
    destruct a.
    + simpl.
      rewrite IHpf. reflexivity.
    + destruct (is_redundant c pf) eqn:RED.
      * destruct (eval_cond (eval_pred pk) c) eqn:C; auto.
        rewrite IHpf.
        eapply is_redundant_ok; eauto.
      * simpl. rewrite IHpf. reflexivity.
Qed.


Theorem filter2formula_correct : forall (pf : pfilter) (pk : packet),
  eval_predform pk (filter2formula pf) = action2bool (exec pk pf).
Proof.
  intros. unfold filter2formula. unfold eval_predform. rewrite <- eval_normalized_cond_ok.
  apply pol2form_ok with (pf := pf); reflexivity.
Qed.

Section PredComparison.

Definition coq_comparison : Set := Datatypes.comparison.

Definition term_compare (t1 t2 : term) : coq_comparison :=
  match t1, t2 with
  | ConstInt i1, ConstInt i2 => Z.compare (Int.intval i1) (Int.intval i2)
  | PacketField f1, PacketField f2 => Pos.compare f1 f2
  | ConstInt _, PacketField _ => Lt
  | PacketField _, ConstInt _ => Gt
  end.

Definition comp_compare (c1 c2 : comparison) : coq_comparison :=
  Z.compare (comp_to_int c1) (comp_to_int c2).

Definition pred_compare (p1 p2 : pred) : coq_comparison :=
  let '(t1, c1, t1') := p1 in
  let '(t2, c2, t2') := p2 in
  match term_compare t1 t2 with
  | Eq =>
      match comp_compare c1 c2 with
      | Eq => term_compare t1' t2'
      | cc => cc
      end
  | tc => tc
  end.

End PredComparison.

Lemma Zcompare_Eq_trans : forall (n m p : Z), (n ?= m)%Z = Eq -> (m ?= p)%Z = Eq -> (n ?= p)%Z = Eq.
Proof.
  intros n m p. repeat rewrite Z.compare_eq_iff. apply Z.eq_trans.
Qed.

Lemma Zcompare_trans : forall (c : coq_comparison) (n m p : Z), (n ?= m)%Z = c -> (m ?= p)%Z  = c -> (n ?= p)%Z = c.
Proof.
  intros c n m p; destruct c; apply Zcompare_Eq_trans || apply Zcompare_Gt_trans || apply Zcompare_Lt_trans.
Qed.

Lemma Pcompare_Eq_trans : forall (n m p : positive), (n ?= m)%positive = Eq -> (m ?= p)%positive = Eq -> (n ?= p)%positive = Eq.
Proof.
  intros n m p. repeat rewrite Pos.compare_eq_iff. apply Pos.eq_trans.
Qed.

Lemma Pcompare_Lt_trans : forall (n m p : positive), (n ?= m)%positive = Lt -> (m ?= p)%positive = Lt -> (n ?= p)%positive = Lt.
Proof.
  intros n m p. repeat rewrite Pos.compare_lt_iff. apply Pos.lt_trans.
Qed.

Lemma Pcompare_Gt_trans : forall (n m p : positive), (n ?= m)%positive = Gt -> (m ?= p)%positive = Gt -> (n ?= p)%positive = Gt.
Proof.
  intros n m p. repeat rewrite Pos.compare_gt_iff. intros Hmn Hpm. apply (Pos.lt_trans p m n Hpm Hmn).
Qed.

Lemma Pcompare_trans : forall (c : coq_comparison) (n m p : positive), (n ?= m)%positive = c -> (m ?= p)%positive  = c -> (n ?= p)%positive = c.
Proof.
  intros c n m p. destruct c; apply Pcompare_Eq_trans || apply Pcompare_Gt_trans || apply Pcompare_Lt_trans.
Qed.

Module Pred <: OrderedTypeAlt.

  Definition t := pred.

  Definition compare (x y : t) := pred_compare x y.

  Lemma term_compare_refl : forall (x : term), term_compare x x = Eq.
  Proof.
    destruct x; simpl; apply Z.compare_refl || apply Pos.compare_refl.
  Qed.

  Lemma term_compare_sym : forall (x y : term), term_compare y x = CompOpp (term_compare x y).
  Proof.
    destruct x; destruct y; simpl; try reflexivity.
    - destruct (Int.intval i0 ?= Int.intval i)%Z eqn:ECompInt;
      rewrite Z.compare_antisym; rewrite CompOpp_involutive; symmetry; tauto.
    - destruct (i0 ?= i)%positive eqn:ECompInt;
      rewrite Pos.compare_antisym; rewrite CompOpp_involutive; symmetry; tauto.
  Qed.

  Lemma term_compare_trans : forall (c : coq_comparison) (x y z : term), term_compare x y = c -> term_compare y z = c -> term_compare x z = c.
  Proof.
    destruct x; destruct y; destruct z; simpl; trivial;
    apply Pcompare_trans || apply Zcompare_trans || (intros H1 H2; rewrite <- H1 in H2; discriminate).
  Qed.

  Lemma term_compare_eq_iff : forall (x y : term), term_compare x y = Eq <-> x = y.
  Proof.
    destruct x; destruct y; simpl; split; try (discriminate || (rewrite Z.compare_eq_iff || rewrite Pos.compare_eq_iff); intros H);
    try congruence. destruct i. destruct i0. apply (Int.mkint_eq intval intval0 intrange intrange0) in H. rewrite H. auto.
  Qed.

  Lemma comp_compare_eq_iff : forall (x y : comparison), comp_compare x y = Eq <-> x = y.
  Proof.
    destruct x; destruct y; unfold comp_compare; simpl; tauto || (split; try discriminate).
  Qed.

  Lemma comp_compare_refl : forall (x : comparison), comp_compare x x = Eq.
  Proof.
    destruct x; simpl; apply Z.compare_refl.
  Qed.

  Lemma comp_compare_sym : forall (x y : comparison), comp_compare y x = CompOpp (comp_compare x y).
  Proof.
    destruct x; destruct y; simpl; apply Z.compare_refl || apply Z.compare_antisym.
  Qed.

  Lemma comp_compare_trans : forall (c : coq_comparison) (x y z : comparison), comp_compare x y = c -> comp_compare y z = c -> comp_compare x z = c.
  Proof.
    destruct x; destruct y; destruct z; destruct c; try discriminate;
    apply Zcompare_Eq_trans || apply Zcompare_Lt_trans || apply Zcompare_Gt_trans.
  Qed.

  Lemma compare_refl : forall (x : t), compare x x = Eq.
  Proof.
    destruct x as [[t c] t']. simpl. rewrite term_compare_refl. rewrite comp_compare_refl. apply term_compare_refl.
  Qed.

  Lemma compare_sym : forall (x y : t), compare y x = CompOpp (compare x y).
  Proof.
    destruct x as [[tx cx ] tx']. destruct y as [[ty cy] ty']. simpl.
    destruct (term_compare ty tx) eqn:ETcompYX;
    destruct (comp_compare cy cx) eqn:ECcompYX;
    destruct (comp_compare cx cy) eqn:ECcompXY;
    destruct (term_compare tx ty) eqn:ETcompXY; simpl;
    reflexivity || apply term_compare_sym || apply comp_compare_sym ||
    (rewrite term_compare_sym in ETcompYX; rewrite ETcompXY in ETcompYX; discriminate) ||
    (rewrite comp_compare_sym in ECcompYX; rewrite ECcompXY in ECcompYX; discriminate).
  Qed.

  Lemma compare_eq_iff : forall (x y : t), compare x y = Eq <-> x = y.
  Proof.
    destruct x as [[tx cx ] tx']. destruct y as [[ty cy] ty']. simpl.
    destruct (term_compare tx ty) eqn:ETcompXY.
    - destruct (comp_compare cx cy) eqn:ECcompXY.
      -- split.
        + intros ETcompXY'. apply term_compare_eq_iff in ETcompXY, ETcompXY'.
          rewrite comp_compare_eq_iff in ECcompXY. subst. reflexivity.
        + intros. inv H. apply term_compare_refl.
      -- split; intros.
        + discriminate.
        + inv H. rewrite comp_compare_refl in ECcompXY. discriminate.
        -- split; intros.
        + discriminate.
        + inv H. rewrite comp_compare_refl in ECcompXY. discriminate.
    - split; intros.
      -- discriminate.
      -- inv H. rewrite term_compare_refl in ETcompXY. discriminate.
    - split; intros. 
      -- discriminate.
      -- inv H. rewrite term_compare_refl in ETcompXY. discriminate.
  Qed.

  Lemma compare_imp_or : forall (c : coq_comparison) (tx ty tx' ty' : term) (cx cy : comparison),
    compare (tx, cx, tx') (ty, cy, ty') = c ->
    term_compare tx ty = c
    \/ (term_compare tx ty = Eq /\ comp_compare cx cy = c)
    \/ (term_compare tx ty = Eq /\ comp_compare cx cy = Eq /\ term_compare tx' ty' = c).
  Proof.
    intros. rewrite <- H. simpl.
    destruct (term_compare tx ty); auto.
    destruct (comp_compare cx cy); auto.
  Qed.

  Lemma compare_trans : forall (c : coq_comparison) (x y z : t), compare x y = c -> compare y z = c -> compare x z = c.
  Proof.
    destruct c.
    - intros x y z. repeat rewrite compare_eq_iff. transitivity y; tauto.
    - destruct x as [[tx cx ] tx']; destruct y as [[ty cy] ty']; destruct z as [[tz cz ] tz'].
      intros. apply (compare_imp_or _ tx ty tx' ty' cx cy) in H; try reflexivity.
      apply (compare_imp_or _ ty tz ty' tz' cy cz) in H0; try reflexivity.
      destruct H as [Htxy | [[Htxy Hcxy] | [Htxy [Hcxy Htxy']]]];
      destruct H0 as [Htyz | [[Htyz Hcyz] | [Htyz [Hcyz Htyz']]]]; simpl.
      -- rewrite (term_compare_trans Lt tx ty tz); tauto.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. reflexivity.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. reflexivity. 
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz.
          rewrite (comp_compare_trans Lt cx cy cz Hcxy Hcyz). reflexivity.
      -- rewrite (term_compare_trans Eq tx ty tz Htxy Htyz). apply comp_compare_eq_iff in Hcyz. rewrite Hcyz in Hcxy.
          rewrite Hcxy. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. reflexivity.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. apply comp_compare_eq_iff in Hcxy.
          rewrite <- Hcxy in Hcyz. rewrite Hcyz. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. rewrite (comp_compare_trans Eq cx cy cz Hcxy Hcyz).
          apply (term_compare_trans Lt tx' ty' tz' Htxy' Htyz').
    - destruct x as [[tx cx ] tx']; destruct y as [[ty cy] ty']; destruct z as [[tz cz ] tz'].
      intros. apply (compare_imp_or _ tx ty tx' ty' cx cy) in H; try reflexivity.
      apply (compare_imp_or _ ty tz ty' tz' cy cz) in H0; try reflexivity.
      destruct H as [Htxy | [[Htxy Hcxy] | [Htxy [Hcxy Htxy']]]];
      destruct H0 as [Htyz | [[Htyz Hcyz] | [Htyz [Hcyz Htyz']]]]; simpl.
      -- rewrite (term_compare_trans Gt tx ty tz); tauto.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. reflexivity.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. reflexivity. 
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz.
          rewrite (comp_compare_trans Gt cx cy cz Hcxy Hcyz). reflexivity.
      -- rewrite (term_compare_trans Eq tx ty tz Htxy Htyz). apply comp_compare_eq_iff in Hcyz. rewrite Hcyz in Hcxy.
          rewrite Hcxy. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. reflexivity.
      -- apply term_compare_eq_iff in Htyz. rewrite Htyz in Htxy. rewrite Htxy. apply comp_compare_eq_iff in Hcxy.
          rewrite <- Hcxy in Hcyz. rewrite Hcyz. reflexivity.
      -- apply term_compare_eq_iff in Htxy. rewrite <- Htxy in Htyz. rewrite Htyz. rewrite (comp_compare_trans Eq cx cy cz Hcxy Hcyz).
          apply (term_compare_trans Gt tx' ty' tz' Htxy' Htyz').
  Qed.

End Pred.

Require Import FMapAVL FMapFacts FMapPositive.

Module PredO := OrderedType_from_Alt(Pred).

Module PredMap := FMapAVL.Make (PredO).
Module PredMapFacts := FMapFacts.Facts (PredMap).

Notation predmap := PredMap.t.
Notation pred2var_env := (PredMap.t var).

Section PredEnvs.

Fixpoint predlist_to_p2v_env_rec (v : positive) (l : list pred) : pred2var_env :=
  match l with
  | [] => PredMap.empty var
  | h :: t => PredMap.add h v (predlist_to_p2v_env_rec (Pos.add v 1) t)
  end.

Definition predlist_to_p2v_env := predlist_to_p2v_env_rec 1.

Fixpoint predlist_to_v2p_env_rec (v : positive) (l : list pred) : var2pred_env :=
  match l with
  | [] => PMap.empty pred
  | h :: t => PMap.add v h (predlist_to_v2p_env_rec (Pos.add v 1) t)
  end.

Definition predlist_to_v2p_env := predlist_to_v2p_env_rec 1.

Lemma incr_var_v2p: forall l v0 v v' p,
  Bdd.find (predlist_to_v2p_env_rec v0 l) v' = Some p ->
  (v < v0)%positive ->
  (v < v')%positive.
Proof.
  induction l as [| x l' IHl'].
  - simpl. intros. apply PMapFacts.find_mapsto_iff in H. apply PMapFacts.empty_mapsto_iff in H. destruct H.
  - simpl. intros. apply PMapFacts.find_mapsto_iff in H. apply PMapFacts.add_mapsto_iff in H. destruct H as [[H1 H2] | [H1 H2]].
    -- rewrite <- H1. tauto.
    -- apply IHl' with (v := v) in H2. tauto. lia.
Qed.

Lemma not_in_v2p : forall l v v' p p',
  ~ In p l -> PMap.find v' (predlist_to_v2p_env_rec v l) = Some p' -> p <> p'.
Proof.
  induction l as [| x l' IHl'].
  - simpl. intros. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.empty_mapsto_iff in H0. destruct H0.
  - simpl. intros. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.add_mapsto_iff in H0. destruct H0 as [[H1 H2] | [H1 H2]].
    -- apply Decidable.not_or in H. destruct H as [H H']. rewrite <- H2. auto.
    -- apply Decidable.not_or in H. destruct H as [H H']. apply IHl' with (p := p) in H2. tauto. tauto.
Qed.

Definition p2v_equiv_v2p (p2v : pred2var_env) (v2p : var2pred_env) : Prop :=
  forall (p : pred) (v : positive), PredMap.find p p2v = Some v <-> PMap.find v v2p = Some p.

Lemma p2v_equiv_v2p_ok : forall (l : list pred) (v : positive),
  NoDup l -> p2v_equiv_v2p (predlist_to_p2v_env_rec v l) (predlist_to_v2p_env_rec v l).
Proof.
  unfold p2v_equiv_v2p. induction l as [| p l' IHl'].
  - simpl. split.
    -- intros. apply PredMapFacts.find_mapsto_iff in H0. apply PredMapFacts.empty_mapsto_iff in H0. destruct H0.
    -- intros. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.empty_mapsto_iff in H0. destruct H0.
  - simpl. intros. apply NoDup_cons_iff in H. destruct H as [HnotIn HnoDup]. split.
    -- intros. apply PredMapFacts.find_mapsto_iff in H. apply PredMapFacts.add_mapsto_iff in H.
      destruct H as [[H1 H2] | [H1 H2]].
      + apply Pred.compare_eq_iff in H1. rewrite <- H1. rewrite <- H2. rewrite <- PMapFacts.find_mapsto_iff. apply PMapFacts.add_mapsto_iff.
        left. split. reflexivity. reflexivity.
      + apply PredMapFacts.find_mapsto_iff in H2. apply IHl' in H2. rewrite <- PMapFacts.find_mapsto_iff. apply PMapFacts.add_mapsto_iff.
        right. split.
        ++ apply (incr_var_v2p l' (Pos.add v 1) v v0) in H2. lia. lia.
        ++ tauto.
        ++ tauto.
    -- intros. apply PMapFacts.find_mapsto_iff in H. apply PMapFacts.add_mapsto_iff in H. destruct H as [[H1 H2] | [H1 H2]].
      + rewrite <- H1. rewrite <- H2. apply PredMapFacts.find_mapsto_iff. apply PredMapFacts.add_mapsto_iff. left. split. apply Pred.compare_refl. reflexivity.
      + apply PredMapFacts.find_mapsto_iff. apply PredMapFacts.add_mapsto_iff. right. split.
        ++ apply not_in_v2p with (p := p) in H2. rewrite Pred.compare_eq_iff. tauto. tauto.
        ++ apply IHl' in H2. apply PredMapFacts.find_mapsto_iff in H2. tauto. tauto.
Qed.

Definition in_predform_p2v_iff (pform : predform) (p2v: pred2var_env) : Prop :=
  forall (p : pred), Formula.In p pform <-> PredMap.In p p2v.

End PredEnvs.

Definition boolform := @formula var.

Definition eval_boolform (v2b : var2bool_env) := @eval_formula var (eval_boolvar v2b).


Definition tok_form (p2v : pred2var_env) (pform : predform) : option boolform :=
  map_opt_formula (fun x => PredMap.find x p2v) pform.

Lemma eval_pred_var2pred :
  forall p2v v2p pk a x
  (EQUIV : p2v_equiv_v2p p2v v2p)
  (EQ: PredMap.find a p2v = Some x),
  compose (eval_pred pk) (var2pred_fun v2p) x = eval_pred pk a.
Proof.
  intros.
  unfold compose, eval_pred,var2pred_fun,of_pmap.
  apply EQUIV in EQ.
  rewrite EQ. reflexivity.
Qed.


Theorem tok_form_correct : forall  (pk : packet)  (p2v : pred2var_env) (v2p : var2pred_env) (pform : predform) (bform : boolform),
  tok_form p2v pform = Some bform ->
  p2v_equiv_v2p p2v v2p ->
  eval_formula (compose (eval_pred pk) (var2pred_fun v2p)) bform =  eval_predform pk pform.
Proof.
  unfold eval_predform,tok_form,eval_cond.
  intros. symmetry.
  eapply eval_map_opt_formula; eauto.
  intros.
  symmetry.
  simpl in H1.
  eapply eval_pred_var2pred;eauto.
Qed.

Definition NoDup_ordered_predlist (order_preds : predform -> list pred) :=
  forall (pform : predform), NoDup (order_preds pform).

Definition order_preds_default (pform : predform) := get_atoms_nodup eq_pred_dec pform.

Theorem NoDup_order_preds_default :
  NoDup_ordered_predlist order_preds_default.
Proof.
  unfold NoDup_ordered_predlist. apply NoDup_get_atoms_nodup.
Qed.

Definition tok_form_aux (preds : list pred) (pform : predform) : option (var2pred_env * pred2var_env * boolform) :=
  let p2v := predlist_to_p2v_env preds in
  let v2p := predlist_to_v2p_env preds in
  do bform <- tok_form p2v pform;
  Some (v2p, p2v, bform).

Definition tok_form_env (order_preds : predform -> list pred) (NODUP : NoDup_ordered_predlist order_preds) (pform : predform) : option (var2pred_env * pred2var_env * list pred * boolform) :=
  let preds := order_preds pform in
  do v2p, p2v, bform <- tok_form_aux preds pform;
  Some (v2p, p2v, preds, bform).

Definition tok_form_aux_correct :
  forall pform pk b v2p p2v preds (HNoDup : NoDup preds) bform,
    eval_predform pk pform = b ->
    tok_form_aux preds pform = Some (v2p, p2v, bform) ->
    eval_formula (compose (eval_pred pk) (var2pred_fun v2p)) bform = b.
Proof.
    unfold tok_form_aux. intros. invert_do H0. remember (predlist_to_p2v_env preds) as p2v.
    apply tok_form_correct with (pform := pform) (p2v := p2v); try tauto. rewrite Heqp2v. apply p2v_equiv_v2p_ok. auto.
Qed.

Theorem tok_form_env_correct :
  forall (pform : predform) (pk : packet) (b : bool) (v2p : var2pred_env) (p2v : pred2var_env) (preds : list pred) (bform : boolform)
          (order_preds : predform -> list pred) (HNoDup: NoDup_ordered_predlist order_preds),
    eval_predform pk pform = b ->
    tok_form_env order_preds HNoDup pform = Some (v2p, p2v, preds, bform) ->
    eval_formula (compose (eval_pred pk) (var2pred_fun v2p)) bform =  b.
Proof.
  intros.
  unfold tok_form_env in H0.
  destruct (tok_form_aux (order_preds pform) pform) eqn:Hptb; try discriminate.
  simpl in H0. destruct p as [[v2p' p2v'] bform']. inv H0. eapply tok_form_aux_correct; eauto.
Qed.

Definition tok_form_default := tok_form_env order_preds_default NoDup_order_preds_default.


Definition gen_clauses (l : list pred) :=
  List.fold_left (fun acc cl => And acc (Clause.formula_of_clause cl))
                 (all_pairs Clause.gen_clause l l) TT.


Fixpoint xrev_map {A B: Type} (F : A -> B) (acc: list B) (l:list A) : list B :=
  match l with
  | nil => acc
  | e::l => xrev_map F ((F e)::acc) l
  end.

Definition rev_map {A B: Type} (F : A -> B)  (l:list A) : list B :=
  xrev_map F nil l.

Definition gen_clause_list1 (l : list pred) :=
  rev_map (fun cl => Clause.formula_of_clause cl) (all_pairs_sym Clause.gen_clause l).

Definition gen_clause_list2 (l : list pred) :=
  let all_pairs := all_pairs_sym Clause.gen_clause l  in
  let fix gcl_rec cll acc :=
    match cll with
    | [] => acc
    | [x] => (Clause.formula_of_clause x) :: acc
    | cl1 :: cl2 :: r =>
      gcl_rec r ((And (Clause.formula_of_clause cl1) (Clause.formula_of_clause cl2)) :: acc)
    end
  in gcl_rec all_pairs [].

Definition gen_clause_list3 (l : list pred) :=
  let all_pairs := all_pairs_sym Clause.gen_clause l in
  let fix gcl_rec cll acc :=
    match cll with
    | [] => acc
    | [x] => (Clause.formula_of_clause x) :: acc
    | [cl1; cl2] => (And (Clause.formula_of_clause cl1) (Clause.formula_of_clause cl2) :: acc)
    | cl1 :: cl2 :: cl3 :: r =>
        gcl_rec r ((And (And (Clause.formula_of_clause cl1) (Clause.formula_of_clause cl2)) (Clause.formula_of_clause cl3)) :: acc)
    end
  in gcl_rec all_pairs [].

Definition gen_clause_list4 (l : list pred) :=
  let all_pairs := all_pairs_sym Clause.gen_clause l in
  let fix gcl_rec cll acc :=
    match cll with
    | [] => acc
    | [x] => (Clause.formula_of_clause x) :: acc
    | [cl1; cl2] => (And (Clause.formula_of_clause cl1) (Clause.formula_of_clause cl2) :: acc)
    | [cl1; cl2; cl3] =>
        ((And (And (Clause.formula_of_clause cl1) (Clause.formula_of_clause cl2)) (Clause.formula_of_clause cl3)) :: acc)
    | cl1 :: cl2 :: cl3 :: cl4 :: r =>
        let fcl1 := Clause.formula_of_clause cl1 in
        let fcl2 := Clause.formula_of_clause cl2 in
        let fcl3 := Clause.formula_of_clause cl3 in
        let fcl4 := Clause.formula_of_clause cl4 in
        gcl_rec r ((And (And (And fcl1 fcl2) fcl3) fcl4) :: acc)
    end
  in gcl_rec all_pairs [].

Lemma gen_clauses_tauto : forall l pk,
    eval_cond (eval_pred pk) (gen_clauses l) = true.
Proof.
  unfold gen_clauses.
  intros.
  assert (IN : forall cl, In cl (all_pairs Clause.gen_clause l l) ->
                    eval_cond (eval_pred pk) (Clause.formula_of_clause cl) = true).
  {
    intros.
    apply In_all_pairs in H.
    destruct H as (x1 & x2 & IN1 & IN2 & GCLAUSE).
    apply Clause.gen_clause_correct with (pk:=pk) in GCLAUSE.
    rewrite <- GCLAUSE.
    rewrite Clause.eval_formula_of_clause. auto.
  }
  assert (ACC : eval_cond (eval_pred pk) TT = true).
  {
    reflexivity.
  }
  revert IN ACC.
  generalize (@TT pred) as acc.
  generalize (all_pairs Clause.gen_clause l l).
  induction l0; simpl.
  - auto.
  - intros.
    apply IHl0.
    intros.
    apply IN. tauto.
    simpl.
    rewrite ACC. simpl.
    apply IN. tauto.
Qed.


Definition cl_map := PMap.t (list (bool * var) * list (bool * var)).

Require Import Filter.

Definition add_clause (l:bool * var  ) (cl' : list (bool * var)) (m : cl_map) : cl_map :=
  let (b, v) := l in
  match cl' with
  | lit::nil =>
      match PositiveMap.find v m with
      | None => if b then PMap.add v (lit::nil,nil) m
                else PMap.add v (nil,lit::nil) m
      | Some (ln,lp) =>
          if b then PMap.add v (lit::ln,lp) m
          else PMap.add v (ln,lit::lp) m
      end
  |  _ => m
  end.

Fixpoint tokenize_clause (env : pred2var_env) (cl : Clause.t) : option (list (bool * var) ) :=
    match cl with
    | nil => Some nil
    | (b,p)::cl' =>
        match PredMap.find p env with
        | None => None
        | Some x => match tokenize_clause env cl' with
                    | None => None
                    | Some cl' => Some ((b,x)::cl')
                    end
        end
    end.


Fixpoint extract {A: Type} (cl:list A) :=
  match cl with
  | nil => nil
  | l1::cl' => let ecl := extract cl' in
               (l1,cl'):: List.map (fun '(l,x) => (l,l1::x)) ecl
  end.

Definition insert_clause (m : cl_map) (cl: list (bool * var)) :=
  List.fold_left (fun acc '(l,cl') => add_clause l cl' acc) (extract cl) m.


Definition mk_reduction_rule (env : pred2var_env) (l : list pred) :
  PMap.t (list (bool * var) * list (bool * var)) :=
  let cl := all_pairs Clause.gen_clause l l in
  List.fold_left  (fun acc cl =>
                     match tokenize_clause env cl with
                     | None => acc
                     | Some cl' => insert_clause acc cl'
                     end) cl (PMap.empty _).
