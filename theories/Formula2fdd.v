Require Import ZArith List ListDec Lia.
Require Import Common Bdd BddExt Formula Filter Fdd Filter2formula NForm.

Section S.
Variable order_preds : predform -> list pred.

Definition order_preds_nodup (pform : predform) := nodup eq_pred_dec (order_preds pform).

Theorem NoDup_order_preds :
  NoDup_ordered_predlist order_preds_nodup.
Proof.
  unfold NoDup_ordered_predlist. intros pform. apply NoDup_nodup.
Qed.

Definition predform_to_boolform_optim := tok_form_env order_preds_nodup NoDup_order_preds.



Ltac simpl_eval_boolform H :=
  unfold eval_boolform in H; simpl in H.

Fixpoint bf2bdd_rec (depth : nat) (bf : boolform) (st : state) : option (bdd * state) :=
  match bf with
  | TT => Some (T, st)
  | FF => Some (F, st)
  | Atom v => Some (mk_var v st)
  | Not p =>
      do e,st1 <- bf2bdd_rec depth p st;
      (mk_not depth e st1)
  | And p q =>
      do ep , stp <- bf2bdd_rec depth p st;
      do eq , stq <- bf2bdd_rec depth q stp;
      (mk_and depth ep eq stq)
  | Or p q =>
      do ep, stp <- bf2bdd_rec depth p st;
      do eq, stq <- bf2bdd_rec depth q stp;
       (mk_or depth ep eq stq)
  | Ite a p q =>
      let (x,st) := mk_var a st in
      do nx , st <-  (mk_not depth x st) ;
      do t1,st <- bf2bdd_rec depth p st;
      do t2,st <- bf2bdd_rec depth q st;
      do t1,st <-  (mk_or depth nx t1 st);
      do t2,st <-  (mk_or depth x  t2 st);
      (mk_and depth t1 t2 st)
  end.

Lemma ite_bool :
  forall (c t e:bool),
         (if c then t else e) =
           (negb c || t) && (c || e).
Proof.
  destruct c; simpl.
  - destruct t;reflexivity.
  - reflexivity.
Qed.

Lemma wfe_ex_T : forall st, exists v0 : var, wf_bdd st v0 T.
Proof.
  exists 1%positive.
  apply wfe_T.
Qed.

Lemma wfe_ex_F : forall st, exists v0 : var, wf_bdd st v0 F.
Proof.
  exists 1%positive.
  apply wfe_F.
Qed.

Lemma wfe_ex_N : forall v st n st',
    mk_node_sem F v T st n st' ->
    exists v0 : var, wf_bdd st' v0 n.
Proof.
  intros.
  exists (Pos.add v 1). apply H. lia.
Qed.

Lemma value_mk_node_sem : forall v st n st',
    mk_node_sem F v T st n st' ->
    forall env, value env st' n (env v).
Proof.
  intros.
  destruct H.
  apply H0.
  destruct (env v); constructor.
Qed.


Lemma mk_not_correct'
  : forall (depth : nat)  (st : state) (a : bdd),
    wf_st st ->
    is_wf_bdd st a ->
       forall (res : bdd) (st' : state),
         mk_not depth a st = Some (res, st') ->
         wf_st st' /\
           incr st st' /\
           is_wf_bdd st' res /\ (forall (env : var -> bool) (va : bool), value env st a va -> value env st' res (negb va)).
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in H0.
  destruct H0 as (v & H0).
  eapply mk_not_correct in H1 ; eauto.
  destruct_and ; split_and;auto.
  rewrite <- wf_bdd_is_wf_bdd. eexists ; eauto.
Qed.

Lemma is_wf_bdd_incr : forall st st' e,
    is_wf_bdd st e ->
    incr st st' ->
    is_wf_bdd st' e.
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in *.
  destruct H as (v & WF).
  eapply wf_bdd_incr in WF ; eauto.
Qed.


Lemma mk_and_correct'
     : forall (depth : nat)  (st : state) (a b : bdd),
       wf_st st ->
       is_wf_bdd st a ->
       is_wf_bdd st b ->
       forall (res : bdd) (st' : state),
       mk_and depth a b st = Some (res, st') ->
       wf_st st' /\
       incr st st' /\
       is_wf_bdd st' res /\
       (forall (env : var -> bool) (va vb : bool), value env st a va -> value env st b vb -> value env st' res (va && vb)).
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in H0.
  destruct H0 as (v0 & H0).
  rewrite <- wf_bdd_is_wf_bdd in H1.
  destruct H1 as (v1 & H1).
  eapply mk_and_correct with (v0 := Pos.max v0 v1) in H2 ; eauto.
  destruct_and ; split_and;auto.
  rewrite <- wf_bdd_is_wf_bdd. eexists ; eauto.
  eapply wf_bdd_le;eauto. lia.
  eapply wf_bdd_le;eauto. lia.
Qed.

Lemma mk_or_correct'
     : forall (depth : nat)  (st : state) (a b : bdd),
       wf_st st ->
       is_wf_bdd st a ->
       is_wf_bdd st b ->
       forall (res : bdd) (st' : state),
       mk_or depth a b st = Some (res, st') ->
       wf_st st' /\
       incr st st' /\
       is_wf_bdd st' res /\
       (forall (env : var -> bool) (va vb : bool), value env st a va -> value env st b vb -> value env st' res (va || vb)).
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in H0.
  destruct H0 as (v0 & H0).
  rewrite <- wf_bdd_is_wf_bdd in H1.
  destruct H1 as (v1 & H1).
  eapply mk_or_correct with (v0 := Pos.max v0 v1) in H2 ; eauto.
  destruct_and ; split_and;auto.
  rewrite <- wf_bdd_is_wf_bdd. eexists ; eauto.
  eapply wf_bdd_le;eauto. lia.
  eapply wf_bdd_le;eauto. lia.
Qed.



Theorem bf2bdd_rec_correct :
  forall (depth : nat) (bf : boolform) (n : bdd) (st st' : state),
  wf_st st ->
  bf2bdd_rec depth bf st = Some (n, st') ->
  wf_st st' /\ incr st st' /\ is_wf_bdd st' n /\
    forall env, value env st' n (eval_formula env bf).
Proof.
  induction bf as [| | v | p IHp | p IHp q IHq | p IHp q IHq| a p IHp q IHq].
  - simpl. intros. inv H0. split_and;auto.
    exact I.
  - simpl. intros. inv H0. split_and;auto.
    exact I.
  - simpl. intros.
    inv H0.
    apply (f_equal Some) in H2.
    apply (wb_mk_node F v T st H (wfe_F st v) (wfe_T st v)) in H2 as [H1wf [H1incr H1sem]]. split_and;auto.
    + eapply wfe_ex_N in H1sem. rewrite <- wf_bdd_is_wf_bdd. auto.
    + intros. eapply value_mk_node_sem; eauto.
  - simpl. intros.
    invert_do H0.
    apply (IHp  e st st1) in EQ;auto. destruct EQ as [EBf1 [EBf2 [EBf3 EBf4]]].
    apply mk_not_correct' in H0;auto.
    destruct_and. split_and;auto.
    eapply incr_trans; eauto; tauto.
  - simpl. intros.
    rename H0 into M.
    invert_do M.
    invert_do M.
    apply (IHp ep st stp) in EQ;auto.
    destruct EQ as [EBfp1 [EBfp2 [EBfp3 EBfp4]]].
    apply (IHq eq stp stq) in EQ0;auto.
    destruct EQ0 as [EBfq1 [EBfq2 [ EBfq3  EBpf4]]].
    apply mk_and_correct' in M; auto.
    destruct_and.
    split_and;auto.
    eapply incr_trans; eauto. eapply incr_trans; eauto.
    intro.
    apply M4;auto.
    eapply value_incr;auto.
    eapply is_wf_bdd_incr; eauto.
  - simpl. intros.
    rename H0 into M.
    invert_do M.
    invert_do M.
    apply (IHp ep st stp) in EQ;auto.
    destruct_and.
    apply (IHq eq stp stq) in EQ0; auto.
    destruct_and.
    apply mk_or_correct' in M.
    destruct_and.
    split_and;auto.
    eapply incr_trans; eauto. eapply incr_trans; eauto.
    intros. apply M4;auto.
    eapply value_incr; eauto. tauto.
    eapply is_wf_bdd_incr; eauto.
    auto.
  - simpl. intros.
    destruct (mk_var a st) as (x,st1) eqn:MV.
    simpl in H0.
    destruct (mk_not depth x st1) eqn:MNOT; try discriminate.
    simpl in H0.
    destruct p0 as (nx, stnx).
    destruct (bf2bdd_rec depth p stnx) eqn:P; try discriminate.
    destruct p0 as (p', stp).
    simpl in H0.
    destruct (bf2bdd_rec depth q stp) eqn:Q; try discriminate.
    destruct p0 as (q', stq).
    simpl in H0.
    destruct (mk_or depth nx p' stq) eqn:ORP; try discriminate.
    simpl in H0.
    destruct p0 as (t,stt).
    destruct (mk_or depth x q' stt) eqn:ORQ; try discriminate.
    simpl in H0.
    destruct p0 as (e,ste).
    apply (f_equal Some) in MV.
    apply (wb_mk_node F a T st H (wfe_F st a) (wfe_T st a)) in MV as [H1wf [H1incr H1sem]].
    assert (WFx : is_wf_bdd st1 x).
    {
      rewrite <- wf_bdd_is_wf_bdd.
      destruct H1sem as [WF _].
      exists (Pos.succ a).
      apply WF. lia.
    }
    apply mk_not_correct' in  MNOT; auto.
    destruct_and.
    eapply IHp  in P; auto.
    destruct_and.
    eapply IHq  in Q; auto.
    destruct_and.
    apply mk_or_correct' in ORP; auto.
    destruct_and.
    apply mk_or_correct' in ORQ;auto.
    destruct_and.
    apply mk_and_correct' in H0;auto.
    destruct_and.
    unfold mk_node_sem in H1sem.
    split_and; auto.
    + eapply incr_trans; eauto. eapply incr_trans; eauto.
      eapply incr_trans; eauto.       eapply incr_trans; eauto.
      eapply incr_trans; eauto.       eapply incr_trans; eauto.
    + intros.
      rewrite ite_bool.
       eapply H4.
       eapply value_incr;eauto.
       eapply ORP4;eauto.
       eapply value_incr;eauto.
       eapply value_incr;eauto.
       eapply MNOT4;eauto.
       unfold mk_node_sem in H1sem.
       eapply H1sem.
       destruct (env a);constructor.
       eapply ORQ4;eauto.
       eapply value_incr;eauto.
       eapply value_incr;eauto.
       eapply value_incr;eauto.
       eapply value_incr;eauto.
       eapply H1sem.
       destruct (env a);constructor.
    +  eapply is_wf_bdd_incr;eauto.
    +
      eapply is_wf_bdd_incr;eauto.
      eapply incr_trans;eauto.
      eapply incr_trans;eauto.
      eapply incr_trans;eauto.
    + eapply is_wf_bdd_incr;eauto.
    + eapply is_wf_bdd_incr;eauto.
      eapply incr_trans;eauto.
    + eapply is_wf_bdd_incr;eauto.
Qed.

Section BDDRESTRICT.

  Section REC.
    Variable Frec : bdd -> nat -> boolform -> state -> memo2 -> option (bdd * state * memo2).

    Variable mk_op   : nat -> bdd -> bdd -> state -> option (bdd * state).

    Definition binop (hyps: bdd) (d: nat)  (b1 b2: boolform) (st:state)  (m:memo2) :=
      do e1,st,m <- Frec hyps d b1 st m;
      do e2,st,m <- Frec hyps d b2 st m;
      do e,st  <-  (mk_op d e1 e2 st);
      restrict d e hyps st m.

    Variable bop : bool -> bool -> bool.

    Variable mk_op_correct
      : forall (depth : nat) (v0 : var) (st : state) (a b : bdd),
        wf_st st ->
        is_wf_bdd st a ->
        is_wf_bdd st b ->
        forall (res : bdd) (st' : state),
          mk_op depth a b st = Some (res, st') ->
          wf_st st' /\
            incr st st' /\
            is_wf_bdd st'  res /\ (forall (env : var -> bool) (va vb : bool), value env st a va -> value env st b vb -> value env st' res (bop va vb)).

    Lemma binop_correct :
      forall hyps depth bf1 bf2
             (IHbf1 : forall (n : bdd) (st st' : state) (m m' : memo2),
          wf_st st ->
          (is_wf_bdd st hyps) ->
          wf_memo2_restrict st m ->
          Frec hyps depth bf1 st m = Some (n, st', m') ->
          wf_st st' /\
          incr st st' /\
          (is_wf_bdd st'  n) /\
          wf_memo2_restrict st' m' /\ (forall env : var -> bool, value env st hyps true -> value env st' n (eval_formula env bf1)))
             (IHbf2 : forall (n : bdd) (st st' : state) (m m' : memo2),
          wf_st st ->
          (is_wf_bdd st  hyps) ->
          wf_memo2_restrict st m ->
          Frec hyps depth bf2 st m = Some (n, st', m') ->
          wf_st st' /\
          incr st st' /\
          (is_wf_bdd st' n) /\
          wf_memo2_restrict st' m' /\ (forall env : var -> bool, value env st hyps true -> value env st' n (eval_formula env bf2)))

      (n : bdd)
      (st st' : state)
      (m m' : memo2)
      (WF : wf_st st)
      (WFH : is_wf_bdd st hyps)
      (WFM : wf_memo2_restrict st m)
      (RES : binop  hyps depth bf1 bf2 st m = Some (n, st', m')),
      wf_st st' /\
        incr st st' /\
        (is_wf_bdd st' n) /\
        wf_memo2_restrict st' m' /\
        (forall env : var -> bool, value env st hyps true ->
                                   value env st' n
                                     (bop (eval_formula env bf1) (eval_formula env bf2))).
  Proof.
    intros.
    unfold binop in RES.
    destruct (Frec hyps depth bf1 st m) eqn:A; try discriminate.
    destruct p as ((e1,st1),m1).
    simpl in RES.
    destruct (Frec hyps depth bf2 st1 m1) eqn:B; try discriminate.
    destruct p as ((e2,st2),m2).
    simpl in RES.
    destruct (mk_op depth e1 e2 st2) eqn:OP;try discriminate.
    simpl in RES.
    destruct p as (ro,sto).
    apply IHbf1 in A; auto.
    destruct_and.
    apply IHbf2 in B; auto.
    destruct_and.
    apply mk_op_correct  in OP; auto.
    destruct_and.
    apply restrict_ok in RES;auto.
    destruct_and.
    split_and;auto.
    - eapply incr_trans;eauto.
      eapply incr_trans;eauto.
      eapply incr_trans;eauto.
    - intros.
      apply RES5.
      eapply value_incr;eauto.
      eapply incr_trans;eauto.
      eapply incr_trans;eauto.
      eapply OP4.
      eapply value_incr.
      eapply A5. auto.
      eapply incr_trans;eauto.
      apply B5.
      eapply value_incr; eauto.
    - eapply is_wf_bdd_incr; eauto.
      eapply incr_trans;eauto.
      eapply incr_trans;eauto.
    - eapply wf_memo2_restrict_incr in B3; eauto.
    - exact xH.
    - eapply is_wf_bdd_incr;eauto.
    - eapply is_wf_bdd_incr;eauto.
  Defined.

  End REC.

  Fixpoint bf2bdd_res (hyps: bdd) (depth : nat) (bf : boolform) (st : state) (m:memo2): option (bdd * state * memo2) :=
    match bf with
    | TT => Some (T, st,m)
    | FF => Some (F, st,m)
    | Atom v => Some (mk_var v st,m)
    | Not p =>
        do e,st, m <- bf2bdd_res hyps depth p st m;
        do ne,st <-  (mk_not depth e st);
        restrict depth ne hyps st m
    | And p q => binop  bf2bdd_res  mk_and hyps depth p q st m
    | Or p q =>  binop bf2bdd_res mk_or hyps depth p q st m
    | Ite a p q => None
  end.

  Lemma bf2bdd_res_correct :
  forall (hyps: bdd) (depth : nat)  (bf : boolform)   (n : bdd) (st st' : state) (m m':memo2)
         (WF : wf_st st)
         (WFH : is_wf_bdd st hyps)
         (WFM: wf_memo2_restrict st m)
         (RES : bf2bdd_res hyps depth bf st m = Some (n, st',m')),
    wf_st st' /\ incr st st' /\
      (is_wf_bdd st'  n) /\
      (wf_memo2_restrict st' m') /\
      (forall (env : var -> bool) , value env st hyps true -> value env st' n (eval_formula env bf)).
  Proof.
    induction bf.
    - simpl. intros. inv RES.
      split_and;auto.
      exact I.
    - simpl. intros. inv RES.
      split_and;auto.
      exact I.
    - simpl. intros.
      inv RES.
      apply (f_equal Some) in H0.
      apply (wb_mk_node F a T st WF (wfe_F st a) (wfe_T st a)) in H0 as [H1wf [H1incr H1sem]]. split_and;auto.
      + rewrite <- wf_bdd_is_wf_bdd.
        eapply wfe_ex_N; eauto.
      + eapply wf_memo2_restrict_incr in WFM; eauto.
      + intros.
        destruct H1sem.
        apply H1. destruct (env a); constructor.
    - simpl. intros.
      destruct (bf2bdd_res hyps depth bf st m) as [((e, stp),me) |] eqn:EBf; try discriminate.
      simpl in RES.
      apply (IHbf e st stp m) in EBf;auto. destruct_and.
      destruct  (mk_not depth e stp) eqn:NOT; try discriminate.
      simpl in RES.
      destruct p as (ne,ste).
      inv RES.
      apply mk_not_correct' in NOT;auto.
      destruct_and.
      eapply restrict_ok in H0;auto. destruct_and.
      split_and;auto.
      + eapply incr_trans; eauto.
        eapply incr_trans; eauto.
      + intros.
        eapply H5.
        eapply value_incr;eauto.
        eapply incr_trans;eauto.
        eapply value_incr;eauto.
      + eapply is_wf_bdd_incr;eauto.
        eapply incr_trans;eauto.
      + eapply wf_memo2_restrict_incr in EBf3;eauto.
    - intros.
      simpl in RES.
      simpl.
      eapply binop_correct with (bop:= andb) in RES; auto.
      + intros.
        eapply mk_and_correct'; eauto.
    - intros.
      simpl in RES.
      simpl.
      eapply binop_correct with (bop:= orb) in RES; auto.
      + intros.
        eapply mk_or_correct'; eauto.
    - intros.
      simpl in RES.
      discriminate.
  Qed.

  Lemma wf_memo2_restrict_empty : forall st, wf_memo2_restrict st (PPMap.empty bdd).
  Proof.
    constructor; intros.
    - rewrite PPMapFacts.empty_o in H.
    discriminate.
    - rewrite PPMapFacts.empty_o in H.
    discriminate.
    - rewrite PPMapFacts.empty_o in H.
    discriminate.
  Qed.


  
End BDDRESTRICT.



Definition bf2bdd (depth : nat) (bf : boolform) : option (bdd * state) :=
  do e, st <- bf2bdd_rec depth bf empty;
  Some (e, st).

Theorem bf2bdd_correct:
  forall (depth : nat) (bf : boolform) (v2b : var2bool_env)  (n : bdd) (st : state),
  bf2bdd depth bf = Some (n, st) ->
  wf_st st /\ (is_wf_bdd st  n) /\ value (eval_boolvar v2b) st n (eval_boolform v2b bf).
Proof.
  unfold bf2bdd. intros. invert_do H.
  inv H.
  eapply bf2bdd_rec_correct in EQ; eauto.
  destruct_and. split_and;eauto.
  apply wf_empty.
Qed.

Definition set_depth (v2p : var2pred_env) : nat := (PMap.cardinal v2p) + 1.

Definition formula2bdd (pf : pfilter) (opt_reject:bool) (pp: bool) (red : bool) (gc:bool) : option (bdd * state * var2pred_env) :=

  (** 1- Balance the formula (may not be that useful) *)
  let pf    := if opt_reject then reduce pf else pf in
  let pform := (if pp then NForm.opt_formula else (fun x => x)) (filter2formula pf) in

  (** 2- Precompute environments *)
  let preds := order_preds_nodup pform in
  let p2v := predlist_to_p2v_env preds in
  let v2p := predlist_to_v2p_env preds in
  let depth := set_depth v2p in
  let st      := empty in

  do bform <- tok_form p2v pform;

  (** 3- Compute the formula *)
  do e,st <- if red
               then
                 (** 3a- Compute a tautology formula to restrict the filter *)
                 let clauses := Filter.Clause.tauto_of_preds preds in
                 do  bool_clause <- tok_form p2v clauses;
                 do  bdd_clause,st <- bf2bdd_rec depth bool_clause st;
                 (** 3b- Compute the filter using the restriction *)
                 do  bdd_filter,st,m <- bf2bdd_res bdd_clause depth  bform st  (PPMap.empty _) ;
                 Some (bdd_filter,st)
               else (** 3b- Compute the filter directly without using tautologies *)
                 bf2bdd_rec depth bform st;
  do egc, stgc <- if gc then copy st depth e else Some (e,st);
  Some (egc, stgc, v2p).


Lemma eval_predform_filter2_formula_opt :
  forall (b:bool) pk f,
    eval_predform pk
      ((if b then opt_formula else (fun x : @formula pred => x)) f)
    = eval_predform pk f.
Proof.
  destruct b; auto.
  intros.
  unfold eval_predform,eval_cond.
  rewrite <- opt_formula_correct; auto.
  apply eq_pred_dec.
Qed.



Theorem formula2fdd_correct : forall (pf : pfilter) (pk : packet) (a : action) (v2p : var2pred_env) (e : bdd) (st : state)
                                     (opt_reject: bool) (pp:bool) (red : bool) (gc:bool),
  exec pk pf = a ->
  formula2bdd pf opt_reject pp red gc = Some (e, st, v2p) ->
  value_fdd pk v2p st e  a.
Proof.
  unfold formula2bdd.
  intros.
  subst.
  invert_do H0.
  rename x into bf.
  match goal with
  | H : context[order_preds_nodup ?F] |- _ =>
      set (preds := (order_preds_nodup F)) in *;
    set (f := F) in preds
  end.
  clearbody f.
  assert (ND : NoDup preds).
  { apply NoDup_order_preds. }
  apply tok_form_correct with (pk:=pk)
                                              (v2p:= predlist_to_v2p_env preds)
    in EQ.
  rewrite eval_predform_filter2_formula_opt in EQ.
  set (env := (compose (eval_pred pk)
            (var2pred_fun (predlist_to_v2p_env preds)))).
  assert (RED :
           wf_st st0 /\
             (is_wf_bdd st0  e0) /\
             value env  st0 e0 (eval_formula env bf)).
  {
    destruct red.
    {
      clear EQ EQ0.
      assert (ND':= ND).
      apply Clause.tauto_of_preds_correct with (pk:=pk) in ND.
      set (T := (Clause.tauto_of_preds preds)) in *.
      clearbody T.
      invert_do EQ1.
      destruct (bf2bdd_res bdd_clause (set_depth (predlist_to_v2p_env preds)) bf st1 (PPMap.empty bdd)) eqn:BR ; try discriminate.
      destruct p as ((bdd_filter,st2),m). simpl in EQ0. inv EQ0.
      apply tok_form_correct with (pk:=pk)
                                                  (v2p:= predlist_to_v2p_env preds)   in EQ.
      eapply bf2bdd_rec_correct in EQ1;auto.
      destruct_and.
      apply bf2bdd_res_correct in BR; auto.
      destruct_and.
      split_and;auto.
      apply BR5.
      rewrite <- ND.
      unfold eval_predform,eval_cond in EQ.
      rewrite <- EQ.
      apply EQ4.
      apply wf_memo2_restrict_empty.
      apply wf_empty.
      apply p2v_equiv_v2p_ok.
      auto.
    }
    {
      eapply bf2bdd_rec_correct in EQ1; auto.
      destruct_and. split_and;auto.
      apply wf_empty.
    }
  }
  destruct_and.
  invert_do EQ0.
  inv EQ0.
  rename EQ2 into CP.
  assert (COPY :  wf_st st /\
       is_wf_bdd st e /\
       (forall (env : var -> bool) (b : bool),
        value env st0 e0 b -> value env st e b)).
  {
    destruct gc.
    - apply copy_correct' in CP;auto.
    - inv CP ; auto.
  }
  destruct_and.
  apply COPY3.
  assert (EQR : exec pk
                  (if opt_reject then reduce pf else pf) =
                  exec pk pf).
  {
    destruct opt_reject; auto.
    rewrite reduce_ok; auto.
  }
  rewrite <- EQR.
  rewrite <- filter2formula_correct.
  change pfilter with (list (prod cond action)).
  rewrite <- EQ.
  apply RED3;auto.
  apply  p2v_equiv_v2p_ok.
  apply NoDup_order_preds.
Qed.

End S.
