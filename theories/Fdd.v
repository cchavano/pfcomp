Require Import compcert.lib.Integers.
Require Import Common Bdd Filter .

Definition var2pred_env := (@PMap.t pred).

Definition var2bool_env := (@PMap.t bool).

Definition mk_v2b (pk : packet) (v2p : var2pred_env) : var2bool_env :=
  PMap.map (fun (p : pred) => eval_pred pk p) v2p.

Lemma mk_v2b_find : forall (pk : packet) (v2p : var2pred_env) (v : var) (b : bool),
  find (mk_v2b pk v2p) v = Some b ->
    exists (p : pred), find v2p v = Some p.
Proof.
  unfold mk_v2b. intros. rewrite PMapFacts.map_o in H. unfold option_map in H.
  destruct (find v2p v); try discriminate. eauto.
Qed.

Lemma mk_v2b_eval : forall (pk : packet) (v2p : var2pred_env) (v : var) (p : pred) (b : bool),
  find v2p v = Some p ->
  find (mk_v2b pk v2p) v  = Some b ->
  eval_pred pk p = b.
Proof.
  intros. apply PMapFacts.find_mapsto_iff in H. apply PMapFacts.find_mapsto_iff in H0.
  apply PMapFacts.map_mapsto_iff in H0. destruct H0 as [p' [H0 H0']]. subst. congruence.
Qed.

Definition var2pred_fun (v2p : var2pred_env) : var -> pred :=
  of_pmap (ConstInt Int.zero, Ceq, ConstInt Int.one) v2p.


Definition eval_boolvar (e : var2bool_env) (v: var): bool := of_pmap false e v.


Definition value_fdd (pk : packet) (v2p : var2pred_env) (st : hashcons) (e : bdd) (b : bool) : Prop :=
  value (compose (eval_pred pk) (var2pred_fun v2p)) st e b.
