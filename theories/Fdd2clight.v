From compcert Require Import Coqlib Integers Floats AST Ctypes Cop Clight Clightdefs Memory Events Globalenvs Values Maps.
Import Clightdefs.ClightNotations.
Local Open Scope Z_scope.
Local Open Scope string_scope.
Local Open Scope clight_scope.

Require Import String List ZArith.
Require Import Common Bdd Fdd Filter Formula2fdd.
From compcert Require Import Errors.
Require Import MoreErrors.

Open Scope error_monad_scope.
Set Keyed Unification.

Import ListNotations.

Program Definition make_program (types: list composite_definition)
                                (defs: list (ident * globdef fundef type))
                                (public: list ident)
                                (main: ident) : Errors.res { p : program | prog_types p = types} :=
  match build_composite_env types with
  | Errors.Error e => Errors.Error e
  | Errors.OK ce =>
      Errors.OK {| prog_defs := defs;
            prog_public := public;
            prog_main := main;
            prog_types := types;
            prog_comp_env := ce;
            prog_comp_env_eq := _ |}
  end.

Definition _packet := ident_of_string "packet".
Definition _filter := ident_of_string "filter".
Definition _main := ident_of_string "main".

Notation field_env := (PMap.t type).
Notation key := PMap.key.

Definition bool2int (b : bool) := if b then Int.one else Int.zero.

Definition bool2const (b : bool) :=
  Econst_int (bool2int b) tbool.

Definition bool2val (b : bool) : val :=
  Vint (bool2int b).

Definition cast_tuint (e: Clight.expr) :=
  if type_eq tuint (typeof e)
  then e
  else Ecast e tuint.

Definition term2clight (t : term) (sid : ident) (fields : field_env) : res Clight.expr :=
  match t with
  | ConstInt i => OK (Econst_int i tuint)
  | PacketField f =>
      match  PMap.find f fields with
      | None => Error (MSG "Cannot find field "::POS f :: MSG " " :: CTX f ::nil)
      | Some ty =>
          OK (cast_tuint (Efield
                            (Ederef
                               (Etempvar _packet (tptr (Tstruct sid noattr)))
                               (Tstruct sid  noattr)) f ty))
      end
  end.

Definition comp2binop (c : comparison) : Cop.binary_operation :=
  match c with
  | Ceq => Oeq
  | Cne => One
  | Clt => Olt
  | Cle => Ole
  | Cgt => Ogt
  | Cge => Oge
  end.

Definition pred2clight (p : pred) (sid : ident) (fields : field_env) : res Clight.expr :=
  let '(t1, c, t2) := p in
  do tc1 <- term2clight t1 sid fields;
  do tc2 <- term2clight t2 sid fields;
  OK (Ebinop (comp2binop c) tc1 tc2 tbool).

Definition bdd2expr (e : bdd) : Clight.statement :=
  match e with
  | T => Sreturn (Some (Econst_int Int.one tbool))
  | F => Sreturn (Some (Econst_int Int.zero tbool))
  | N p => Sgoto p
  end.

Definition node2clight (v2p : var2pred_env) (n : node) (label : ident) (sid : ident) (fields : field_env) : res statement :=
  let '(l, v, h) := n in
  do p <- res_of_option "find variable" (PMap.find v v2p);
  do cond <- pred2clight p sid fields;
  let ite := Sifthenelse cond (bdd2expr h) (bdd2expr l) in
  OK (Slabel label ite).

(* Definition bdd2clight_aux (v2p : var2pred_env) (elements : list (key * node)) (sid : ident) (fields : field_env) : res statement :=
  fold_right
      (fun (b : key * node) (a : res statement) =>
        let (v, n) := b in
        do a1 <- a;
        do cn <- node2clight v2p n v sid fields;
        OK (Ssequence cn a1))
      (OK (Sreturn (Some (bool2const false))))
      elements.

Definition bdd2clight_body (v2p : var2pred_env) (e : Bdd.bdd) (st : Bdd.state) (sid : ident) (fields : field_env) : Errors.res statement :=
  match e with
  | T | F => Errors.OK (bdd2expr e)
  | N p =>
    do seq <- bdd2clight_aux v2p (PMap.elements (graph st)) sid fields;
    OK (Ssequence (Sgoto p) seq)
  end. *)

Definition bdd2clight_node_acc (v2p : var2pred_env) (sid : ident) (fields : field_env) (k : key) (n : node) (acc : res statement) :=
  do acc1 <- acc;
  do cn <- node2clight v2p n k sid fields;
  OK (Ssequence cn acc1).

Definition bdd2clight_aux_right v2p elements sid fields : res statement :=
  List.fold_right
    (fun b acc => bdd2clight_node_acc v2p sid fields (fst b) (snd b) acc)
    (OK (Sreturn (Some (bool2const false))))
    elements.

Definition bdd2clight_aux_left (v2p : var2pred_env) (elements : list (key * node)) (sid : ident) (fields : field_env) : res statement :=
  List.fold_left
    (fun acc b => bdd2clight_node_acc v2p sid fields (fst b) (snd b) acc)
    elements
    (OK (Sreturn (Some (bool2const false)))).

Definition bdd2clight_aux (v2p : var2pred_env) (graph : pmap node) (sid : ident) (fields : field_env)  : Errors.res statement :=
   PMap.fold
     (bdd2clight_node_acc v2p sid fields)
     graph
     (OK (Sreturn (Some (bool2const false)))).

Lemma bdd2clight_aux_eq_aux_left :
  forall v2p g sid fields,
    bdd2clight_aux v2p g sid fields = bdd2clight_aux_left v2p (PMap.elements g) sid fields.
Proof.
  intros. unfold bdd2clight_aux_left. unfold bdd2clight_aux_left. apply PMap.fold_1.
Qed.

Lemma bdd2clight_aux_left_eq_right :
  forall v2p g sid fields,
    bdd2clight_aux_left v2p g sid fields = bdd2clight_aux_right v2p (List.rev g) sid fields.
Proof.
  intros. unfold bdd2clight_aux_left. unfold bdd2clight_aux_right. symmetry. apply fold_left_rev_right.
Qed.

Definition bdd2clight_body (v2p: var2pred_env) (e : Bdd.bdd) (st : Bdd.state) (sid : ident) (fields : field_env) : Errors.res statement :=
  match e with
  | T | F => Errors.OK (bdd2expr e)
  | N p =>
    do seq <- bdd2clight_aux v2p (graph st) sid fields;
    OK (Ssequence (Sgoto p) seq)
  end.

Definition public_idents : list ident := [_filter].

Fixpoint mk_field_env (m : members) : field_env :=
  match m with
  | [] => (PMap.empty type)
  | h :: t => PMap.add (name_member h) (type_member h) (mk_field_env t)
  end.

Definition members_nodup_id (m : members) : Prop :=
  NoDup (List.map (fun x => name_member x) m).

Inductive wf_field : member -> Prop :=
| wf_plain : forall id, wf_field (Member_plain id tuint)
| wf_bitfield : forall id sz w
                       (SZ0: sz = I32)
                       (SZ1: (0 < w <=  (bitsize_intsize sz))%Z),
    wf_field (Member_bitfield id sz Unsigned noattr w false).

Inductive wf_members : members -> Prop :=
  | wf_members_nil : wf_members []
  | wf_members_cons (m : members) :
      forall n,
        wf_members m ->
        wf_field n ->
        members_nodup_id (n :: m) ->
          wf_members (n :: m).

Definition wf_struct (cd : composite_definition) : Prop :=
  match cd with
    | Composite id Struct m noattr => wf_members m
    | _ => False
  end.


Lemma wf_field_dec : forall m, {wf_field m} + {~ wf_field m}.
Proof.
  intros.
  destruct m.
  - destruct (type_eq t tuint).
    + subst. left. constructor.
    + right;intro. inv H.
      congruence.
  - destruct sg.
    { right; intro; inv H.
    }
    destruct (intsize_eq sz I32).
    destruct (attr_eq a noattr).
    destruct padding.
    {
      right; intro; inv H.
    }
    subst.
    destruct (Z_lt_le_dec 0 width).
    destruct (Z_lt_le_dec (bitsize_intsize I32) width).
    right. intro. inv H. lia.
    left. constructor;auto.
    right. intro. inv H. lia.
    right;intro.
    inv H. congruence.
    right;intro.
    inv H. congruence.
Qed.

Theorem wf_members_dec : forall fields, {wf_members fields} + {~ wf_members fields}.
Proof.
  intros.
  - induction fields.
    + left. constructor.
    + simpl.
      destruct (wf_field_dec a).
      destruct (ListDec.NoDup_dec Pos.eq_dec (List.map name_member (a::fields))).
      destruct IHfields.
      left.
      constructor; auto.
      right; intro. inv H. tauto.
      right; intro. inv H. unfold members_nodup_id in H4. tauto.
      right. intro. inv H. tauto.
Qed.

Definition cstruct := (ident * members)%type.

Definition bdd2clight (v2p : var2pred_env) (e : bdd) (st : state) (cstr:cstruct) : Errors.res (function * program) :=
    let fields := mk_field_env (snd cstr) in
    do body <- bdd2clight_body v2p e st (fst cstr) fields;
    let f_filter := {|
                     fn_return := tbool;
                     fn_callconv := cc_default;
                     fn_params := [(_packet, (tptr (Tstruct (fst cstr) noattr)))];
                   fn_vars := [];
                     fn_temps := [];
                     fn_body := body;
                   |} in
    let global_definitions := [(_filter, Gfun(Internal f_filter))] in
    let mk_res := make_program [Composite (fst cstr) Struct (snd cstr) noattr] global_definitions public_idents _main in
  match mk_res with
  | Errors.Error e => Errors.Error e
  | Errors.OK (exist _ p  _)  => Errors.OK (f_filter, p)
  end
  .

Lemma bdd2clight_aux_right_imp_node2clight :
  forall el v2p sid fields cst p n,
    bdd2clight_aux_right v2p el sid fields = OK cst ->
    InA (@PMap.eq_key_elt node) (p, n) el ->
    exists ite, node2clight v2p n p sid fields = OK (Slabel p ite).
Proof.
  induction el; intros.
  - apply InA_nil in H0. destruct H0.
  - simpl in H. unfold bdd2clight_node_acc in H. destruct a as [k' n']. monadInv1 H. apply InA_cons in H0. destruct H0 as [H0 | H0].
    -- inv H0. simpl in H1. simpl in H. inv H. destruct n' as [[l v] h]. simpl in EQ1. monadInv1 EQ1.
      simpl. rewrite EQ0. simpl. rewrite EQ1. simpl. eauto.
    -- eapply IHel; eauto.
Qed.

Lemma bdd2clight_body_imp_node2clight :
  forall v2p st sid fields cst p p' n,
    bdd2clight_body v2p (N p) st sid fields = OK cst ->
    find (graph st) p' = Some n ->
    exists ite, node2clight v2p n p' sid fields = OK (Slabel p' ite).
Proof.
  intros. unfold bdd2clight_body in H. monadInv1 H. rewrite bdd2clight_aux_eq_aux_left in EQ.
  rewrite bdd2clight_aux_left_eq_right in EQ. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.elements_mapsto_iff in H0.
  apply InA_rev in H0. eapply bdd2clight_aux_right_imp_node2clight; eauto.
Qed.

(* Lemma bdd2clight_aux_left_imp_node2clight :
  forall el v2p sid fields cst p n,
    bdd2clight_aux_left v2p el sid fields = OK cst ->
    InA (@PMap.eq_key_elt node) (p, n) el ->
    exists ite, node2clight v2p n p sid fields = OK (Slabel p ite).
Proof.
  induction el; intros.
  - apply InA_nil in H0. destruct H0.
  - unfold bdd2clight_aux_left in H. unfold node_stmt_acc in H. simpl in H. *)


(* Lemma bdd2clight_body_imp_node2clight :
  forall v2p st sid fields cst p p' n,
    bdd2clight_body v2p (N p) st sid fields = OK cst ->
    find (graph st) p' = Some n ->
    exists ite, node2clight v2p n p' sid fields = OK (Slabel p' ite).
Proof.
  intros. simpl in H. monadInv1 H. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.elements_mapsto_iff in H0.
  eapply bdd2clight_aux_imp_node2clight; eauto.
Qed. *)

Section FindLabel.

Lemma in_pairlist_diff :
  forall (a a' : key) (b b' : node) (l l' : list (key * node)),
    l = (a', b') :: l' ->
    InA (@PMap.eq_key_elt node) (a, b) l ->
    NoDupA (@PMap.eq_key node) l ->
    (a, b) = (a', b') \/ (a <> a') /\ (InA (@PMap.eq_key_elt node) (a, b) l').
Proof.
  intros. subst. inv H1. inv H0.
  - inv H1. left. simpl in H. inv H. simpl in H0. rewrite H0. reflexivity.
  - right. split; trivial. induction l' as [| (a0, b0)].
    -- apply InA_nil in H1. destruct H1. 
    -- simpl in H3. unfold not in H3. rewrite InA_cons in H3.
    apply Classical_Prop.not_or_and in H3. destruct H3 as [H3 H3'].
    apply InA_cons in H1. unfold not in H3. unfold PMap.eq_key in H3. simpl in H3. destruct H1 as [H1 | H1'].
      + unfold PMap.eq_key_elt in H1. simpl in H1. destruct H1 as [H1 H1'].
      inv H1. auto.
      + inv H4. apply IHl'; auto.
Qed.

Lemma find_label_ok_right :
  forall el v2p sid fields cst k  l v h ite p,
    bdd2clight_aux_right v2p el sid fields = OK cst ->
    InA (@PMap.eq_key_elt node) (p, (l, v, h)) el ->
    NoDupA (@PMap.eq_key node) el ->
    node2clight v2p (l, v, h) p sid fields = OK (Slabel p ite)->
    Clight.find_label p cst k = Some (ite, k) \/
    exists s, Clight.find_label p cst k = Some (ite, Kseq s k).
Proof.
  induction el as [| (v, n) el' IHel'].
  - intros. apply InA_nil in H0. destruct H0.
  - intros. unfold bdd2clight_aux_right in H. unfold bdd2clight_node_acc in H. simpl in H. monadInv1 H.
    eapply in_pairlist_diff in H0; eauto. inv H0.
    -- inv H. simpl in EQ1. monadInv1 EQ1. simpl. rewrite peq_true. simpl in H2. monadInv1 H2. rewrite EQ0 in EQ2. inv EQ2. rewrite EQ1 in EQ4. inv EQ4. right. eauto.
    -- unfold node2clight in EQ1. destruct n as [[l' v'] h'] eqn:En.
    monadInv1 EQ1. simpl in H2. monadInv1 H2. simpl. destruct H as [H H'].
    rewrite peq_false; try tauto. inv H1. destruct h'; simpl; destruct l'; simpl; eapply IHel'; eauto; simpl; rewrite EQ2; simpl; rewrite EQ4; simpl; reflexivity.
Qed.

Lemma find_label_ok :
  forall v2p st sid fields p p' l v h cst ite k,
    bdd2clight_body v2p (N p) st sid fields = OK cst ->
    find (graph st) p' = Some (l, v, h) ->
    node2clight v2p (l, v, h) p' sid fields = OK (Slabel p' ite)->
    Clight.find_label p' cst k = Some (ite, k) \/
    exists s, Clight.find_label p' cst k = Some (ite, Kseq s k).
Proof.
  intros. unfold bdd2clight_body in H. monadInv1 H. rewrite bdd2clight_aux_eq_aux_left in EQ. 
  apply PMapFacts.find_mapsto_iff in H0.
  apply PMapFacts.elements_mapsto_iff in H0. simpl.
  rewrite bdd2clight_aux_left_eq_right in EQ. apply InA_rev in H0.
  eapply find_label_ok_right; eauto. apply NoDupA_rev. apply PMap.eqk_equiv.
  apply PMap.elements_3w.
Qed.

End FindLabel.

Notation empty_cenv := (PTree.empty composite).

Section CompositeFacts.

Definition members_composite_def (cd : composite_definition) :=
  match cd with Composite id su m a => m end.


(*Definition wf_field_env (fields : field_env) : Prop :=
  forall f t, find fields f = Some t -> t = tuint.

Lemma wf_field_env_ok :
  forall cd, wf_struct cd -> wf_field_env (mk_field_env (members_composite_def cd)).
Proof.
  intros. destruct cd as [id su m a]. simpl. simpl in H. destruct su.
  - unfold wf_field_env. induction m.
    -- simpl. intros.
       rewrite PMap.gempty in H0. discriminate.
    -- inv H. specialize (IHm H2). simpl. intros. apply PMapFacts.find_mapsto_iff in H. apply PMapFacts.add_mapsto_iff in H.
       destruct H as [[H H'] | [H H']].
       inv H3. reflexivity.
       rewrite PMapFacts.find_mapsto_iff in H'. specialize (IHm _ _ H'). tauto.
  - destruct H.
Qed.
*)

  Definition has_members (ce: composite_env) (id:ident) (m: members) :=
    exists d, PTree.get id ce = Some d /\ co_members d =  m.


  Lemma add_composite_definitions_of :
    forall id fields types ce ce'
           (ND : NoDup ((map name_composite_def types)))
           (NOTIN : ce ! id = None)
           (IN : In (id, fields)
                   (map
                      (fun x : composite_definition =>
                         (name_composite_def x, members_composite_def x)) types))
           (BUILD : add_composite_definitions ce types = OK ce'),
      has_members ce' id fields.
  Proof.
    unfold has_members.
    induction types.
    - simpl. tauto.
    - intros.
      simpl in IN.
      simpl in BUILD.
      destruct a.
      monadInv1 BUILD.
      simpl in *.
      destruct IN as [IN | IN].
      + inv IN.
        eapply add_composite_definitions_incr with (id:=id) (co:=x) in EQ0.
        eexists ; split; eauto.
        unfold composite_of_def in EQ.
        destruct (ce ! id); try discriminate.
        destruct (complete_members ce fields) ; try discriminate.
        inv EQ. reflexivity.
        rewrite PTree.gss. reflexivity.
      +  eapply IHtypes in EQ0; auto.
         inv ND; auto.
         rewrite PTree.gso; auto.
         intro. subst id0.
         rewrite in_map_iff in IN.
         destruct IN as (d & EQC & IN).
         inv EQC.
         inv ND.
         apply H1.
         rewrite in_map_iff.
         exists d; split; auto.
  Qed.

  Lemma build_composite_env_fields :
    forall types id fields ce
           (ND : NoDup (List.map name_composite_def types))
           (IN : In (id,fields) (List.map (fun x => (name_composite_def x,members_composite_def x)) types))
           (BUILD : build_composite_env types = OK ce),
      has_members ce id fields.
  Proof.
    intros.
    unfold build_composite_env in BUILD.
    eapply add_composite_definitions_of; eauto.
    rewrite PTree.gempty; reflexivity.
  Qed.

Lemma build_composite_env_ok :
  forall co id su m a,
    composite_of_def (empty_cenv) id su m a = Errors.OK co ->
    build_composite_env [Composite id su m a] = Errors.OK (PTree.set id co (empty_cenv)).
Proof.
  intros. subst. unfold build_composite_env. simpl. rewrite H. eauto.
Qed.


Lemma wf_composite_id_search :
  forall co ce id su m a,
    composite_of_def (empty_cenv) id su m a = Errors.OK co ->
    build_composite_env [Composite id su m a] = Errors.OK ce ->
    PTree.get id ce = Some co.
Proof.
  intros. subst. eapply build_composite_env_ok in H; eauto. rewrite H in H0. inv H0.
  unfold "!". simpl. apply PTree.gss0.
Qed.

Definition field_in_members (m : members) (f : ident) : Prop :=
  In f (List.map (fun x => name_member x) m).

Lemma wf_struct_complete_members :
  forall cd ce,
    wf_struct cd ->
    complete_members ce (members_composite_def cd) = true.
Proof.
  intros. destruct cd as [id su m a]. simpl in H. destruct su.
  - induction H. 
    -- reflexivity.
    -- simpl.
       rewrite andb_true_iff.
       simpl in IHwf_members.
       split;auto.
       inv H0.
       + simpl;auto.
       + simpl; auto.
  - destruct H.
Qed.


Lemma field_offset_rec_ok :
  forall m co f id su a ce z,
    wf_struct (Composite id su m a) ->
    field_in_members m f ->
    composite_of_def empty_cenv id su m a = Errors.OK co ->
    exists r, field_offset_rec ce f m z = Errors.OK r.
Proof.
  induction m as [| m0 m' IHm']; intros.
  - unfold field_in_members in H0. destruct H0.
  - simpl. destruct (ident_eq f (name_member m0)).
    -- unfold layout_field. simpl in H. destruct su; try tauto.
       inv H. inv H5.
       + eauto.
       + destruct (zle w 0). lia.
         destruct (zlt (bitsize_intsize I32) w). lia.
         destruct (      zle (z + w)
        (Coqlib.floor z (bitalignof_intsize I32) + bitalignof_intsize I32)
).
         eexists;reflexivity.
         eexists ; reflexivity.
    -- eapply (IHm' _ f id su a _).
       + destruct su.
        ++ simpl in H. inv H. simpl. tauto.
        ++ destruct H.
      + unfold field_in_members in H0. simpl in H0. destruct H0 as [H0 | H0].
        ++ unfold not in n. symmetry in H0. apply n in H0. destruct H0.
        ++ tauto. (* apply In_field_in_members. tauto. *)
      + assert (Hwf_struct : wf_struct (Composite id su m' a)).
              { destruct su. simpl in H. simpl. inv H. tauto. destruct H. } 
              unfold composite_of_def. simpl. apply wf_struct_complete_members with (ce := empty_cenv) in Hwf_struct.
              simpl in Hwf_struct. rewrite Hwf_struct. eauto.
Qed.

Lemma wf_struct_composite_ok :
  forall id su m a ce,
    wf_struct (Composite id su m a) ->
    ce!id = None ->
    exists co, composite_of_def ce id su m a = Errors.OK co.
Proof.
  intros. destruct su. pose proof H as H'. simpl in H.
  - induction H; intros.
    -- unfold composite_of_def. rewrite H0. simpl. eauto.
    -- unfold composite_of_def. rewrite H0. simpl.
        eapply wf_struct_complete_members with (ce := ce) in H' as Hcomplete; eauto.
        simpl in Hcomplete. rewrite Hcomplete. eauto.
  - destruct H.
Qed.

Theorem wf_struct_build_env_ok :
  forall id su m a,
    wf_struct (Composite id su m a) ->
    exists ce, build_composite_env [Composite id su m a] = Errors.OK ce.
Proof.
  intros. destruct su.
  - apply wf_struct_complete_members with (ce := empty_cenv) in H as Hcomplete. simpl in H. induction H.
    -- unfold build_composite_env. simpl. eauto.
    -- unfold build_composite_env. simpl.
        unfold composite_of_def. simpl.
        simpl in Hcomplete.
        rewrite Hcomplete.
        eexists;reflexivity.
  - destruct H.
Qed.

Lemma wf_struct_field_env_members :
  forall id su m a t f,
    wf_struct (Composite id su m a) ->
    find (mk_field_env m) f = Some t ->
    field_in_members m f.
Proof.
  intros. destruct su.
  - simpl in H. induction H.
    -- simpl in H0. apply PMapFacts.find_mapsto_iff in H0.
        apply PMapFacts.empty_mapsto_iff in H0. destruct H0.
    -- simpl in H0. apply PMapFacts.find_mapsto_iff in H0. apply PMapFacts.add_mapsto_iff in H0.
        destruct H0 as [[H0 H0'] | [H0 H0']].
        + unfold field_in_members. simpl. auto.
        + unfold field_in_members. simpl. right.
          rewrite PMapFacts.find_mapsto_iff in H0'. apply IHwf_members in H0'.
          unfold field_in_members in H0'. auto.
  - destruct H.
Qed.

(*Lemma wf_struct_field_env :
  forall id su m a,
    wf_struct (Composite id su m a) ->
    wf_field_env (mk_field_env m).
Proof.
  intros. destruct su.
  - simpl in H. induction H.
    -- simpl. unfold wf_field_env. intros. apply PMapFacts.find_mapsto_iff in H.
        apply PMapFacts.empty_mapsto_iff in H. destruct H.
    -- simpl. unfold wf_field_env. intros.
       inv H0. simpl in *.
       apply PMapFacts.find_mapsto_iff in H2.
        apply PMapFacts.add_mapsto_iff in H2. destruct H2 as [[H2 H2'] | [H2 H2']].
        + auto.
        + unfold wf_field_env in IHwf_members. rewrite PMapFacts.find_mapsto_iff in H2'.
          specialize (IHwf_members _ _ H2'). tauto.
  - destruct H.
Qed.
*)

End CompositeFacts.

Section PredEval.


  Definition member_has_field (id:ident) (ty : type) (m:member)  : Prop :=
    name_member m = id /\ type_member m = ty.

  Definition has_field (m:members) (id:ident) (ty:type) :=
    Exists (member_has_field id ty) m.

  Definition packet_repr (ce:composite_env) (pk: packet) (pstruct: cstruct) (mem:mem) (b:block) (ofs:ptrofs) :=
    forall f ty, has_field (snd pstruct) f ty ->
                 forall delta bf,
                   field_offset ce f (snd pstruct) = Errors.OK(delta,bf) ->
                     deref_loc ty mem b (Ptrofs.add ofs (Ptrofs.repr delta)) bf (Vint  (pk f)).

  Definition is_int32_type (t:type) : bool :=
    match t with
    | Tint I32 _ _ => true
    |  _  => false
    end.

  Lemma is_int32_type_inv : forall t,
      is_int32_type t = true ->
      exists sg a, t = Tint I32 sg a.
  Proof.
    destruct t; try discriminate.
    intros. do 2 eexists;eauto.
    simpl in H. destruct i; try discriminate. reflexivity.
  Qed.



    

(*
Lemma eval_PacketField_access :
  forall ge e le b ofs mm ex f pk co ce id su m a
    (Hload : packetfield_memload id su m a mm ce b ofs),
    wf_struct (Composite id su m a) ->
    field_in_members m f ->
    composite_of_def (empty_cenv) id su m a = Errors.OK co ->
    build_composite_env [Composite id su m a] = Errors.OK ce ->
    (genv_cenv ge) = ce ->
    PTree.get _packet le = Some (Vptr b ofs) ->
    ex = Efield (Ederef (Etempvar _packet (tptr (Tstruct id noattr))) (Tstruct id noattr)) f tuint ->
    eval_bdd ge e le mm ex (Vint (pk f)).
Proof.
  intros until a. intro. intros Hwf_struct Hfield_member. intros.
  eapply field_offset_rec_ok with (co := co) (f := f) (z := 0%Z) (ce := ce) in Hwf_struct as Hfield_offset; try (tauto || rewrite <- H1; tauto).
  destruct Hfield_offset. rewrite H3. eapply eval_Elvalue.
    - eapply eval_Efield_struct with (co := co) (bf := Full); simpl; eauto.
      -- eapply eval_Elvalue; simpl.
        + apply eval_Ederef. apply eval_Etempvar. rewrite H2. eauto.
        + apply deref_loc_copy. reflexivity.
      -- rewrite H1. apply (wf_composite_id_search co ce id su m a); eauto.
      --  unfold composite_of_def in H. eauto. simpl in H.
          unfold field_offset. destruct (complete_members empty_cenv m).
          + inv H. eauto. 
          + discriminate H.
    - simpl. eapply deref_loc_value; simpl; eauto.
Qed.
 *)

  Lemma sem_cast_Vint_tuint : forall i m,
      sem_cast (Vint i) tuint (Tint I32 Unsigned noattr) m = Some (Vint i).
  Proof.
    unfold sem_cast.
    simpl. destruct Archi.ptr64.
    reflexivity.
    destruct (intsize_eq I32 I32); try discriminate; auto.
  Qed.

  
Theorem sem_binary_operation_ok :
  forall (i1 i2 : int) (c : comparison) (m : mem) ge,
  sem_binary_operation ge (comp2binop c)
    (Vint i1) tuint (Vint i2) tuint m = Some (bool2val (Int.cmpu c i1 i2)).
Proof.
  intros.
  replace (sem_binary_operation ge (comp2binop c)) with
    (sem_cmp c) by (destruct c; reflexivity).
  unfold sem_cmp.
  assert (CLASS : classify_cmp tuint tuint = cmp_default).
  {
    reflexivity.
  }
  rewrite CLASS.
  unfold sem_binarith.
  assert (classify_binarith tuint tuint = bin_case_i Unsigned).
  {
    reflexivity.
  }
  rewrite H.
  unfold binarith_type.
  rewrite ! sem_cast_Vint_tuint.
  destruct (Int.cmpu c i1 i2);reflexivity.
Qed.


Lemma find_mk_field_env_members :
  forall ge fields fd t
         (WF: wf_members fields)
         (FD: find (mk_field_env fields) fd = Some t),
    exists delta bf, field_offset ge fd fields = OK (delta, bf).
Proof.
  unfold field_offset.
  intros ge m.
  generalize 0%Z as z.
  induction m; simpl.
  - intros. rewrite PMap.gempty in FD. discriminate.
  - intros.
    destruct (ident_eq fd (name_member a)).
    subst.
    rewrite PMap.gss in FD. inv FD.
    inv WF.
    { unfold layout_field.
      inv H2. do 2 eexists; eauto.
      destruct (zle w 0). lia.
      destruct (zlt (bitsize_intsize I32) w); try lia.
      destruct  (zle (z + w)
        (Coqlib.floor z (bitalignof_intsize I32) + bitalignof_intsize I32)).
      do 2 eexists ; eauto.
      do 2 eexists ;eauto.
    }
    inv WF.
    rewrite PMap.gso in FD by auto.
    eapply IHm;eauto.
Qed.

Lemma find_has_field : forall m fd t,
    wf_members m ->
    find (mk_field_env m) fd = Some t ->
    has_field m fd t.
Proof.
  intros.
  unfold has_field.
  rewrite Exists_exists.
  revert fd t H0.
  induction H; simpl.
  - intros. rewrite PMap.gempty in H0.
    discriminate.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H2.
    destruct (PMapFacts.eq_dec fd (name_member n)).
    inv H2.
    exists n. split ; auto.
    unfold member_has_field. split ; auto.
    apply IHwf_members in H2; auto.
    firstorder.
Qed.

Lemma typeof_cast_tuint : forall e,
    typeof
      (cast_tuint e) = tuint.
Proof.
  unfold cast_tuint.
  intros. destruct (type_eq tuint (typeof e)); auto.
Qed.

Lemma wf_members_is_int_type : forall  fd t m,
    wf_members m ->
    has_field m fd t ->
    is_int32_type t = true.
Proof.
  intros.
  unfold has_field in H0.
  induction H.
  - rewrite Exists_nil in H0. tauto.
  - rewrite Exists_cons in H0.
    destruct H0.
    { clear - H1 H0.
      unfold member_has_field in H0.
      destruct H0; subst.
      inv H1.
      + reflexivity.
      + simpl. reflexivity.
    }
    eapply IHwf_members; eauto.
Qed.


Lemma sem_cast_Vint_to_tuint : forall t i mm,
    is_int32_type t = true ->
    sem_cast (Vint i) t tuint mm = Some (Vint i).
Proof.
  intros.
  apply is_int32_type_inv in H.
  destruct H as (sg & a & EQ); subst.
  unfold sem_cast. simpl.
  destruct (Archi.ptr64).
  simpl. reflexivity.
  destruct (intsize_eq I32 I32). reflexivity.
  congruence.
Qed.


Lemma eval_term2clight :
  forall (ge:genv) (e:env) pk le id m mm b ofs t ce
         (WF    : wf_members m)
         (MEMBER : has_members ge id m)
         (Hload : packet_repr ge pk (id, m) mm b ofs)
         (PK : le ! _packet = Some (Vptr b ofs))
         (T2C : term2clight t id (mk_field_env m) = OK ce),
         eval_expr ge e le mm ce (Vint (eval_term pk t)) /\ (typeof ce = tuint).
Proof.
  destruct t as [i | fd].
  - (* Constant *)
    simpl. intros. inv T2C.
    split. constructor. reflexivity.
  - (* Field *)
    intros.
    unfold packet_repr in Hload.
    unfold has_members in MEMBER.
    destruct MEMBER as (co & GET & COM).
    simpl in T2C.
    destruct (find (mk_field_env m) fd) eqn:FD; try discriminate.
    assert (FD':= FD).
    apply find_mk_field_env_members with (ge:=genv_cenv ge) in FD
        as (delta & bf & EQ).
    apply find_has_field in FD'; auto.
    inv T2C. simpl.
    split;[|apply typeof_cast_tuint].
    assert (EVALFD : eval_expr ge e le mm (Efield
          (Ederef (Etempvar _packet (tptr (Tstruct id noattr)))
             (Tstruct id noattr)) fd t) (Vint (pk fd))).
    {
      econstructor.
      eapply eval_Efield_struct.
      econstructor.
      econstructor. econstructor; eauto.
      simpl. apply deref_loc_copy. reflexivity.
      reflexivity.
      eauto. eauto.
      simpl.
      eapply Hload;auto.
    }
    unfold cast_tuint. simpl.
    destruct (type_eq tuint t); auto.
    econstructor; eauto.
    simpl.
    eapply sem_cast_Vint_to_tuint; eauto.
    eapply wf_members_is_int_type;eauto.
    auto.
Qed.

Theorem eval_Ebinop_pred :
  forall (ge:genv)  e le mm b ofs p id m cp pk
         (WF : wf_members m)
         (MEMBER : has_members ge id m)
         (Hload : packet_repr ge pk (id, m) mm  b ofs),
    PTree.get _packet le = Some (Vptr b ofs) ->
    pred2clight p id (mk_field_env m) = OK cp ->
    eval_expr ge e le mm cp (bool2val (eval_pred pk p)).
Proof.
  intros.
  destruct p as [[t1 c] t2]. simpl in *. monadInv1 H0.
  eapply eval_term2clight with (e:=e) in EQ;eauto.
  eapply eval_term2clight with (e:=e) in EQ1;eauto.
  destruct_and.
  econstructor; eauto.
  rewrite EQ3 in *. rewrite EQ2 in *.
  eapply sem_binary_operation_ok.
Qed.

Lemma pred2clight_type :
  forall p id fields cp, pred2clight p id fields = OK cp -> typeof cp = tbool.
Proof.
  intros. destruct p as [[t1 c] t2]. simpl in H. monadInv1 H. reflexivity.
Qed.

End PredEval.


Lemma call_cont_fixpoint :
  forall (k : cont), call_cont (call_cont k) = call_cont k.
Proof.
  intros. induction k; auto.
Qed.


Lemma bool_val_bool2val : forall b m,
  bool_val (bool2val b) tbool m = Some b.
Proof.
  destruct b; simpl; reflexivity.
Qed.

From compcert Require Import Smallstep.
Require Import Program.Tactics.

Section Simulation.

  (* We consider a given network packet *)
  Variable pk : packet.

  (* We have an environment mapping the Fdd variables to predicates *)
  Variable v2p : var2pred_env.

  (* We also have a Bdd *)
  Variable p : positive.
  Variable st : Bdd.state.

  Let n : Bdd.bdd := N p.

  (* [id] is the name of the structure containing the packet.
     [fields] is the list of fields.
   *)
  Variable id : ident.
  Variable fields : members.

  (* We have some well-formedness conditions *)
  Variable WF_fields : wf_members fields.

  (* Using the compiler, we get a program containing the filter function *)
  Variable prog : Clight.program.
  Variable filter : Clight.function.

  Hypothesis Hcomp_res : bdd2clight v2p n st  (id,fields) = OK (filter, prog).

  Let sem : semantics := Clight.semantics2 prog.
  Let ge : genv := globalenv sem.
  Let e : env := empty_env.

  Lemma has_members_ge :
    has_members ge id fields.
  Proof.
    unfold ge,sem,semantics2.
    simpl.
    assert (PCOMPENV := prog_comp_env_eq prog).
    assert (prog_types prog = [Composite id Struct fields noattr]).
    {
      unfold bdd2clight in Hcomp_res.
      monadInv1 Hcomp_res.
      unfold fst,snd in *.
      destruct (make_program [Composite id Struct fields noattr]
            [(_filter,
             Gfun
               (Internal
                  {|
                    fn_return := tbool;
                    fn_callconv := cc_default;
                    fn_params := [(_packet, tptr (Tstruct id noattr))];
                    fn_vars := [];
                    fn_temps := [];
                    fn_body := x
                  |}))] public_idents _main
               ) ; try discriminate.
      destruct s. inv EQ0.
      auto.
    }
    rewrite H in *.
    eapply build_composite_env_fields; eauto.
    simpl. constructor. simpl; tauto. constructor.
    simpl. tauto.
  Qed.

  Lemma filter_ret_type : fn_return filter = tbool.
  Proof.
    unfold bdd2clight in Hcomp_res.
    monadInv1 Hcomp_res.
    unfold fst,snd in *.
    destruct (          make_program [Composite id Struct fields noattr]
            [(_filter,
             Gfun
               (Internal
                  {|
                    fn_return := tbool;
                    fn_callconv := cc_default;
                    fn_params := [(_packet, tptr (Tstruct id noattr))];
                    fn_vars := [];
                    fn_temps := [];
                    fn_body := x
                  |}))] public_idents _main
); try discriminate.
    destruct s. inv EQ0.
    reflexivity.
  Qed.


Inductive match_states (k : cont) : Bdd.bdd -> Clight.state -> Prop :=
  | match_goto : forall p m le b ofs
                        (PACKET : packet_repr ge pk (id, fields) m b ofs)
                        (LE : le ! _packet = Some (Vptr b ofs)),
      match_states k (N p) (State filter (Sgoto p) k e le m)
| match_return1 : forall m le,
    match_states k T (State filter (Sreturn (Some (Econst_int Int.one tbool))) k e le m)
| match_return0 : forall m le,
    match_states k F (State filter (Sreturn (Some (Econst_int Int.zero tbool))) k e le m).

Inductive has_mem : Clight.state -> mem -> Prop :=
| has_mem_state : forall f st k e le m,
    has_mem (State f st k e le m) m.

Definition same_mem (st1 st2 : Clight.state) :=
  exists m, has_mem st1 m /\ has_mem st2 m.

Lemma has_mem_uniq : forall st m1 m2,
    has_mem st m1 -> has_mem st m2 -> m1 = m2.
Proof.
  intros. inv H. inv H0.
  reflexivity.
Qed.

Lemma same_mem_trans : forall st1 st2 st3,
    same_mem st1 st2 ->
    same_mem st2 st3 ->  same_mem st1 st3.
Proof.
  intros.
  destruct H. destruct H0.
  destruct_and.
  eapply has_mem_uniq in H3;eauto.
  subst.
  exists x; split;auto.
Qed.

Lemma find_v2p_eval_pred : forall pk v2p x v,
    find v2p v = Some x ->
    (compose (eval_pred pk) (var2pred_fun v2p)) v = eval_pred pk x.
Proof.
  intros.
  unfold compose, var2pred_fun , of_pmap.
  rewrite H.
  reflexivity.
Qed.

Theorem transl_step_correct :
  forall k,
  forall S1 S2, Bdd.step (compose (eval_pred pk) (var2pred_fun v2p)) st S1 S2 ->
  forall T1, match_states k S1 T1 ->
  (exists T2 k',
    plus Clight.step2 ge T1 E0 T2 /\
      match_states k' S2 T2 /\ same_mem T1 T2 /\
      call_cont k = call_cont k').
Proof.
  intros.
  assert (HASMGE := has_members_ge).
  inv H. inv H0. unfold bdd2clight in Hcomp_res.
  (*destruct su; try discriminate. *)
  monadInv1 Hcomp_res.
  unfold n in EQ. apply bdd2clight_body_imp_node2clight with (p' := p0) (n := (l, v, h)) in EQ as Hcnode; try auto.
  destruct Hcnode. destruct (make_program [Composite id Struct fields noattr]) eqn:Hmkprog; try discriminate.
  destruct s as (p' & EQp).
  inject Hmkprog. inv EQ0.
  assert (Hfilter : fn_body filter = x). { rewrite <- H2. tauto. } rewrite H2.
  simpl in *.
  remember (mk_field_env fields) as fields_env.
  assert (Hlabel : forall ite, node2clight v2p (l, v, h) p0 id fields_env = OK (Slabel p0 ite) ->
          find_label p0 (fn_body filter) (call_cont k) = Some (ite, call_cont k) \/
          exists s, find_label p0 (fn_body filter) (call_cont k) = Some (ite, Kseq s (call_cont k))).
    { intros. rewrite Hfilter. eapply find_label_ok; eauto.  }
  assert (Hcnode : exists ite, node2clight v2p (l, v, h) p0 id fields_env = OK (Slabel p0 ite)).
    { apply bdd2clight_body_imp_node2clight with (p' := p0) (n := (l, v, h)) in EQ. destruct EQ. exists x0.
      tauto. tauto. }
  destruct Hcnode as [ite Hcnode]. specialize (Hlabel ite Hcnode). 
    (*eapply wf_field_env_ok in Hwf_struct as Hwf_fields; eauto.*)
    simpl in EQ. monadInv1 EQ.
  destruct Hlabel as [Hlabel | [s Hlabel]].
  -
    eexists (State filter (if compose (eval_pred pk) (var2pred_fun v2p) v then (bdd2expr h) else (bdd2expr l)) (call_cont k) e le m). eexists. split.
    -- eapply plus_left'.
      + constructor. eauto.
      + instantiate (1 := E0).
        simpl in Hcnode. monadInv1 Hcnode.
        apply res_of_option_inv in EQ.
        rewrite find_v2p_eval_pred with (x:=x);auto.
        apply plus_one. simpl.
        apply step_ifthenelse with (v1 := bool2val (eval_pred pk x)).
        eapply eval_Ebinop_pred (*with (b := b) (ofs := ofs)*); eauto.
        simpl.
        erewrite pred2clight_type; eauto.
        apply bool_val_bool2val.
      + reflexivity.
    -- split_and.
      + destruct (compose (eval_pred pk) (var2pred_fun v2p) v).
        ++ unfold bdd2expr. destruct h; econstructor; eauto.
        ++ unfold bdd2expr. destruct l; econstructor; eauto.
      + exists m. split; constructor.
      + symmetry. simpl. apply call_cont_fixpoint.
  - eexists (State filter (if (compose (eval_pred pk) (var2pred_fun v2p) v) then (bdd2expr h) else (bdd2expr l)) (Kseq s (call_cont k)) e le m). eexists. split.
    -- eapply plus_left'.
      + constructor. eauto.
      + instantiate (1 := E0).
        simpl in Hcnode. monadInv1 Hcnode.
        apply res_of_option_inv in EQ.
        rewrite find_v2p_eval_pred with (x:=x);auto.
        apply plus_one. simpl. apply step_ifthenelse with (v1 := bool2val (eval_pred pk x)).
        eapply eval_Ebinop_pred with (b := b) (ofs := ofs); eauto. simpl.
        erewrite pred2clight_type; eauto.
        apply bool_val_bool2val.
      + reflexivity.
    -- split_and.
      + destruct (compose (eval_pred pk) (var2pred_fun v2p) v).
        ++ unfold bdd2expr. destruct h; econstructor; eauto.
        ++ unfold bdd2expr. destruct l; econstructor; eauto.
      + exists m; split; constructor.
      + symmetry. simpl. apply call_cont_fixpoint.
Qed.


Theorem star_state_sreturn :
  forall (b : bool) (cst : Clight.state) (n : bdd) (k : cont) ,
  Bdd.Star ((compose (eval_pred pk) (var2pred_fun v2p))) st n (bool2bdd b) ->
  match_states k n cst ->
  (exists k' le m, Star sem cst E0 (State filter (Sreturn (Some (bool2const b))) k' e le m) /\ has_mem cst m /\
   call_cont k' = call_cont k).
Proof.
  unfold Bdd.Star. intros. generalize dependent cst. revert k.
  remember (bool2bdd b) as B. induction H.
  - intros. subst. inv H0; destruct b; simpl in H1; try discriminate;
    exists k; do 2 eexists ; split_and ; (apply star_refl || constructor || reflexivity).
  - intros. specialize (IHclos_refl_trans_1n HeqB).
    subst.
    eapply transl_step_correct in H; eauto.
    destruct H as [T2 [k' [Hplus [Hmatch [Hsame Hcont]]]]].
    specialize (IHclos_refl_trans_1n _ _ Hmatch). destruct IHclos_refl_trans_1n as (k0 & le0 & l0 & IH1 & IHm & IH2). exists k0. do 2 eexists ; split_and.
      -- eapply plus_star. eapply plus_star_trans; eauto.
      -- destruct Hsame as (ms & HM1 & HM2).
        eapply has_mem_uniq in IHm; eauto.
        congruence.
      -- rewrite IH2. auto.
Qed.

Theorem star_return_state :
forall (b : bool) (cst : Clight.state) (n : bdd) (k : cont),
Bdd.Star ((compose (eval_pred pk) (var2pred_fun v2p))) st n (bool2bdd b) ->
match_states k n cst ->
exists m, Star sem cst E0 (Returnstate (bool2val b) (call_cont k) m)
          /\ has_mem cst m.
Proof.
  intros. eapply star_state_sreturn in H; eauto.
  destruct H as (k' & le & m & HStar & HMem & HCall).
  exists m.
  split.
  eapply plus_star. eapply star_plus_trans with (t2 := E0); eauto. apply plus_one.
  assert (Hrx : Step sem (State filter (Sreturn (Some (bool2const b))) k' e le m) E0 (Returnstate (bool2val b) (call_cont k') m)).
  - econstructor. econstructor. destruct b; simpl; unfold bool2val; simpl; rewrite filter_ret_type; reflexivity. reflexivity.
  - rewrite HCall in Hrx. auto.
  - auto.
Qed.

End Simulation.

(*From compcert Require Import Axioms.*)

Lemma list_norepet_small : forall {A: Type} (l:list A), Nat.leb (length l)  1 = true -> list_norepet l.
Proof.
  destruct l. simpl.
  constructor.
  destruct l.
  constructor. simpl. tauto.
  constructor.
  simpl. discriminate.
Qed.

Lemma list_disjoint_nil_r : forall {A: Type} (l:list A),
    list_disjoint l [].
Proof.
  unfold list_disjoint.
  simpl. tauto.
Qed.

Theorem bdd2clight_correct :
  forall (pk : packet)
         (v2p : var2pred_env)
         (n : bdd) (st : Bdd.state) (v : bool)
         (id:ident) (fields : members)
          (filter : Clight.function) (prog : Clight.program) (b : Values.block) (k : cont) (o : ptrofs) (m : mem)
          (WF : wf_members fields)
          (Hload : packet_repr (genv_cenv (globalenv (Clight.semantics2 prog))) pk (id, fields) m  b o),
  bdd2clight v2p n st (id,fields)  = OK (filter, prog) ->
  value_fdd pk v2p st n v ->
  Star (Clight.semantics2 prog) (Callstate (Ctypes.Internal filter) [(Values.Vptr b o)] k m) [] (Returnstate (bool2val v) (call_cont k) m).
Proof.
  intros.
  pose proof H as H'.
  unfold bdd2clight in H.
  monadInv1 H. destruct make_program eqn:EMkprog in EQ0; try discriminate.
  destruct s as (p' & EQp').
  inversion EQ0.
  apply plus_star. eapply plus_left. econstructor; econstructor; simpl.
  { apply list_norepet_small; reflexivity. }
  { apply list_norepet_small; reflexivity. }
  { apply list_disjoint_nil_r. }
  { apply alloc_variables_nil. }
  { reflexivity. }
  destruct n as [| | vp]; inversion EQ.
  - apply plus_star. apply plus_one.
    apply step_return_1 with (v := Values.Vint (Int.zero)); try auto.
    apply eval_Econst_int. simpl.
    inv H0. reflexivity.
  - apply plus_star. apply plus_one. apply step_return_1 with (v := Values.Vint (Int.one)); try auto. apply eval_Econst_int.
    inv H0;reflexivity.
  - apply value_imp_Star in H0. monadInv1 H3.
    remember ((Maps.PTree.set _packet (Vptr b o) (Maps.PTree.empty val))) as le.
    eapply star_step with (t1 := E0) (t2 := E0) (s2 := (State _ (Sgoto vp) (Kseq x0 k) empty_env le m)); try reflexivity.
    apply step_seq.
    eapply star_return_state with (k := Kseq x0 k)
                                  (cst := State
                                  {|
                                    fn_return := tbool;
                                    fn_callconv := cc_default;
                                    fn_params := [(_packet, tptr (Tstruct id noattr))];
                                    fn_vars := [];
                                    fn_temps := [];
                                    fn_body := Ssequence (Sgoto vp) x0
                                  |} (Sgoto vp) (Kseq x0 k) empty_env le m)
                                  in H0; eauto.
    destruct H0 as (m' & STAR & MEM).
    inv MEM.
    --  eauto.
    --  econstructor; eauto.
        subst.
        rewrite PTree.gss. reflexivity.
  - reflexivity.
Qed.

