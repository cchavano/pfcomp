From compcert Require Import Errors.
Require Import String List.

Definition bind3 {A B C D: Type} (f: res (A * B * C)) (g: A -> B -> C -> res D) : res D :=
  match f with
  | OK (x, y , z) => g x y z
  | Error m => Error m
  end.

Definition bind4 {A B C D E: Type} (f: res (A * B * C *D)) (g: A -> B -> C -> D -> res E) : res E :=
  match f with
  | OK (x, y , z , t) => g x y z t
  | Error m => Error m
  end.

Notation "'do' X , Y , Z  <- A ; B" := (bind3 A (fun X Y Z => B))
                                         (at level 200, X ident, Y ident, Z ident, A at level 100, B at level 200).

Notation "'do' X , Y , Z , T <- A ; B" := (MoreErrors.bind4 A (fun X Y Z T => B))
                                            (at level 200, X ident, Y ident, Z ident, T ident, A at level 100, B at level 200).

Definition res_of_option {A: Type} (str:string) (o:option A) : res A :=
  match o with
  | None => Error (MSG str::nil)
  | Some v => OK v
  end.

Lemma res_of_option_inv : forall {A: Type} str (o:option A) v,
    res_of_option str o = OK  v ->
    o = Some v.
Proof.
  destruct o; simpl; congruence.
Qed.
