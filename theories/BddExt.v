(** BddExt extends bdds with the following operations
  - [restrict] restricts a bdd variable to a given value
  - [reduce] is using [restrict] to reduce (further) a bdd by exploiting that
    a bdd variable [x] may entail another bdd variable [y]
  - [copy] copies only the relevant part of a state for a given bdd.
*)


Require Import ZArith Lia.
Require Import Bdd.
Require Import Common.


Definition is_F (e:bdd) :=
  match e with
  | F => true
  | _ => false
  end.

Definition is_T (e:bdd) :=
  match e with
  | T => true
  | _ => false
  end.

Lemma is_F_true : forall e, is_F e = true -> e = F.
Proof.
  destruct e;simpl; congruence.
Qed.

Definition memo (f c:positive) (o :option (bdd *state *memo2)) :=
  do e,st,m <- o ;
  Some(e,st, PPMap.add (f, c) e m).

Fixpoint restrict (d:nat) (f c:bdd) (st:state) (m:memo2) : option (bdd * state * memo2) :=
  match f with
  | T | F => Some(f,st,m)
  | N fp   =>
      match d with
      | O => None
      | S d' =>
          match c with
          | T  => Some (N fp,st,m)
          | F  => Some (T , st,m)
          | N cp =>
              match PPMap.find (fp, cp) m with
              | Some e' => Some(e',st,m)
              | None    =>
                  memo fp cp
                    (match PMap.find fp (graph st) , PMap.find cp (graph st) with
                     | Some(l,v,r) , Some(l',v',r') =>
                         match Pos.compare v v' with
                         | Eq =>
                             if is_F l'
                             then restrict d' r r' st m
                             else if is_F r' then  (restrict d' l l' st m)
                                  else
                                    do l,st,m <- restrict d' l l' st m ;
                                    do r,st,m <- restrict d' r r' st m ;
                                    let (e,st) := mk_node l v r st in
                                    Some ((e,st),m)
                         | Gt =>
                             do l,st,m <- restrict d' l c st m;
                             do r,st,m <- restrict d' r c st m;
                             let (e,st) := mk_node l v r st in
                             Some ((e,st),m)
                         | Lt =>
                             do c', st <-  mk_or d l' r' st;
                             restrict d' f c' st m
                             end
                     |  _ , _  => None
                     end)
              end
          end
      end
  end.


Lemma restrict_eq : forall (d:nat) (f c:bdd) (st:state) (m:memo2),
    restrict d f c st m =
    match f with
  | T | F => Some(f,st,m)
  | N fp   =>
      match d with
      | O => None
      | S d' =>
          match c with
          | T  => Some (N fp,st,m)
          | F  => Some (T , st,m)
          | N cp =>
              match PPMap.find (fp, cp) m with
              | Some e' => Some(e',st,m)
              | None    =>
                  memo fp cp
                    (match PMap.find fp (graph st) , PMap.find cp (graph st) with
                     | Some(l,v,r) , Some(l',v',r') =>
                         match Pos.compare v v' with
                         | Eq =>
                             if is_F l'
                             then restrict d' r r' st m
                             else if is_F r' then  (restrict d' l l' st m)
                                  else
                                    do l,st,m <- restrict d' l l' st m ;
                                    do r,st,m <- restrict d' r r' st m ;
                                    let (e,st) := mk_node l v r st in
                                    Some ((e,st),m)
                         | Gt =>
                             do l,st,m <- restrict d' l c st m;
                          do r,st,m <- restrict d' r c st m;
                          let (e,st) := mk_node l v r st in
                          Some ((e,st),m)
                         | Lt =>
                             match mk_or d l' r' st with
                             | None => None
                             | Some (c',st) => restrict d' f c' st m
                             end
                         end
                     |  _ , _  => None
                     end)
              end
          end
      end
  end.
Proof.
  destruct d; reflexivity.
Qed.

Record wf_memo2_restrict (hc: hashcons) (m: memo2):=
  {
    wf_memo2_restrict_wf_res : forall x y v e,
      PPMap.find (x, y) m = Some e ->
      wf_bdd hc v (N x) -> wf_bdd hc v (N y) -> wf_bdd hc v e;
    wf_memo2_restrict_wf : forall x y e, PPMap.find (x, y) m = Some e ->
                                     exists v, wf_bdd hc v (N x) /\ wf_bdd hc v (N y);
    wf_memo2_restrict_sem : forall x y res,
      PPMap.find (x,y) m = Some res ->
      forall env b, value env hc (N y) true ->
                    value env hc (N x) b ->
                    value env hc res b
  }.

Lemma  wf_memo2_restrict_incr :
  forall hc1 hc2 m
         (WF1 : wf_hashcons hc1)
         (WF2 : wf_hashcons hc2)
         (INCR : incr hc1 hc2)
         (R   : wf_memo2_restrict hc1 m)
  ,
    wf_memo2_restrict hc2 m.
Proof.
  intros  hc1 hc2 m Hwf1 Hwf2 Hincr Hwfm.
  constructor.
  - intros.
    assert (Hwf_bdd1:exists v, wf_bdd hc1 v (N x) /\ wf_bdd hc1 v (N y)).
    { destruct Hwfm ; eauto. }
    destruct Hwf_bdd1 as [v' [Hwf_bdd1 Hwf_bdd2]].
    destruct Hwfm.
    eauto using wf_bdd_rcni.
  - destruct Hwfm.  intros.
    edestruct wf_memo2_restrict_wf0; eauto.
    destruct H0.
    eexists ; split ; eauto.
  - destruct Hwfm.  intros.
    edestruct wf_memo2_restrict_wf0; eauto.
    destruct H2.
    apply wf_memo2_restrict_sem0 with (env:=env) (b:=b) in H; auto.
    eauto.
    eapply value_rcni; eauto.
    eapply value_rcni; eauto.
Qed.

Lemma wf_memo2_restrict_add :
  forall hc m ea x y
         (WF : wf_memo2_restrict hc  m)
         (WFE : forall v, wf_bdd hc v (N x) -> wf_bdd hc v (N y) ->
                          wf_bdd hc v ea)
         (WFEX : exists v : var, wf_bdd hc v (N x) /\ wf_bdd hc v (N y))
         (WFEV :
           forall env r,
             value env hc (N y) true ->
             value env hc (N x) r ->
             value env hc ea r)
  ,
    wf_memo2_restrict hc (PPMap.add (x,y) ea m).
Proof.
  intros.
  inv WF.
  constructor ; auto.
  - intros.
    rewrite PPMapFacts.add_o in H.
    destruct (PPMapFacts.eq_dec (x, y) (x0, y0)).
    + apply PP.reflect in e0.
      inv e0. inv H.
      apply WFE; auto.
    + eauto.
  - intros.
    rewrite PPMapFacts.add_o in H.
    destruct (PPMapFacts.eq_dec (x, y) (x0, y0)).
    + apply PP.reflect in e0.
      inv e0. inv H.
      auto.
    + eauto.
  - intros.
    rewrite PPMapFacts.add_o in H.
    destruct (PPMapFacts.eq_dec (x, y) (x0, y0)).
    + apply PP.reflect in e.
      inv e. inv H.
      eauto.
    + eauto.
Qed.

Lemma wf_bdd_ex_find :
  forall st p l v' r
         (WF : wf_hashcons st)
         (WFE1 : exists vi : var, wf_bdd st vi (N p))
         (FDF : Bdd.find (graph st) p = Some (l, v', r)),
    (exists vi : var, wf_bdd st vi r) /\
      (exists vi : var, wf_bdd st vi l).
Proof.
  intros.
  destruct WFE1 as (vi,WFE1).
  inv WFE1.
  rewrite FDF in H0. inv H0.
  split; eexists.
  eapply wf_map_wf_bdd_h;eauto.
  eapply wf_map_wf_bdd_l;eauto.
Qed.

Lemma restrict_correct :
  forall d e1 e2 st st' e' m m'
         (WF  : wf_st st)
         (WFE1 : exists vi, wf_bdd st vi e1)
         (WFE2 : exists vi, wf_bdd st vi e2)
         (WFM  : wf_memo2_restrict st m)
         (RED : restrict d e1 e2 st m = Some(e',st',m')),
    wf_st st' /\ incr st st' /\
      (forall vi (WFE1 : wf_bdd st vi e1)
              (WFE2 : wf_bdd st vi e2),
          wf_bdd st' vi e') /\ wf_memo2_restrict st' m' /\
      (forall env r
              (EVALT : value env st e2 true)
              (EVALE : value env st e1 r),
          value env st' e' r).
Proof.
  induction d.
  - destruct e1.
    + intros. inv RED. split_and;auto.
    + intros. inv RED. split_and;auto.
    + intros. discriminate.
  - intros. destruct e1.
    + inv RED. split_and; auto.
    + inv RED. split_and; auto.
    +
      {
        rewrite restrict_eq in RED.
        destruct e2.
        - inv RED;split_and;auto.
          intros. inv EVALT.
        - inv RED;split_and;auto.
        - destruct (PPMap.find  (elt:=bdd) (id,id0) m) eqn:FD.
          { inv RED.
            split_and;auto.
            - destruct WFM;eauto.
            - intros.
              eapply wf_memo2_restrict_sem; eauto.
          }
          match goal with
          | H : memo _ _ ?X = _ |- _ =>
            assert (BDY : forall e'' st'' m'', X = Some (e'',st'',m'') ->
                    wf_st st'' /\
                      incr st st'' /\
                      (forall vi, wf_bdd st vi (N id) ->
                                 wf_bdd st vi (N id0) ->
                                 wf_bdd st'' vi e'') /\
                      wf_memo2_restrict st'' m'' /\
                      (forall (env : var -> bool) (r : bool)
                              (EVALT : value env st (N id0) true)
                              (EVALE:value env st (N id) r),value env st'' e'' r));
            [clear H|destruct X as [((e'',st''),m'')|];try discriminate;
            specialize (BDY e'' st'' m'' eq_refl) ]
        end.
          { intros e'' st'' m'' RED.
            destruct (Bdd.find (graph st) id) eqn: FDF; try discriminate.
            destruct n as ((l & v) & r).
            unfold bind3 at 1 in RED.
            destruct (Bdd.find (graph st) id0) as [((l' & v') & r') | ] eqn:FDC;
              try discriminate.
            unfold bind3 at 1 in RED.
            destruct ((v ?= v')%positive) eqn:CMP.
            + apply Pos.compare_eq in CMP.
              subst.
              destruct (is_F l') eqn:IFl'.
              { apply is_F_true in IFl'.
                subst.
                apply IHd  in RED;auto.
                destruct_and.
                split_and;auto.
                - intros.
                  eapply RED1.
                  eapply wf_bdd_le.
                  eapply wf_map_wf_bdd_h. eauto. eapply FDF.
                  inv H.
                  rewrite FDF in H2. inv H2.
                  lia.
                  eapply wf_bdd_le.
                  eapply wf_map_wf_bdd_h. eauto. eapply FDC.
                  inv H.
                  rewrite FDF in H2. inv H2.
                  lia.
                -
                  intros.
                  inv EVALT.
                  rewrite FDC in H0. inv H0.
                  destruct (env v) eqn:EV.
                  eapply RED5;eauto.
                  inv EVALE.
                  rewrite H0 in FDF. inv FDF.
                  rewrite EV in H2.
                  auto.
                  inv H1.
                - eapply wf_bdd_ex_find in FDF; eauto.
                  tauto.
                - eapply wf_bdd_ex_find in FDC; eauto.
                  tauto.
              }
              destruct (is_F r') eqn:IFr'.
              {
                apply is_F_true in IFr'.
                subst.
                apply IHd  in RED;auto.
                destruct_and.
                split_and;auto.
                -
                  intros.
                  eapply RED1.
                  eapply wf_bdd_le.
                  eapply wf_map_wf_bdd_l. eauto. eapply FDF.
                  inv H.
                  rewrite FDF in H2. inv H2.
                  lia.
                  eapply wf_bdd_le.
                  eapply wf_map_wf_bdd_l. eauto. eapply FDC.
                  inv H.
                  rewrite FDF in H2. inv H2.
                  lia.
                - intros.
                  inv EVALT.
                  rewrite FDC in H0. inv H0.
                  destruct (env v) eqn:EV.
                  inv H1.
                  eapply RED5;eauto.
                  inv EVALE.
                  rewrite H0 in FDF. inv FDF.
                  rewrite EV in H2.
                  auto.
                - eapply wf_bdd_ex_find in FDF;eauto.
                  tauto.
                - eapply wf_bdd_ex_find in FDC;eauto.
                  tauto.
              }
          destruct (restrict d l l' st m) eqn:RL; try discriminate.
          destruct p as ((l1,st1),m1).
          simpl in RED.
          destruct (restrict d r r' st1 m1) eqn:RR; try discriminate.
          destruct p as ((r1,st2),m2).
          simpl in RED. inv RED.
          apply IHd  in RL;auto.
          destruct_and.
          apply IHd in RR;auto.
          destruct_and.
          assert (WFL : wf_bdd st2 v' l1).
          {
            eapply wf_bdd_incr;eauto.
            apply RL1.
            eapply wf_map_wf_bdd_l in FDF;eauto.
            eapply wf_map_wf_bdd_l in FDC;eauto.
          }
           assert (WFH : wf_bdd st2 v' r1).
          {
            eapply RR1;eauto.
          }
          destruct (mk_node l1 v' r1 st2) as (er,str) eqn:MKNODE.
          inv H0.
          specialize (wb_mk_node _ _ _ _ RR0 WFL WFH st'' e'' (f_equal Some MKNODE)).
          simpl.
          intros ( WF1 & I2 & (NS1 & NS2) ).
          split_and;auto.
          * eapply incr_trans;eauto.
            eapply incr_trans;eauto.
          * intros.
            eapply NS1.
            inv H.
            rewrite H2 in FDF.
            inv FDF. auto.
          *
            assert (ISTST' : incr st st'').
            {
              eapply incr_trans;eauto.
              eapply incr_trans;eauto.
            }
            eapply wf_memo2_restrict_incr in RR3; eauto.
          * intros.
            inv EVALT.
            rewrite H0 in FDC.
            inv FDC.
            inv EVALE.
            rewrite H2 in FDF.
            inv FDF.
            eapply NS2; eauto.
            destruct (env v').
            { eapply RR5.
              eapply value_incr;eauto.
              eapply value_incr;eauto.
            }
            {
              eapply value_incr;eauto.
            }
          * eapply wf_bdd_ex_find in FDF;auto.
            destruct FDF as (FDF & _).
            destruct FDF as (vi & WFR).
            exists vi. eapply wf_bdd_incr;eauto.
          * eapply wf_bdd_ex_find in FDC;auto.
            destruct FDC as (FDC & _).
            destruct FDC as (vi & WFR).
            exists vi. eapply wf_bdd_incr;eauto.
          *  eapply wf_bdd_ex_find in FDF;eauto. tauto.
          *  eapply wf_bdd_ex_find in FDC;eauto. tauto.
          + rewrite Pos.compare_lt_iff in CMP.
            destruct (mk_or (S d) l' r') eqn:OR ; try discriminate.
            destruct p as (o,st1).
            eapply mk_or_correct in OR;eauto.
            destruct_and.
            destruct (restrict d (N id) o st1 m) eqn:RES;try discriminate.
            destruct p as ((er,str),mr).
            simpl in RED. inv RED.
            apply IHd  in RES;eauto.
            destruct_and.
            split_and;auto.
            * eapply incr_trans;eauto.
            * intros.
              eapply RES1.
              eapply wf_bdd_incr in H ;eauto.
              inv H0.
              rewrite H2 in FDC. inv FDC.
              eapply wf_bdd_lt; eauto.
            * intros.
              eapply RES5.
              inv EVALT.
              rewrite H0 in FDC. inv FDC.
              destruct (env v').
              assert (Hva : exists va, value env st l' va).
              {
                eapply value_exists; eauto.
              }
              destruct Hva as (va & Hva).
              rewrite <- orb_true_r with (b:= va) .
              eapply OR4; auto.
              assert (Hvb : exists vb, value env st r' vb).
              {
                eapply value_exists; eauto.
              }
              destruct Hvb as (vb & Hvb).
              rewrite <- orb_true_l with (b:= vb) .
              eapply OR4; auto.
              eapply value_incr;eauto.
            * eapply wf_memo2_restrict_incr with (hc1:=st); auto.
          + rewrite Pos.compare_gt_iff in CMP.
            destruct (restrict d l (N id0) st m) eqn:RL; try discriminate.
            destruct p as ((l1,st1),m1).
            simpl in RED.
            destruct (restrict d r (N id0) st1 m1) eqn:RR; try discriminate.
            destruct p as ((r1,st2),m2).
            simpl in RED.
            apply IHd  in RL;auto.
            destruct_and.
            apply IHd in RR;auto.
            destruct_and.
            assert (WFL : wf_bdd st2 v l1).
            {
              eapply wf_bdd_incr;eauto.
            }
            assert (WFH : wf_bdd st2 v r1).
            {
              eapply RR1.
              eapply wf_bdd_incr.
              eapply wf_map_wf_bdd_h in FDF;eauto.
              eapply incr_trans;eauto.
              eapply wf_bdd_le;eauto.
              lia.
            }
            inv RED.
            destruct (mk_node l1 v r1 st2) as (er,str) eqn:MKNODE.
            inv H0.
            specialize (wb_mk_node _ _ _ _ RR0 WFL WFH st'' e'' (f_equal Some MKNODE)).
            simpl.
            intros ( WF1 & I2 & (NS1 & NS2) ).
            split_and;auto.
            * eapply incr_trans;eauto.
              eapply incr_trans;eauto.
            * intros. apply NS1.
              inv H. rewrite FDF in H2. inv H2. auto.
            *
            assert (ISTST' : incr st st'').
            {
              eapply incr_trans;eauto.
              eapply incr_trans;eauto.
            }
            eapply wf_memo2_restrict_incr in RR3; eauto.
            * intros.
              inv EVALE.
              rewrite H0 in FDF. inv FDF.
              eapply NS2;eauto.
              destruct (env v) eqn:EV'.
              eapply RR5;eauto.
              eapply value_incr; eauto.
            * apply wf_bdd_ex_find in FDF;eauto.
              destruct FDF.
              destruct H.
              eexists. eapply wf_bdd_incr;eauto.
            * eexists. eapply wf_bdd_incr;eauto.
            * eexists. eauto.
      }
      { simpl in RED.
        inv RED.
        destruct_and.
        split_and;auto.
        apply wf_memo2_restrict_add; auto.
        - intros.
          apply BDY1;auto.
          destruct WFE1.
          eapply wf_bdd_rcni;eauto.
          destruct WFE2.
          eapply wf_bdd_rcni;eauto.
        - destruct WFE1 as (vi,WFE1).
          destruct WFE2 as (vi',WFE2).
          exists (Pos.max vi vi'); split.
          eapply wf_bdd_incr;eauto.
          eapply wf_bdd_le;eauto.
          lia.
          eapply wf_bdd_incr;eauto.
          eapply wf_bdd_le;eauto.
          lia.
        - intros.
          destruct WFE1.
          destruct WFE2.
          eapply BDY5.
          eapply value_rcni; eauto.
          eapply value_rcni; eauto.
      }
      }
Qed.

Definition is_wf_bdd (st:hashcons) (e:bdd) :=
  match e with
  | N x => PMap.find x (graph st) <> None
  | _   => True
  end.

Lemma wf_bdd_is_wf_bdd : forall st n,
  (exists v, wf_bdd st v n) <-> is_wf_bdd st n.
Proof.
  split ; intro.
  - destruct H.
    inv H; simpl; auto.
    congruence.
  - destruct n; simpl.
    exists xH; constructor.
    exists xH; constructor.
    simpl in H.
    destruct (PMap.find id (graph st)) eqn: FD; try congruence.
    destruct n as ((l,v),r).
    exists (Pos.succ v). econstructor; eauto.
    lia.
Qed.


Lemma restrict_ok :
  forall d e1 e2 st st' e' m m'
         (WF  : wf_st st)
         (WFE1 : is_wf_bdd st e1)
         (WFE2 : is_wf_bdd st e2)
         (WFM  : wf_memo2_restrict st m)
         (RED : restrict d e1 e2 st m = Some(e',st',m')),
    wf_st st' /\ incr st st'  /\
      is_wf_bdd st' e' /\
      wf_memo2_restrict st' m' /\
      (forall env r
              (EVALT : value env st e2 true)
              (EVALE : value env st e1 r),
          value env st' e' r).
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in WFE1.
  rewrite <- wf_bdd_is_wf_bdd in WFE2.
  destruct WFE1 as (v1, WFE1).
  destruct WFE2 as (v2, WFE2).
  eapply restrict_correct in RED ; eauto.
  destruct_and. split_and;auto.
  rewrite <- wf_bdd_is_wf_bdd.
  exists (Pos.max v1 v2).
  apply RED1 ; auto.
  eapply wf_bdd_le; eauto. lia.
  eapply wf_bdd_le; eauto. lia.
Qed.




(*
Module Memo3.

  Axiom t : Type.

  Axiom get : t -> (var * var) -> var -> option bdd.

End Memo3.

Fixpoint restrict_or (d:nat) (f1 f2 c:bdd) (st:state) (m2:memo2) (m3:Memo3.t) : option (bdd * state * memo2 * memo3) :=
  match f1  with
  | T  => Some(T,st,m2,m3)
  | F  => do e,st,m2 <- restrict d f2 c st m2 ;
          Some(e,st,m2,m3)
  | N f1p   =>
      match f2 with
      | T   => Some(T,st,m2,m3)
      | F   => do e,st,m2 <- restrict d f1 c st m2 ;
               Some(e,st,m2,m3)
      | N f2p  =>
          match c with
          | T  => do e,st <- mk_or d f1 f2 st ;
                  Some(e,st,m2,m3)
          | F  => Some (T,st,m2,m3)
          | N cp =>
              match Memo3.get (f1p,f2p) cp) m3 with
              | Some e' => Some(e',st,m2,m3)
              | None    =>
                  memo3 f1p f2p cp
                    (match PMap.find fp (graph st) , PMap.find cp (graph st) with
                     | Some(l,v,r) , Some(l',v',r') =>
                         match Pos.compare v v' with
                         | Eq =>
                             if is_F l'
                             then restrict d' r r' st m
                             else if is_F r' then  (restrict d' l l' st m)
                                  else
                                    do l,st,m <- restrict d' l l' st m ;
                                    do r,st,m <- restrict d' r r' st m ;
                                    let (e,st) := mk_node l v r st in
                                    Some ((e,st),m)
                         | Gt =>
                             do l,st,m <- restrict d' l c st m;
                          do r,st,m <- restrict d' r c st m;
                          let (e,st) := mk_node l v r st in
                          Some ((e,st),m)
                         | Lt =>
                             match mk_or d l' r' st with
                             | None => None
                             | Some (c',st) => restrict d' f c' st m
                             end
                         end
                     |  _ , _  => None
                     end)
              end
          end
      end
  end.
*)


(*
Fixpoint xcofactor (vr : var) (pol: bool) (d:nat)  (bdd: bdd)  (st: state) (m:memo1) : option (bdd * state * memo1) :=
  match bdd with
  | F => Some(F,st,m)
  | T => Some(T,st,m)
  | N p =>
      match d with
      | O => None
      | S d' =>
          match PMap.find p m with
          | Some e' => Some(e',st,m)
          | None    =>
              do nodea <- PMap.find p (graph st);
              let '(l,v,r) := nodea in
              match Pos.compare vr v with
              | Eq => Some (if pol then r else l, st,m)
              | Gt => Some (N p, st,m)
              | Lt => do l',st,m <- xcofactor vr pol d' l st m;
                      do r',st,m <- xcofactor vr pol d' r st m;
                      let (e,st) := mk_node l' v r' st in
                      Some ((e,st),PMap.add p e m)
              end
          end
      end
  end.


Record wf_memo_cofactor (x:var) (b:bool) (hc: hashcons) (m: memo1):=
  {
    wf_memo_cofactor_find_wf_res : forall x v e, PMap.find x m = Some e ->
                                            wf_bdd hc v (N x) -> wf_bdd hc v e;
    wf_memo_cofactor_find_wf : forall na e, PMap.find na m = Some e ->
                                       exists v, wf_bdd hc v (N na);
    wf_memo_cofactor_find_sem : forall na res, PMap.find na m = Some res ->
                                               (forall env r
                                                       (EVAL : value (add_env x b env) hc (N na) r),
                                                   value env hc res r)
  }.

Module MemoR.

  Definition t := PMap.t (PMap.t bdd * PMap.t bdd).

  Definition wf_memo (hc:hashcons) (m : t) :=
    forall v m1 m2, PMap.find v m = Some(m1,m2) ->
                    wf_memo_cofactor v false hc m1 /\
                      wf_memo_cofactor v true hc m2.

  Definition find (x:var) (b:bool) (m:t) :=
    match PMap.find x m with
    | None => PMap.empty _
    | Some (m1,m2) => if b then m2 else m1
    end.

  Definition add (x:var) (b:bool) (mx: PMap.t bdd) (m:t) :=
    match PMap.find x m with
    | None => PMap.add x (if b then (PMap.empty _,mx) else (mx,PMap.empty _)) m
    | Some(m1,m2) =>
        PMap.add x (if b then (m1,mx) else (mx,m2)) m
    end.

End MemoR.


Lemma incr_wf_memo_cofactor:
  forall x b (hc1 hc2 : hashcons) (memo : memo1),
  wf_hashcons hc1 ->
  wf_hashcons hc2 ->
  incr hc1 hc2 ->
  wf_memo_cofactor x b hc1 memo -> wf_memo_cofactor x b hc2 memo.
Proof.
  intros  x b hc1 hc2 memo Hwf1 Hwf2 Hincr Hwfm.
  constructor.
  - intros.
    assert (Hwf_bdd1:exists v, wf_bdd hc1 v (N x0)).
    { destruct Hwfm ; eauto. }
    destruct Hwf_bdd1 as [v' Hwf_bdd1].
    destruct Hwfm.
    eauto using wf_bdd_rcni.
  - destruct Hwfm. intros. edestruct wf_memo_cofactor_find_wf0; eauto.
  - destruct Hwfm.  intros.
    edestruct wf_memo_cofactor_find_wf0; eauto.
Qed.

Lemma wf_memo_cofactor_add :
  forall st v b m p ea
         (WF : wf_memo_cofactor v b st  m)
         (WFE : forall v, wf_bdd st v (N p) ->
                          wf_bdd st v ea)
         (WFEX : exists v : var, wf_bdd st v (N p))
         (WFEV : forall env r, value (add_env v b env) st (N p) r  ->
                             value env st ea r)
  ,
    wf_memo_cofactor v b st (PMap.add p ea m).
Proof.
  intros.
  inv WF.
  constructor ; auto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec x p).
    +  inv H.
       apply WFE; auto.
    + eauto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec na p).
    + inv H.
      auto.
    + eauto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec na p).
    + inv H.
      auto.
    + eauto.
Qed.


Lemma xcofactor_correct :
  forall d vi x b e e' st st' m m'
         (WF  : wf_st st)
         (WFE : wf_bdd st vi e)
         (WFM : wf_memo_cofactor x b st m)
         (RED : xcofactor x b d e st m = Some(e',st',m')),
    wf_st st' /\ incr st st' /\ wf_bdd st' vi e' /\ wf_memo_cofactor x b st' m' /\
      (forall env r
              (EVAL : value (add_env x b env) st e r),
          value env st' e' r).
Proof.
  induction d; simpl.
  - destruct e.
    + intros. inv RED. split_and;auto.
      intros. inv EVAL. constructor.
    + intros. inv RED. split_and;auto.
      intros. inv EVAL. constructor.
    + intros. discriminate.
  - intros. destruct e.
    + inv RED. split_and; auto.
      intros. inv EVAL.
      constructor.
    + inv RED. split_and; auto.
      intros. inv EVAL.
      constructor.
    + {
        destruct (Bdd.find m p) eqn:MEMO.
        { inv RED.
          split_and;auto.
          - eapply wf_memo_cofactor_find_wf_res;eauto.
          - intros.
            eapply wf_memo_cofactor_find_sem;eauto.
        }
        destruct (Bdd.find (graph st) p) eqn: FG; try discriminate.
        destruct n as ((l & v) & r).
        simpl in RED.
        destruct ((x ?= v)%positive) eqn:CMP.
        - inv RED.
          apply Pos.compare_eq in CMP.
          subst.
          split_and;auto.
          + inv WFE.
            rewrite H0 in FG. inv FG.
            destruct b.
            {
              eapply wf_map_wf_bdd_h in H0;eauto.
              eapply wf_bdd_lt; eauto.
            }
            {
              eapply wf_map_wf_bdd_l in H0;eauto.
              eapply wf_bdd_lt; eauto.
            }
          + intros.
            inv EVAL.
            rewrite H0 in FG.
            inv FG.
            rewrite gss in H1.
            rewrite <- value_independent in H1; auto.
            destruct b.
            eapply wf_map_wf_bdd_h in H0;eauto.
            eapply wf_map_wf_bdd_l in H0;eauto.
        -
          destruct (xcofactor x b d l st m) eqn:RL; try discriminate.
          destruct p0 as ((l',stl'),ml').
          simpl in RED.
          destruct (xcofactor x b d r stl' ml') eqn:RR; try discriminate.
          destruct p0 as ((r',str'),mr').
          simpl in RED.
          inv RED.
          apply IHd with (vi:=v) in RL;auto.
          destruct_and.
          apply IHd with (vi:=v) in RR;auto.
          destruct_and.
          destruct (mk_node l' v r' str') as (e,stn) eqn:MKNODE.
          inv H0.
          assert (WFL : wf_bdd str' v l').
          {
            inv WFE.
            rewrite H0 in FG.
            inv FG.
            eapply wf_bdd_incr;eauto.
          }
          assert (WFH : wf_bdd str' v r').
          {
            inv WFE.
            rewrite H0 in FG.
            inv FG.
            eapply wf_bdd_incr;eauto.
          }
          specialize (wb_mk_node _ _ _ _ RR0 WFL WFH st' e' (f_equal Some MKNODE)).
          simpl.
          intros ( WF1 & I2 & (NS1 & NS2) ).
          assert (EVAL :
                   forall env  (r0 : bool),
                     value (add_env x b env) st (N p) r0 -> value env st' e' r0).
          {
            intros env r0 EVAL.
            rewrite Pos.compare_lt_iff in CMP.
            inv EVAL.
            rewrite H0 in FG.
            inv FG.
            rewrite gso in H1.
            eapply NS2;eauto.
            destruct (env v).
            eapply RR5.
            eapply value_incr;eauto.
            eapply value_incr;eauto.
            lia.
          }
          split_and ; auto.
          +
            eapply incr_trans;eauto.
            eapply incr_trans;eauto.
          +
            inv WFE. rewrite FG in H0. inv H0.
            eapply NS1.
            lia.
          +
            apply wf_memo_cofactor_add.
            apply (incr_wf_memo_cofactor x b str' st' mr');auto.
            {
              intros.
              apply NS1.
              inv H.
              assert (INCR : incr st st').
              {
                eapply incr_trans;eauto.
                eapply incr_trans;eauto.
              }
              apply INCR in FG.
              rewrite H1 in FG.
              inv FG. auto.
            }
            {
              eexists. eauto.
            }
            {
              intros.
              assert (INCR : incr st st').
              {
                eapply incr_trans;eauto.
                eapply incr_trans;eauto.
              }
              eapply value_rcni in H; eauto.
            }
          + eapply wf_bdd_incr ; eauto.
          + eapply  wf_map_wf_bdd_l;eauto.
        -
          inv RED.
          split_and;auto.
          intros.
          rewrite Pos.compare_gt_iff in CMP.
          rewrite <- value_independent in EVAL; auto.
          inv WFE.
          inv EVAL.
          rewrite FG in H1. inv H1.
          rewrite FG in H0. inv H0.
          econstructor; eauto.
      }
Qed.

Lemma wf_memo_cofactor_empty : forall x b st,
  wf_memo_cofactor x b st (PMap.empty bdd).
Proof.
  constructor;auto.
  - intros. rewrite PMap.gempty in H.
    discriminate.
  - intros. rewrite PMap.gempty in H. discriminate.
  - intros. rewrite PMap.gempty in H. discriminate.
Qed.

Definition cofactor (vr : var) (pol: bool) (d:nat) (bdd: bdd)  (st: state)  :=
  do e' , st , u <- xcofactor vr pol  d bdd st (PMap.empty _) ;
  Some (e',st).

Lemma cofactor_correct :
  forall d vi x b e e' st st'
         (WF  : wf_st st)
         (WFE : wf_bdd st vi e)
         (RED : cofactor x b d e st = Some(e',st')),
    wf_st st' /\ incr st st' /\ wf_bdd st' vi e' /\
      (forall env r
              (EVAL : value (add_env x b env) st e r),
          value env st' e' r).
Proof.
  unfold cofactor.
  intros.
  destruct (xcofactor x b d e st (PMap.empty bdd)) eqn:XRES;try discriminate.
  inv RED. destruct p as ((er,str),mr).
  inv H0.
  eapply xcofactor_correct in XRES;eauto.
  destruct_and. split_and;auto.
  apply wf_memo_cofactor_empty.
Qed.

Definition add_list  (l: list (bool* var)) (val : PMap.t bool) :=
  List.fold_left (fun acc '(pol,vr) => PMap.add vr pol  acc) l val.


Fixpoint xcofactor_list (l : list (bool* var))  (d:nat)  (bdd: bdd)  (st: state) (m: MemoR.t) : option (bdd * state * MemoR.t) :=
  match l with
  | nil => Some (bdd,st,m)
  | (pol,vr)::l' =>
      let mvr := MemoR.find vr pol m in
      do e',st,mvr <- xcofactor vr pol d bdd st mvr;
      xcofactor_list l'  d e' st (MemoR.add vr pol mvr m)
  end.

(*
Fixpoint cofactor_list (l : list (bool* var))  (d:nat)  (bdd: bdd)  (st: state) : option (bdd * state) :=
  match l with
  | nil => Some (bdd,st)
  | (pol,vr)::l' => do e',st <- cofactor vr pol d bdd st;
              cofactor_list l'  d e' st
  end.
*)

Lemma PMap_swap :
  forall {A: Type} a a' b (m: pmap A),
    PMap.add a b (PMap.add a' b m) =
      PMap.add a' b (PMap.add a b m).
Proof.
  induction a; simpl.
  - destruct a'; simpl.
    destruct m.
    + f_equal.
      apply IHa.
    + f_equal. apply IHa.
    + destruct m; simpl.
      f_equal;auto.
      f_equal.
    + destruct m.
      f_equal;auto.
      f_equal;auto.
  - intros.
    induction a' ; simpl.
    + destruct m;simpl;auto.
    + destruct m;simpl;auto.
      f_equal;auto.
      f_equal;auto.
    + destruct m;simpl;auto.
  - intros.
    induction a' ; simpl.
    + destruct m;simpl;auto.
    + destruct m;simpl;auto.
    + destruct m;simpl; auto.
Qed.

Lemma PMap_swap_diff :
  forall {A: Type} a a' b b' (m: pmap A),
    a <> a' ->
    PMap.add a b (PMap.add a' b' m) =
      PMap.add a' b' (PMap.add a b m).
Proof.
  induction a; simpl.
  - destruct a'; simpl.
    intros.
    destruct m.
    + f_equal.
      apply IHa.
      congruence.
    + f_equal. apply IHa.
      congruence.
    + destruct m; simpl.
      intros.
      f_equal;auto.
      intros.
      f_equal.
    + destruct m.
      f_equal;auto.
      f_equal;auto.
  - intros.
    induction a' ; simpl.
    + destruct m;simpl;auto.
    + destruct m;simpl;auto.
      f_equal;auto.
      rewrite IHa. reflexivity.
      congruence.
      f_equal;auto.
      apply IHa;congruence.
    + destruct m;simpl;auto.
  - intros.
    induction a' ; simpl.
    + destruct m;simpl;auto.
    + destruct m;simpl;auto.
    + congruence.
Qed.



Lemma add_list_add :
  forall l a b env
         (NODUP1 : List.NoDup (a::List.map snd l)),
    add_list l (PMap.add a b env) =
      PMap.add a b (add_list l env).
Proof.
  induction l; simpl;auto.
  intros. destruct a.
  simpl in NODUP1.
  inv NODUP1.
  rewrite <- IHl; auto.
  assert (a0 <> p).
  { intro. subst. simpl in H1. tauto.
  }
  rewrite PMap_swap_diff by auto.
  reflexivity.
  { inv H2.
    constructor;auto.
    simpl in *.
    intros.
    destruct (Pos.eq_dec p a0); intuition congruence.
  }
Qed.

(*
Lemma cofactor_list_correct :
  forall l d vi e e'  st st'
         (ND : List.NoDup (List.map snd l))
         (WF  : wf_st st)
         (WFE : wf_bdd st vi e)
         (RED : cofactor_list l  d e st = Some(e',st')),
    wf_st st' /\ incr st st' /\ wf_bdd st' vi e' /\
      (forall env  r
              (EVAL : value (add_list l env) st e r),
          value env st' e' r).
Proof.
  induction l.
  - simpl. intros. inv RED.
    split_and;auto.
  - simpl.
    intros.
    destruct a as (pol,vr).
    destruct (cofactor vr pol d e st) eqn:RES.
    + simpl in RED. destruct p as (e1,st1).
      apply cofactor_correct with (vi:=vi) in RES;auto.
      destruct_and.
      apply IHl with (vi:= vi) in RED; auto.
      destruct_and.
      split_and;auto.
      eapply incr_trans;eauto.
      intros.
      eapply RED4.
      eapply RES4.
      rewrite <- add_list_add; auto.
      inv ND;auto.
    +
      simpl in RED. discriminate.
Qed.
*)



Fixpoint xreduce (f : var -> list (bool*var) * list (bool*var)) (d:nat) (bdd: bdd)  (st: state) (m:memo1) (mr : MemoR.t) :
  option (bdd * state * memo1 * MemoR.t) :=
  match bdd with
  | F => Some(F,st,m,mr)
  | T => Some(T,st,m,mr)
  | N p =>
      match PMap.find p m with
      | Some e' => Some(e',st,m,mr)
      | None    =>
          match d with
          | O => None
          | S d' =>
              do nodea <- PMap.find p (graph st);
              let '(l,v,r) := nodea in
              let (fn,fp) := f v in
              do l,st,mr <- xcofactor_list fn  d' l st mr;
              do r,st,mr <- xcofactor_list fp  d' r st mr;
              do r, st, m, mr <- xreduce f d' l st m mr ;
              do r, st, m, mr <- xreduce f d' r st m mr ;
              let (e',st) := (mk_node l v r st) in
              Some ((e',st),PMap.add p e' m, mr)
          end
      end
  end.


Definition eval_var (env:PMap.t bool) (vr:var) :=
  match PMap.find vr env with
  | None => false
  | Some b => b
  end.

Definition eval_lit (env: var -> bool) (l:bool *var):=
  if fst l then env (snd l) else
    negb (env (snd l)).

Definition tauto_list (P : (var -> bool) -> Prop) (f: var -> list (bool* var) * list (bool* var)) :=
  forall x,
  forall env (b:bool), P env ->
               List.forallb (eval_lit env) (if (env x) then snd (f x) else fst (f x)) = true.


Inductive ld_obool : option bool -> option bool -> Prop:=
| Ld_None : forall x, ld_obool None x
| Ld_Some : forall b, ld_obool (Some b) (Some b).

Definition ld_env (e1 e2: PMap.t bool) :=
  forall x, ld_obool (PMap.find x e1)
              (PMap.find x e2).

(*Lemma value_ld_env : forall e1 e2 st r b,
    ld_env e1 e2 ->
    value e1 st r b ->
    value e2 st r b.
Proof.
  intros. induction H0.
  - constructor.
  - constructor.
  - econstructor; eauto.
    specialize (H v).
    rewrite H1 in H.
    inv H.
    reflexivity.
Qed.

Lemma ld_obool_refl : forall v,
    ld_obool v v.
Proof.
  destruct v.
  constructor.
  constructor.
Qed.

Lemma ld_env_refl : forall env, ld_env env env.
Proof.
  repeat intro.
  apply ld_obool_refl.
Qed.

Lemma ld_env_trans : forall e1 e2 e3, ld_env e1 e2 -> ld_env e2 e3 ->
                                      ld_env e1 e3.
Proof.
  unfold ld_env; repeat intro.
  specialize (H x). specialize (H0 x).
  inv H. constructor.
  rewrite <- H3 in *. inv H0.
  constructor.
Qed.

Lemma ld_env_add_list_mono :
  forall fp e1 e2,
    ld_env e1 e2 ->
    ld_env (add_list fp e1) (add_list fp e2).
Proof.
  unfold add_list.
  induction fp.
  - simpl;auto.
  - simpl.
    intros.
    apply IHfp.
    destruct a.
    repeat intro.
    specialize (H x).
    rewrite! PositiveMapAdditionalFacts.gsspec.
    destruct (PMapFacts.eq_dec x p). constructor.
    auto.
Qed.

Lemma ld_env_add_list : forall fp env,
    forallb (eval_lit env) fp = true ->
    ld_env env (add_list fp env).
Proof.
  induction fp ; simpl.
  - unfold ld_env.
    intros. apply ld_obool_refl.
  - intros.
    rewrite andb_true_iff in H.
    destruct H as (EL & ALL).
    destruct a as (b,vr).
    apply IHfp in ALL.
    eapply ld_env_trans;eauto.
    apply ld_env_add_list_mono.
    repeat intro.
    rewrite! PositiveMapAdditionalFacts.gsspec.
    unfold eval_lit in EL.
    simpl in EL.
    unfold eval_var in EL.
    destruct (PMapFacts.eq_dec x vr).
    subst. destruct (Bdd.find env vr).
    destruct b0; simpl in EL. destruct b; try discriminate.
    constructor.
    destruct b; simpl in EL; try discriminate. constructor.
    constructor.
    apply ld_obool_refl.
Qed.
*)


(*Lemma xreduce_correct :
  forall P f d vi e e' st st'
         (ND1    : forall x, List.NoDup (map snd (fst (f x))))
         (ND2    : forall x, List.NoDup (map snd (snd (f x))))
         (TAUTO : tauto_list P f)
         (WF  : wf_st st)
         (WFE : wf_bdd st vi e)
         (RED : reduce f d e  st = Some(e',st')),
    wf_st st' /\ incr st st' /\ wf_bdd st' vi e' /\
      (forall env b
              (PENV : P env)
              (EVAL : value env st e b), value env st' e' b).
Proof.
  induction d; simpl.
  - destruct e.
    + intros. inv RED. split ; auto.
    + intros. inv RED. split;auto.
    + intros. discriminate.
  - intros. destruct e.
    + inv RED. split; auto.
    + inv RED. split;auto.
    + {
        destruct (Bdd.find (graph st) p) eqn: FG; try discriminate.
        destruct n as ((l & v) & r).
        simpl in RED.
        destruct (f v) as (fn & fp) eqn:FV.
        destruct (cofactor_list fn  d l st) eqn:RESN; try discriminate.
        simpl in RED.
        destruct p0 as (l'& stl').
        destruct (reduce f d l' stl') eqn:REDL ; try discriminate.
        destruct p0 as (l'' & stl'').
        simpl in RED.
        destruct (cofactor_list fp  d r stl'') eqn:RESP; try discriminate.
        simpl in RED.
        destruct p0 as (r' & str').
        destruct (reduce f d r' str') eqn:REDR; try discriminate.
        simpl in RED.
        destruct p0 as (r'' & str'').
        inv RED.
        apply cofactor_list_correct with (vi:=v) in RESN;auto.
        destruct_and.
        apply IHd with (vi:=v) in REDL;auto.
        destruct_and.
        apply cofactor_list_correct with (vi:=v) in RESP;auto.
        destruct_and.
        apply IHd with (vi:=v) in REDR;auto.
        destruct_and.
        assert (WFL : wf_bdd str'' v l'').
        {
          inv WFE.
          rewrite H1 in FG.
          inv FG.
          eapply wf_bdd_incr;eauto.
          eapply incr_trans;eauto.
        }
        assert (WFH : wf_bdd str'' v r'').
        {
          inv WFE.
          rewrite H1 in FG.
          inv FG.
          eapply wf_bdd_incr;eauto.
        }
        rename H0 into MKNODE.
        specialize (wb_mk_node _ _ _ _ REDR0 WFL WFH st' e' (f_equal Some MKNODE)).
        simpl.
        intros ( WF1 & I2 & (NS1 & NS2) ).
        split_and ; auto.
        +
          eapply incr_trans;eauto.
          eapply incr_trans;eauto.
          eapply incr_trans;eauto.
          eapply incr_trans;eauto.
        + inv WFE. rewrite FG in H0. inv H0.
          eapply NS1.
            lia.
        + intros.
          inv EVAL.
          inv WFE.
          rewrite H0 in FG.
          inv FG.
          rewrite H3 in H0. inv H0.
          eapply NS2;eauto.
          destruct vv.
          *
            eapply REDR4. auto.
            eapply RESP4;eauto.
            eapply value_incr with (st:= st); auto.
            apply TAUTO in H1.
            rewrite FV in H1.
            simpl in H1.
            eapply value_ld_env with (e1 := env);auto.
            eapply ld_env_add_list; eauto.
            auto.
            eapply incr_trans;eauto.
          *
            eapply value_incr with (st:= stl''); eauto.
            eapply REDL4. auto.
            eapply RESN4;eauto.
            apply TAUTO in H1.
            rewrite FV in H1.
            simpl in H1.
            eapply value_ld_env with (e1 := env);auto.
            eapply ld_env_add_list; eauto.
            auto.
            eapply incr_trans;eauto.
        +
          specialize (ND2 v).
          rewrite FV in ND2. auto.
        +
          eapply wf_bdd_incr;eauto.
        +
          specialize (ND1 v).
          rewrite FV in ND1. auto.
        +
          eapply wf_bdd_incr;eauto.
      }
Qed.
*)

Definition reduce (f : var -> list (bool*var) * list (bool*var)) (d:nat) (bdd: bdd)  (st: state) : option (bdd * state) :=
  do e,st,m,u <- xreduce f d bdd st (PMap.empty _) (PMap.empty _);
  Some (e,st).

(*
Lemma reduce_correct :
  forall P f d vi e e' st st'
         (ND1    : forall x, List.NoDup (map snd (fst (f x))))
         (ND2    : forall x, List.NoDup (map snd (snd (f x))))
         (TAUTO : tauto_list P f)
         (WF  : wf_st st)
         (WFE : wf_bdd st vi e)
         (RED : reduce f d e  st = Some(e',st')),
    wf_st st' /\ incr st st' /\ wf_bdd st' vi e' /\
      (forall env b
              (PENV : P env)
              (EVAL : value env st e b), value env st' e' b).
Proof.
*)
*)

Record wf_memo_copy (hc0: hashcons) (hc: hashcons) (m: memo1):=
  {
    wf_memo_copy_find_wf_res : forall x v e, PMap.find x m = Some e ->
                                            wf_bdd hc0 v (N x) -> wf_bdd hc v e;
    wf_memo_copy_find_wf : forall na e, PMap.find na m = Some e ->
                                       exists v, wf_bdd hc0 v (N na);
    wf_memo_copy_find_sem : forall na res, PMap.find na m = Some res ->
                                               (forall env r
                                                       (EVAL : value  env hc0 (N na) r),
                                                   value env hc res r)
  }.

Import PMap.

Fixpoint xcopy (st: state) (d:nat) (e : bdd) (st':state) (m:memo1): option  (bdd * state * memo1) :=
  match d with
  | O => None
  | S d' =>
      match e with
      | T | F => Some (e,st',m)
      | N p   =>
          match find p m with
          | Some e' => Some(e',st',m)
          | None    =>
              do l,v,r <- find p (graph st) ;
              do l',st',m <- xcopy st d' l st' m;
              do r',st',m <- xcopy st d' r st' m;
              let (e,st') := mk_node l' v r' st' in
              Some (e,st',add p e m)
          end
      end
  end.

Definition copy (st: state) (d:nat) (e : bdd)  : option (bdd * state) :=
  do e,st,u <- xcopy st d e Bdd.empty (PMap.empty _);
  Some (e,st).

Lemma incr_wf_memo_copy :
  forall st st1 st2 m
         (WF: wf_memo_copy st st1 m)
         (INCR : incr st1 st2),
    wf_memo_copy st st2 m.
Proof.
  intros.
  inv WF.
  constructor ;auto.
  - intros.
    eapply wf_bdd_incr; eauto.
(*  - intros.
    apply wf_memo_copy_find_wf0 in H.
    destruct H.
    exists x.
    eapply wf_bdd_incr; eauto. *)
  - intros.
    eapply wf_memo_copy_find_sem0 in H; eauto.
Qed.

Lemma wf_memo_copy_add :
  forall st st' m p ea
         (WF : wf_memo_copy st st' m)
         (WFE : forall v, wf_bdd st v (N p) ->
                          wf_bdd st' v ea)
         (WFEX : exists v : var, wf_bdd st v (N p))
         (WFEV : forall env r, value env st (N p) r  ->
                             value env st' ea r)
  ,
    wf_memo_copy st st' (PMap.add p ea m).
Proof.
  intros.
  inv WF.
  constructor ; auto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec x p).
    +  inv H.
       apply WFE; auto.
    + eauto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec na p).
    + inv H.
      auto.
    + eauto.
  - intros.
    rewrite PositiveMapAdditionalFacts.gsspec in H.
    destruct (PMapFacts.eq_dec na p).
    + inv H.
      auto.
    + eauto.
Qed.


Lemma xcopy_ok :
  forall d st e st' v0 e' st'' m m'
         (WFS  : wf_st st)
         (WFS' : wf_st st')
         (WFE  : wf_bdd st v0 e)
         (WFM  : wf_memo_copy st st' m)
         (CP   : xcopy st d e st' m = Some (e',st'',m')),
    wf_st st'' /\ incr st' st'' /\ wf_bdd st'' v0 e' /\ wf_memo_copy st st'' m' /\
      forall env b, value env st e b ->
                    value env st'' e' b.
Proof.
  induction d.
  - simpl. discriminate.
  - simpl. destruct e.
    + intros. inv CP.
      split_and;auto.
      intros. inv H ; constructor.
    + intros. inv CP.
      split_and;auto.
      intros. inv H ; constructor.
    + intros.
      destruct (Bdd.find m id) eqn:CACHE.
      {
        inv CP.
        split_and;auto.
        - destruct WFM.
          eapply wf_memo_copy_find_wf_res0;eauto.
        - intros.
          destruct WFM.
          eapply wf_memo_copy_find_sem0;eauto.
      }
      destruct (Bdd.find (graph st) id )eqn:FD ; try discriminate.
      simpl in CP.
      destruct n as ((l,v),h); simpl in *.
      destruct (xcopy st d l st' m) eqn:CP1; try discriminate.
      destruct p as ((l',stl'),ml').
      simpl in CP.
      destruct (xcopy st d h stl' ml') eqn:CP2; try discriminate.
      simpl in CP.
      destruct p as ((r',str'),mr').
      apply IHd with (v0:=v) in CP1;auto.
      destruct_and.
      apply IHd with (v0:=v) in CP2;auto.
      destruct_and.
      destruct (mk_node l' v r' str') as (er,str) eqn:MK.
      inv CP.
      assert (WFL : wf_bdd str' v l').
      {
        inv WFE.
        rewrite H0 in FD.
        inv FD.
        eapply wf_bdd_incr;eauto.
      }
      assert (WFR : wf_bdd str' v r').
      {
        inv WFE.
        rewrite H0 in FD.
        inv FD.
        eapply wf_bdd_incr;eauto.
      }
      specialize (wb_mk_node _ _ _ _ CP5 WFL WFR st'' e' (f_equal Some MK)).
      simpl.
      intros ( WF1 & I2 & (NS1 & NS2) ).
      assert (EVAL :
               forall env (r : bool), value env st (N id) r -> value env st'' e' r).
      {
        intros.
        inv WFE.
        rewrite H1 in FD. inv FD.
        inv H. rewrite H1 in H2. inv H2.
        eapply NS2.
        destruct (env v1).
        eapply CP10. auto.
        eapply value_incr.
        apply CP6. auto.
        auto.
      }
      split_and;auto.
      *  eapply incr_trans;eauto.
         eapply incr_trans;eauto.
      * eapply NS1.
        inv WFE. rewrite FD in H0. inv H0. auto.
      *
        apply wf_memo_copy_add.
        eapply incr_wf_memo_copy; eauto.
        {
          intros.
          inv H.
          rewrite H1 in FD. inv FD.
          eauto.
        }
        {
          eexists. eauto.
        }
        {
          auto.
        }
      * eapply wf_map_wf_bdd_h;eauto.
      * eapply wf_map_wf_bdd_l;eauto.
Qed.

Lemma wf_memo_copy_empty: forall st,
  wf_memo_copy st Bdd.empty (PMap.empty bdd).
Proof.
  intro.
  constructor; intros.
  rewrite PMap.gempty in H. discriminate.
  rewrite PMap.gempty in H. discriminate.
  rewrite PMap.gempty in H. discriminate.
Qed.

Lemma copy_correct :
  forall d st e v0 e' st''
         (WFS  : wf_st st)
         (WFE  : wf_bdd st v0 e)
         (CP   : copy st d e  = Some (e',st'')),
    wf_st st''  /\ wf_bdd st'' v0 e' /\
      forall env b, value env st e b ->
                    value env st'' e' b.
Proof.
  unfold copy.
  intros.
  destruct (xcopy st d e Bdd.empty (PMap.empty bdd)) eqn:CPX;
    try discriminate.
  simpl in CP. destruct p as ((e2,st2),m2).
  inv CP.
  eapply xcopy_ok in CPX;eauto.
  - destruct_and. split_and;auto.
  - apply wf_empty.
  - apply wf_memo_copy_empty.
Qed.

Lemma copy_correct' :
  forall d st e  e' st''
         (WFS  : wf_st st)
         (WFE  : is_wf_bdd st  e)
         (CP   : copy st d e  = Some (e',st'')),
    wf_st st''  /\ is_wf_bdd st'' e' /\
      forall env b, value env st e b ->
                    value env st'' e' b.
Proof.
  intros.
  rewrite <- wf_bdd_is_wf_bdd in WFE.
  destruct WFE as (v0& WFE).
  eapply copy_correct in CP ; eauto.
  destruct_and;split_and;auto.
  rewrite <- wf_bdd_is_wf_bdd.
  eexists; eauto.
Qed.
