From compcert Require Import Integers Floats AST Ctypes Clight Clightdefs Globalenvs Smallstep Errors.
Require Import Common Bdd Filter Filter2formula Formula2fdd Fdd2clight.
Require Import MoreErrors String.

Import ListNotations.

Definition pol2clight (ord : predform -> list pred) (opt_reject: bool) (pp:bool) (red : bool)  (gc:bool) (pf : pfilter)
  (pstruct:cstruct) : res (Clight.function * Clight.program) :=
  if wf_members_dec (snd pstruct) then
    match formula2bdd ord pf opt_reject pp red gc with
    | Some  (e, st, v2p) => bdd2clight v2p e st pstruct
    | None => Error (msg "formula2bdd failed"%string)
    end
  else Error (msg "structure is not well-defined"%string).

Theorem pol2clight_correct :
  forall (order_preds : predform -> list pred) (pf : pfilter) (pstruct: cstruct) (a : action) (filter : Clight.function) (prog : Clight.program) (pk : packet) b k o m opt_reject pp red gc
  (Hload : packet_repr (genv_cenv (globalenv (Clight.semantics2 prog))) pk pstruct m  b o),
  pol2clight order_preds opt_reject pp red gc pf pstruct = OK (filter, prog) ->
  exec pk pf = a ->
  Star (Clight.semantics2 prog) (Callstate (Ctypes.Internal filter) [(Values.Vptr b o)] k m) [] (Returnstate (bool2val (action2bool a)) (call_cont k) m).
Proof.
  intros. simpl in H. unfold pol2clight in H. destruct (wf_members_dec (snd pstruct)) in H; try discriminate.
  destruct (formula2bdd order_preds pf opt_reject pp red) eqn:FDD; try discriminate.
  destruct p as ((e,st),v2p).
  eapply formula2fdd_correct with (pk := pk) in FDD; eauto.
  eapply bdd2clight_correct; eauto.
Qed.
