(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          Xavier Leroy, INRIA Paris-Rocquencourt                     *)
(*                                                                     *)
(*  Copyright Institut National de Recherche en Informatique et en     *)
(*  Automatique.  All rights reserved.  This file is distributed       *)
(*  under the terms of the GNU Lesser General Public License as        *)
(*  published by the Free Software Foundation, either version 2.1 of   *)
(*  the License, or  (at your option) any later version.               *)
(*  This file is also distributed under the terms of the               *)
(*  INRIA Non-Commercial License Agreement.                            *)
(*                                                                     *)
(* *********************************************************************)

From compcert Require Coqlib.
(*From compcert Require Wfsimpl.*)
(*From compcert Require DecidableClass Decidableplus.*)
From compcert Require AST.
From compcert Require Iteration.
From compcert Require Floats.
From compcert Require Machregs.
From compcert Require Ctyping.
From compcert Require Ctypes.
From compcert Require Clight.
From compcert Require Initializers.
From pfcomp Require Filter2formula Pfcompiler Naive NForm.

(* Standard lib *)
Require Import ExtrOcamlBasic.
Require Import ExtrOcamlString.

(* Coqlib *)
Extract Inlined Constant Coqlib.proj_sumbool => "(fun x -> x)".

(* Datatypes *)
Extract Inlined Constant Datatypes.fst => "fst".
Extract Inlined Constant Datatypes.snd => "snd".

(* Errors *)
Extraction Inline Errors.bind Errors.bind2.

Load extractionMachdep.

(* Avoid name clashes *)
Extraction Blacklist List String Int Bdd.

(* Needed in Coq 8.4 to avoid problems with Function definitions. *)
Set Extraction AccessOpaque.

(* Go! *)

Cd "extraction".

Separate Extraction
   Ctypesdefs.string_of_ident
   BinPos.Pos.pred BinInt.Z.succ
   Integers.Ptrofs.signed Floats.Float.of_bits
   Floats.Float.to_bits Floats.Float32.to_bits Floats.Float32.of_bits
   Floats.Float32.from_parsed  Floats.Float.from_parsed
   Machregs.register_names Machregs.register_by_name
   SpecFloat Archi AST Memdata Initializers Ctyping
   Ctypes Clight
   Filter2formula.tok_form_default
   Pfcompiler.pol2clight
   Filter.Clause.all_diff_tauto
   Filter2formula.gen_clauses
   Filter2formula.gen_clause_list1
   Filter2formula.gen_clause_list2
   Filter2formula.gen_clause_list3
   Filter2formula.gen_clause_list4
   Naive.filter2c_naive
   NForm.opt_formula
.
