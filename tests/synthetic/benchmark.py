#!/usr/bin/python3

import argparse
import csv
import re
import subprocess
from enum import Enum
from pathlib import Path
from timeit import timeit

import pandas as pd
from matplotlib import pyplot as plt

# Parsing command line
parser = argparse.ArgumentParser(description="Synthetic Benchmarks Tool")

subparsers = parser.add_subparsers(dest="command")

# Run command
run_parser = subparsers.add_parser("run", help="Run the benchmark. More information with './benchmark run --help'",
                                   formatter_class=argparse.ArgumentDefaultsHelpFormatter)
run_parser.add_argument("--min-rules", default=100, type=int, help="Minimum number of rules in filter files")
run_parser.add_argument("--max-rules", default=3000, type=int,
                        help="Maximum number of rules in filter files (included)")
run_parser.add_argument("--step-rules", default=100, type=int, help="Step for the number of rules in filter files")
run_parser.add_argument("--draw-graphs", action="store_true", help="Draw the result graphs")
run_parser.add_argument("--append", action="store_true",
                        help="Append the benchmarks results in the CSV files instead of overwriting them")

# Draw command
draw_parser = subparsers.add_parser("draw", help="Draw the benchmark result graphs")

# Clean command
clean_parser = subparsers.add_parser("clean", help="Clean the generated files and associated results")

args = parser.parse_args()

# Column indexes of the firewall log
IDX_SPORT = 0
IDX_DPORT = 1
IDX_NAT_SPORT = 2
IDX_NAT_DPORT = 3
IDX_ACTION = 4

FILTER_TEMPLATE = """STRUCT Packet {
\tsport: u32;
\tdport: u32;
\tnat_sport: u32;
\tnat_dport: u32;
}

POLICY {
<RULES>}
"""

# File and directory constants
FILE_PACKETS = "packets.bin"
FILE_MAIN = "main.c"
CSV_THROUGHPUT = "throughput.csv"
CSV_NODES = "nodes.csv"
CSV_COMP = "comp.csv"
CSV_MEM = "mem.csv"
FILE_LOG = "firewall_log.csv"
IMG_THROUGHPUT = "throughput.png"
IMG_NODES = "nodes.png"
IMG_NODES_O1 = "nodes_o1.png"
IMG_COMP_TIME = "comp.png"
IMG_MEM = "mem.png"
DIR_FILTER = "filters/"
DIR_C = "C/"
DIR_BIN = "bin/"

# Compilers
PFC = "../../pfc"
CC = "../../CompCert/ccomp"

# Memory usage management shell command
MEM_USAGE_CMD = ["/usr/bin/time", "-f", "'Max memory usage: %M'", "-o", "/dev/fd/1", "-a"]

# Log data
NB_PACKETS = 65532
PACKET_SIZE = 16  # In bytes (4 fields of 4 bytes)

# Threshold not to exceed for some benchmarks
MAX_RULES_NOGC = 200
MAX_RULES_O0_PFC = 2600
MAX_RULES_O0_THROUGHPUT = 2100


# Class representing a rule extracted from a firewall log
class Rule:
    def __init__(self, sport: int, dport: int, nat_sport: int, nat_dport: int, action: str):
        self.sport = sport
        self.dport = dport
        self.nat_sport = nat_sport
        self.nat_dport = nat_dport
        self.action = Rule.dsl_action(action)

    def __eq__(self, other):
        return self.sport == other.sport and self.dport == other.dport and self.nat_sport == other.nat_sport and self.nat_dport == other.nat_dport

    def __hash__(self):
        return hash((self.sport, self.dport, self.nat_sport, self.nat_dport))

    @staticmethod
    def dsl_action(a):
        if a == "allow":
            return "accept"
        else:
            return "reject"


# Normalize a directory name
def set_dir(dir):
    return str(Path(dir)) + "/"


# Returns a string with tabulation set to 4 spaces
def xstr(s):
    return s.expandtabs(4)


# Extracts the rules of the firewall log
def get_rules(max):
    # Open the log file
    input = open(FILE_LOG, "r")
    csv_reader = csv.reader(input)

    # Skip the first line
    next(csv_reader)

    # Build the list of rules
    rule_set = set()
    rule_list = []

    i = 0
    for row in csv_reader:
        if i >= max:
            break
        elif row[IDX_ACTION] == "allow":
            rule = Rule(int(row[IDX_SPORT]), int(row[IDX_DPORT]), int(row[IDX_NAT_SPORT]), int(row[IDX_NAT_DPORT]),
                        row[IDX_ACTION])
            if rule not in rule_set:
                rule_set.add(rule)
                rule_list.append(rule)
                i += 1
    input.close()

    return rule_list


# Generates the filter files from a list of rules
def rules_to_filter(rules):
    policy = ""

    for rule in rules:
        policy += ("\t((sport = {} and dport = {}) and (nat_sport = {} and nat_dport = {})): {};\n"
                   .format(rule.sport, rule.dport, rule.nat_sport, rule.nat_dport, rule.action))

    filter = FILTER_TEMPLATE.replace("<RULES>", policy)

    output_name = DIR_FILTER + "filter" + str(len(rules)) + ".pf"

    output = open(output_name, "w")

    output.write(xstr(filter))

    output.close()


# Returns the number of different predicates contained in a list of rules
def nb_predicates(rules):
    dict = {"sport": set(), "dport": set(), "nat_sport": set(), "nat_dport": set()}

    for rule in rules:
        dict["sport"].add(rule.sport)
        dict["dport"].add(rule.dport)
        dict["nat_sport"].add(rule.nat_sport)
        dict["nat_dport"].add(rule.nat_dport)

    res = 0

    for key in dict.keys():
        res += len(dict.get(key))

    return res


# Generates all the necessary filter files
def gen_filters():
    for i in range(args.min_rules, args.max_rules + 1, args.step_rules):
        rules = get_rules(i)
        rules_to_filter(rules)


# Creates the binary file containing the packet data
def create_packets():
    # Open the log file
    csv_log = open(FILE_LOG, "r")
    csv_reader = csv.reader(csv_log)

    # Skip the first line
    next(csv_reader)

    # Open the output file
    output = open(FILE_PACKETS, "wb")

    for row in csv_reader:
        packet = [int(row[IDX_SPORT]), int(row[IDX_DPORT]), int(row[IDX_NAT_SPORT]), int(row[IDX_NAT_DPORT])]

        for byte in packet:
            output.write(byte.to_bytes(4, byteorder="little"))

    csv_log.close()
    output.close()


# Returns the size of a file in kB (rounded with 1 decimal)
def get_file_size(file):
    return round(Path(file).stat().st_size / 1000, 1)


# Compiler modes used in the benchmarks
class CompilerMode(Enum):
    O0 = 1
    O1 = 2
    NAIVE = 3
    NOGC = 4


# Returns the command to compile a filtering policy and the associated output name
def pfc_setup(mode, file):
    c_out = file.replace(DIR_FILTER, DIR_C).replace(".pf", ".c")
    cmd = ["-pp"]

    match mode:
        case CompilerMode.O1:
            cmd = cmd + ["-O1"]
            c_out = c_out.replace(".c", "-o1.c")
        case CompilerMode.NAIVE:
            cmd = cmd + ["-compiler", "naive"]
            c_out = c_out.replace(".c", "-naive.c")
        case CompilerMode.NOGC:
            cmd = cmd + ["-O1", "-no-gc"]
            c_out = c_out.replace(".c", "-nogc.c")

    cmd = MEM_USAGE_CMD + [PFC, file, "-o", c_out] + cmd

    return c_out, cmd


# Returns the command to compile the C version of a filtering policy and the associated output name
def ccomp_setup(file):
    bin_out = file.replace(DIR_C, DIR_BIN).replace(".c", "")
    cmd = [CC, file, FILE_MAIN, "-o", bin_out]
    return bin_out, cmd


# Times a command and rounds the result
def time_cmd(cmd):
    print(">>> Compiling " + ' '.join(cmd) + "\n")
    return round(timeit(lambda: subprocess.run(cmd), number=1), 3)


# Gets the time and memory usage of pfc-compilation command
def time_mem_cmd(cmd):
    print(">>> Compiling " + ' '.join(cmd) + "\n")
    cmd_res = subprocess.run(cmd, text=True, capture_output=True).stdout
    time = round(float(re.search("COMPILER: ([0-9]+.[0-9]+)", cmd_res).group(1)), 3)
    mem = int(re.search("Max memory usage: ([0-9]+)", cmd_res).group(1)) / 1000
    return time, mem


# Gets the output of a command as an utf8-encoded string
def check_output_cmd(cmd):
    return subprocess.check_output(cmd, text=True)


# Returns the filtering time from a command running a binary filtering policy
def get_filter_time(cmd):
    print(">>> Filtering time measured on " + cmd + "\n")
    res = check_output_cmd(cmd)
    return int(re.search("([0-9]+)\\smicros", res).group(1))


# Computes the throughput given the filtering time
def compute_throughput(time):
    return int((PACKET_SIZE * NB_PACKETS) / time)


# Initializes a dictionary with a default value
def init_dict(dict, val):
    for key in dict.keys():
        dict[key] = val


# Append dictionary values into a string
def dict_to_str(dict):
    s = ""
    for value in dict.values():
        if int(value) == -1:
            s += ","
        else:
            s += str(value) + ","
    return s[:-1]


# Counts the number of "if" in a file
def count_if(file):
    return len(re.findall("if", file))

# Core function that runs the benchmarks
def run_benchmarks():
    mode = 'a' if args.append else 'w'

    csv_nodes = open(CSV_NODES, mode)
    csv_comp = open(CSV_COMP, mode)
    csv_throughput = open(CSV_THROUGHPUT, mode)
    csv_mem = open(CSV_MEM, mode)

    common_headers = "nb_rules,nb_predicates"

    # Write column headers in CSV files
    if not args.append:
        csv_nodes.write("{},nb_nodes,nb_nodes_o1,nb_nodes_nogc\n".format(common_headers))

        csv_comp.write(
            "{},pfc_time,pfc_time_o1,pfc_time_naive,total_time,total_time_o1,total_time_naive\n"
            .format(common_headers))

        csv_throughput.write(
            "{},filter_time,filter_time_o1,filter_time_naive,throughput,throughput_o1,throughput_naive\n"
            .format(common_headers))

        csv_mem.write("{},mem,mem_o1,mem_nogc,mem_naive\n".format(common_headers))

    nodes = dict.fromkeys(["nb_nodes", "nb_nodes_o1", "nb_nodes_nogc"], -1)

    comp = dict.fromkeys(["pfc_time", "pfc_time_o1", "pfc_time_naive",
                          "total_time", "total_time_o1", "total_time_naive"], -1.)

    throughput = dict.fromkeys(["filter_time", "filter_time_o1", "filter_time_naive",
                                "throughput", "throughput_o1", "throughput_naive"], -1.)

    mem = dict.fromkeys(["mem", "mem_o1", "mem_nogc", "mem_naive"])

    for i in range(args.min_rules, args.max_rules + 1, args.step_rules):
        init_dict(nodes, -1)
        init_dict(comp, -1.)
        init_dict(throughput, -1.)
        init_dict(mem, -1.)

        rules = get_rules(i)
        nb_preds = nb_predicates(rules)

        file = DIR_FILTER + "filter" + str(i) + ".pf"

        # Get the C output file name and the pfc command to run
        c_out, pfc_cmd = pfc_setup(CompilerMode.O0, file)
        c_out_o1, pfc_cmd_o1 = pfc_setup(CompilerMode.O1, file)
        c_out_naive, pfc_cmd_naive = pfc_setup(CompilerMode.NAIVE, file)
        c_out_nogc, pfc_cmd_nogc = pfc_setup(CompilerMode.NOGC, file)

        # Time the compilation of filters to C programs
        comp["pfc_time_o1"], mem["mem_o1"] = time_mem_cmd(pfc_cmd_o1)
        comp["pfc_time_naive"], mem["mem_naive"] = time_mem_cmd(pfc_cmd_naive)

        c_file_o1 = open(c_out_o1).read()

        nodes["nb_nodes_o1"] = count_if(c_file_o1)

        if i <= MAX_RULES_O0_PFC:
            comp["pfc_time"], mem["mem"] = time_mem_cmd(pfc_cmd)
            c_file = open(c_out).read()
            nodes["nb_nodes"] = count_if(c_file)

        if i <= MAX_RULES_NOGC:
            _, mem["mem_nogc"] = time_mem_cmd(pfc_cmd_nogc)
            c_file_nogc = open(c_out_nogc).read()
            nodes["nb_nodes_nogc"] = count_if(c_file_nogc)

        common_data = "{},{}".format(i, nb_preds)

        csv_nodes.write("{},{}\n".format(common_data, dict_to_str(nodes)))

        csv_nodes.flush()

        csv_mem.write("{},{}\n".format(common_data, dict_to_str(mem)))

        csv_mem.flush()

        # Get the bin output file name and the ccomp command to run
        # We do not compile the c_out_nogc file
        bin_out, ccomp_cmd = ccomp_setup(c_out)
        bin_out_o1, ccomp_cmd_o1 = ccomp_setup(c_out_o1)
        bin_out_naive, ccomp_cmd_naive = ccomp_setup(c_out_naive)

        # Time the compilation of C programs to binary executable
        ccomp_time_o1 = time_cmd(ccomp_cmd_o1)
        ccomp_time_naive = time_cmd(ccomp_cmd_naive)

        # Total compilation time
        comp["total_time_o1"] = comp["pfc_time_o1"] + ccomp_time_o1
        comp["total_time_naive"] = comp["pfc_time_naive"] + ccomp_time_naive

        # Get the filtering throughput
        throughput["filter_time_o1"] = get_filter_time(bin_out_o1)
        throughput["filter_time_naive"] = get_filter_time(bin_out_naive)

        throughput["throughput_o1"] = compute_throughput(throughput["filter_time_o1"])
        throughput["throughput_naive"] = compute_throughput(throughput["filter_time_naive"])

        if i <= MAX_RULES_O0_THROUGHPUT:
            ccomp_time = time_cmd(ccomp_cmd)
            comp["total_time"] = comp["pfc_time"] + ccomp_time
            throughput["filter_time"] = get_filter_time(bin_out)
            throughput["throughput"] = compute_throughput(throughput["filter_time"])

        # Write in csv_comp_time
        csv_comp.write(
            "{},{}\n"
            .format(common_data, dict_to_str(comp)))
        csv_comp.flush()

        # Write in csv_throughput
        csv_throughput.write(
            "{},{}\n".format(common_data, dict_to_str(throughput)))
        csv_throughput.flush()

    csv_nodes.close()
    csv_comp.close()
    csv_throughput.close()
    csv_mem.close()


# Draws benchmark graphs from CSV results files
def draw_graphs():
    if Path(CSV_NODES).is_file() \
            and Path(CSV_THROUGHPUT).is_file() \
            and Path(CSV_COMP).is_file() \
            and Path(CSV_MEM).is_file():

        label_bdd_o0 = "pfcomp -O0"
        label_bdd_o1 = "pfcomp -O1"
        label_naive = "naive compiler"
        label_bdd_nogc = "pfcomp -O1 -no-gc"

        fig, ax = plt.subplots(1, 1, dpi=250)

        # Throughput
        data_throughput = pd.read_csv(CSV_THROUGHPUT)
        nb_preds = data_throughput["nb_predicates"]
        # nb_rules = data_throughput["nb_rules"]
        throughput = data_throughput["throughput"]
        throughput_o1 = data_throughput["throughput_o1"]
        throughput_naive = data_throughput["throughput_naive"]

        ax.plot(nb_preds, throughput, "-x", label=label_bdd_o0)
        ax.plot(nb_preds, throughput_o1, "-o", label=label_bdd_o1)
        ax.plot(nb_preds, throughput_naive, "--x", label=label_naive)
        ax.set_xlabel("Number of predicates", labelpad=6)
        ax.set_ylabel("Throughput (MBps, log scale)", labelpad=6)
        ax.set_yscale('log')
        ax.legend()
        plt.savefig(IMG_THROUGHPUT)

        ax.cla()

        # Nodes O0 + O1
        fig.set_figwidth(8)
        data_nodes = pd.read_csv(CSV_NODES)
        nb_preds = data_nodes["nb_predicates"]
        nb_nodes = data_nodes["nb_nodes"]
        nb_nodes_o1 = data_nodes["nb_nodes_o1"]
        nb_nodes_nogc = data_nodes["nb_nodes_nogc"]

        ax.plot(nb_preds, nb_nodes, "-x", label=label_bdd_o0)
        ax.plot(nb_preds, nb_nodes_o1, "-o", label=label_bdd_o1)
        ax.plot(nb_preds, nb_nodes_nogc, "--s", label=label_bdd_nogc, color="lightseagreen")
        ax.set_xlabel("Number of predicates", labelpad=6)
        ax.set_ylabel("Number of nodes in the C file", labelpad=6)
        ax.legend()
        plt.savefig(IMG_NODES)

        ax.cla()

        # Nodes O1 only
        fig.set_figwidth(8)
        ax.plot(nb_preds, nb_nodes_o1, "-o", color="darkorange", label=label_bdd_o1)
        ax.set_xlabel("Number of predicates", labelpad=6)
        ax.set_ylabel("Number of nodes in the C file", labelpad=6)
        ax.legend()
        plt.savefig(IMG_NODES_O1)

        ax.cla()

        # Compilation time
        fig.set_figwidth(6.4)
        data_comp_time = pd.read_csv(CSV_COMP)
        nb_rules = data_comp_time["nb_rules"]
        comp_time = data_comp_time["pfc_time"]
        comp_time_o1 = data_comp_time["pfc_time_o1"]
        comp_time_naive = data_comp_time["pfc_time_naive"]

        ax.plot(nb_rules, comp_time, "-x", label=label_bdd_o0)
        ax.plot(nb_rules, comp_time_o1, "-o", label=label_bdd_o1)
        ax.plot(nb_rules, comp_time_naive, "--x", label=label_naive)
        ax.set_xlabel("Number of rules", labelpad=6)
        ax.set_ylabel("pfcomp compilation time (s)", labelpad=6)
        ax.legend()
        plt.savefig(IMG_COMP_TIME)

        ax.cla()

        # Memory usage
        fig.set_figwidth(7)
        data_mem = pd.read_csv(CSV_MEM)
        nb_preds = data_mem["nb_predicates"]
        mem = data_mem["mem"]
        mem_o1 = data_mem["mem_o1"]
        mem_nogc = data_mem["mem_nogc"]
        mem_naive = data_mem["mem_naive"]

        ax.plot(nb_preds, mem, "-x", label=label_bdd_o0)
        ax.plot(nb_preds, mem_o1, "-o", label=label_bdd_o1)
        ax.plot(nb_preds, mem_nogc, "--s", label=label_bdd_nogc, color="lightseagreen")
        ax.plot(nb_preds, mem_naive, "--x", label=label_naive)
        ax.set_xlabel("Number of predicates", labelpad=6)
        ax.set_ylabel("Maximum memory resident set size (MB)", labelpad=6)
        ax.legend()
        plt.savefig(IMG_MEM)

        ax.cla()
    else:
        print("Error: cannot draw result graphs, some files are missing.")
        exit(1)


# Cleans the file generated by the benchmarks
def clean():
    rm_files = [FILE_PACKETS]
    rm_csv = [FILE_PACKETS, CSV_COMP, CSV_NODES, CSV_THROUGHPUT, CSV_MEM]
    rm_img = [IMG_COMP_TIME, IMG_NODES, IMG_THROUGHPUT, IMG_NODES_O1, IMG_MEM]
    rm_dir = [DIR_FILTER, DIR_C, DIR_BIN]
    cmd = ["rm", "-r", "-f"]

    if args.command == "run" and not args.append:
        subprocess.run(cmd + rm_csv)
    subprocess.run(cmd + rm_files)
    subprocess.run(cmd + rm_img)
    subprocess.run(cmd + rm_dir)


# Creates the necessary directories
def create_directories():
    for dir in [DIR_FILTER, DIR_BIN, DIR_C]:
        Path(dir).mkdir(parents=True, exist_ok=True)


# Main (entry-point) function to run the benchmarks
def run():
    if not Path(CC).is_file():
        print("Error: ccomp executable not found, please build the project before running tests.")
        exit(1)
    elif not Path(PFC).is_file():
        print("Error: pfc executable not found, please build the project before running tests.")
        exit(1)
    else:
        create_directories()
        create_packets()
        gen_filters()
        run_benchmarks()


if __name__ == "__main__":
    match args.command:
        case "clean":
            clean()
        case "run":
            print("Cleaning old benchmarks files...")
            clean()
            print("Benchmarks started...")
            run()
            if args.draw_graphs:
                draw_graphs()
            print("Benchmarks completed successfully!")
        case "draw":
            draw_graphs()
        case _:
            parser.print_help()
