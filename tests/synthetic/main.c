#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#define NB_PACKETS 65532
#define NB_MESURES 1000

struct Packet {
  unsigned int sport;
  unsigned int dport;
  unsigned int nat_sport;
  unsigned int nat_dport;
}__attribute__((packed));

typedef struct Packet PACKET;

extern _Bool filter(PACKET *);

int main() {
  unsigned buffer[NB_PACKETS * sizeof(PACKET)];

  FILE *stream;

  PACKET *pks;

  stream = fopen("./packets.bin", "rb");

  if (stream == NULL) {
    fprintf(stderr, "Error when opening the binary file contaning packet data.\n");
    exit(1);
  }

  fread(buffer, sizeof(PACKET), NB_PACKETS, stream);

  pks = (PACKET *)buffer;

//   for (int i = 0; i < NB_PACKETS; i++) {
//     PACKET pk = pks[i];
//     printf("%u %u %u %u\n", pk.sport, pk.dport, pk.nat_sport, pk.nat_dport);
//   }

  struct timeval start, end;

  long accumulated = 0;

  for (int i = 0; i < NB_MESURES; i++) {

    gettimeofday(&start, NULL);

    for (int i = 0; i < NB_PACKETS; i++) {
      filter(&pks[i]);
    }

    gettimeofday(&end, NULL);

    long seconds = (end.tv_sec - start.tv_sec);
    long micros = ((seconds * 1000000L) + end.tv_usec) - (start.tv_usec);

    accumulated += micros;

  }

  long res = (accumulated/NB_MESURES);
  printf("Filtering time: %lu micros\n", res);

  return 0;
}
