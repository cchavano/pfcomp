# Prerequisites

- make
- linux-headers
- nftables
- iperf3

# Description

In this benchmark, we evaluate the performance of the following packet filters:
- nft: nftables without a named set
- nftset: nftables with a named set
- pfc: pfcomp + compcert

To do so, we set up two network namespaces (netns) called client and server, and connect them through a virtual ethernet cable.

We then load filtering rules into the INPUT netfilter hook of netns server. Default policy is reject. Rules accept packets depending on their source and destination IP addresses. Addresses are chosen at random when adding the rules.

Loading nftables rules amounts to using the nft tool. To load pfcomp rules, we load a small kernel module that registers the code generated by pfcomp into the INPUT netfilter hook of netns server.

Next we set the addresses of client and server, such that packets flowing from client to server hit the worst-case performance of the filter.

Then we run a throughput and latency test from the client to the server using the iperf3 and ping tools.

# Usage

Edit configuration in `benchmark` script if needed.

Set up network namespaces:
```
./benchmark setup_netns
```

Run benchmark:
```
./benchmark run
```
Results are written to results.csv (overwriting the file). You can run it multiple times.

Show the results as a graphic:
```
./plot
```

Clean up:
```
./benchmark clean
```

When you are done running benchmarks, it is not really necessary to remove the network namespaces. They will be removed upon reboot. You can run `./benchmark clean_netns` if you want to remove them anyway. However, if you do so, you will need to reboot in order to run the benchmark again. This is because nsids cannot be removed or reassigned (other than by rebooting).

# Remarks

`_benchmark` is a completion file for zsh.
