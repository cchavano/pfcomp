#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/string.h>

static struct nf_hook_ops *nf_hook_ops = NULL;
struct net *server_net_ns;

struct Packet {
  unsigned int saddr;
  unsigned int daddr;
};
_Bool filter(struct Packet *$packet);

unsigned int nf_handler_pfcomp(void *priv, struct sk_buff *skb, const struct nf_hook_state *state) {
  struct iphdr *iph;
  struct Packet packet;
  if (!skb) {
    return NF_ACCEPT;
  }
  iph = ip_hdr(skb);
  packet.saddr = ntohl(iph->saddr);
  packet.daddr = ntohl(iph->daddr);
  if(filter(&packet)) {
    return NF_ACCEPT;
  }
  return NF_DROP;
}

static int __init nfhook_init(void) {
  server_net_ns = get_net_ns_by_id(&init_net, 10);
  if (server_net_ns == NULL) {
    server_net_ns = &init_net;
  }
  nf_hook_ops = (struct nf_hook_ops *) kcalloc(1, sizeof(struct nf_hook_ops), GFP_KERNEL);
  if (nf_hook_ops != NULL) {
    nf_hook_ops->hook = (nf_hookfn *)nf_handler_pfcomp;
    nf_hook_ops->hooknum = NF_INET_LOCAL_IN;
    nf_hook_ops->pf = NFPROTO_IPV4;
    nf_hook_ops->priority = NF_IP_PRI_FIRST;
    nf_register_net_hook(server_net_ns, nf_hook_ops);
  }
  printk(KERN_INFO "nfhook init\n");
  return 0;
}

static void __exit nfhook_exit(void) {
  if (nf_hook_ops != NULL) {
    nf_unregister_net_hook(server_net_ns, nf_hook_ops);
    kfree(nf_hook_ops);
  }
  printk(KERN_INFO "nfhook exit\n");
}

module_init(nfhook_init);
module_exit(nfhook_exit);

MODULE_LICENSE("GPL");
